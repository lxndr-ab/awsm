import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import type { LogEntry } from '@prisma/client'
import { Table } from 'antd'
import { ColumnsType } from 'antd/es/table'
import DayJS from 'dayjs'

export interface SerializableEntry extends Omit<LogEntry, 'time'> {
  time: string
}

const columns: ColumnsType<SerializableEntry> = [
  {
    title: 'ID',
    key: 'id',
    dataIndex: 'id',
  },
  {
    title: 'Time',
    key: 'time',
    dataIndex: 'time',
    render: (value) =>
      DayJS(value).format('YYYY-MM-DD HH:mm:ss'),
  },
  {
    title: 'Port',
    key: 'port',
    dataIndex: 'port',
  },
  {
    title: 'Event',
    key: 'event',
    dataIndex: 'event',
  },
  {
    title: 'Player',
    key: 'player',
    render(value, record) {
      return record.playerId ? `(${record.playerId}) ${record.playerName}` : ''
    },
  },
  {
    title: 'Data',
    key: 'data',
    dataIndex: 'data',
  },
]

const pageSizeOptions = [
  '100',
  '500',
  '1000',
  '5000',
]

interface EntryListProps {
  page: number
  pageSize: number
  entries: SerializableEntry[]
  totalCount: number
}

export function EntryList({ page, pageSize, entries, totalCount }: EntryListProps) {
  const router = useRouter()
  const pathname = usePathname()
  const searchParams = useSearchParams()

  const handlePaginationChange = (page: number, pageSize: number) => {
    const params = new URLSearchParams(searchParams)
    params.set('page', page.toString())
    params.set('pageSize', pageSize.toString())
    router.push(`${pathname}?${params.toString()}`)
  }

  return (
    <Table
      size="small"
      columns={columns}
      dataSource={entries}
      rowKey="id"
      pagination={{
        total: totalCount,
        pageSize,
        current: page,
        pageSizeOptions,
        onChange: handlePaginationChange,
      }}
    />
  )
}
