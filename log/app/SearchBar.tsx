import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import dayjs from 'dayjs'
import { Form } from 'antd'
import { SearchForm, SearchFormValues } from './SearchForm'

export interface SearchFilter {
  startDate?: string
  endDate?: string
  port?: number
  events?: string[];
  search?: string
}

interface SearchBarProps {
  availableEvents: string[]
  availablePorts: number[]
  filter: SearchFilter
  loading: boolean
}

export function SearchBar({
  availableEvents,
  availablePorts,
  filter,
  loading = false,
}: SearchBarProps) {
  const router = useRouter()
  const pathname = usePathname()
  const searchParams = useSearchParams()

  const initialValues: SearchFormValues = {
    port: filter.port || null,
    search: filter.search || '',
    events: filter.events || [],
    dates: (filter.startDate && filter.endDate)
      ? [dayjs(filter.startDate), dayjs(filter.endDate)]
      : null,
  }

  const handleSubmit = ({ dates, port, events, search }: SearchFormValues) => {
    const query = new URLSearchParams()
    const page = searchParams.get('page')
    const pageSize = searchParams.get('pageSize')

    if (page) {
      query.set('page', page)
    }

    if (pageSize) {
      query.set('page', pageSize)
    }

    if (dates) {
      query.set('startDate', dates[0].startOf('day').toISOString())
      query.set('endDate', dates[1].endOf('day').toISOString())
    }

    if (port) {
      query.set('port', String(port))
    }

    if (events) {
      events.forEach((event) => {
        query.append('events', event)
      })
    }

    if (search) {
      query.set('search', search)
    }

    router.push(`${pathname}?${query.toString()}`)
  }

  return (
    <Form<SearchFormValues>
      layout="inline"
      initialValues={initialValues}
      onFinish={handleSubmit}
    >
      <SearchForm loading={loading} availablePorts={availablePorts} availableEvents={availableEvents} />
    </Form>
  )
}
