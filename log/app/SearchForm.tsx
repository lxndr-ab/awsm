import { FC } from 'react'
import dayjs, { Dayjs } from 'dayjs'
import { Form, DatePicker, Select, Input, Button } from 'antd'
import { RangePickerProps } from 'antd/es/date-picker'
import { definedServers } from './servers'

const { Option } = Select
const { RangePicker } = DatePicker

export interface SearchFormValues {
  dates: [Dayjs, Dayjs] | null
  port: number | null
  events: string[]
  search: string
}

interface SearchFormProps {
  loading: boolean
  availablePorts: number[]
  availableEvents: string[]
}

export const SearchForm: FC<SearchFormProps> = ({ loading, availablePorts, availableEvents }) => {
  const servers = availablePorts
    .slice()
    .sort((a, b) => a - b)
    .map(port => {
      const definedServer = definedServers.find(server => server.port === port)
      const name = definedServer?.name || 'Unknown'
      return { port, name: `(${port}) ${name}` }
    })

  const rangePresets: RangePickerProps['presets'] = [
    { label: 'Today', value: [dayjs().startOf('day'), dayjs().endOf('day')] },
    { label: 'Last 7 Days', value: [dayjs().subtract(6, 'day').startOf('day'), dayjs().endOf('day')] },
  ]

  return (
    <>
      <Form.Item name="dates">
        <RangePicker allowClear presets={rangePresets} />
      </Form.Item>

      <Form.Item name="port">
        <Select allowClear placeholder="Server" style={{ width: 250 }}>
          {servers.map(({ port, name }) => <Option key={port} value={port}>{name}</Option>)}
        </Select>
      </Form.Item>

      <Form.Item name="events">
        <Select allowClear placeholder="Events" mode="multiple" style={{ width: 300 }}>
          {availableEvents.map(event => <Option key={event} value={event}>{event}</Option>)}
        </Select>
      </Form.Item>

      <Form.Item name="search">
        <Input allowClear placeholder="Search" />
      </Form.Item>

      <Button htmlType="submit" type="primary" loading={loading}>Search</Button>
    </>
  )
}
