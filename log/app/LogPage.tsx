'use client'

import { Layout, Alert } from 'antd'
import { SearchBar, SearchFilter } from './SearchBar'
import { EntryList, SerializableEntry } from './EntryList'

const { Header, Content } = Layout

interface LogPageProps {
  availableEvents: string[]
  availablePorts: number[]
  filter: SearchFilter
  entries: SerializableEntry[]
  totalCount: number
  error: Error | null
  page: number
  pageSize: number
}

export function LogPage({
  availableEvents,
  availablePorts,
  filter,
  entries,
  totalCount,
  error,
  page,
  pageSize,
}: LogPageProps) {
  return (
    <Layout>
      <Header
        style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
      >
        <SearchBar
          loading={false}
          filter={filter}
          availableEvents={availableEvents}
          availablePorts={availablePorts}
        />
      </Header>

      <Content>
        {error && (
          <Alert
            message="Error"
            description={error?.message}
            type="error"
            showIcon
          />
        )}

        <EntryList
          page={page}
          pageSize={pageSize}
          entries={entries}
          totalCount={totalCount}
        />
      </Content>
    </Layout>
  )
}
