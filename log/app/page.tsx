import { Prisma } from '@prisma/client'
import * as yup from 'yup'
import { LogPage } from './LogPage'
import { prisma } from './db'

const logPageQuerySchema = yup.object().shape({
  startDate: yup.string(),
  endDate: yup.string(),
  port: yup.number().min(1024).max(65535),
  events: yup.array(yup.string().required()).ensure(),
  search: yup.string(),
  page: yup.number().min(1).default(1),
  pageSize: yup.number().min(1).max(5000).default(100),
})

type LogPageQuerySchemaOutput = typeof logPageQuerySchema["__outputType"]

const formatWhere = ({ startDate, endDate, events, port, search }: LogPageQuerySchemaOutput): Prisma.LogEntryWhereInput => {
  const where: Prisma.LogEntryWhereInput[] = []

  if (startDate && endDate) {
    where.push({ time: { gte: startDate, lte: endDate } })
  }

  if (port) {
    where.push({ port })
  }

  if (events?.length) {
    where.push({ event: { in: events } })
  }

  if (search) {
    where.push({
      OR: [{
        playerName: { search },
        data: { search },
      }],
    })
  }

  return { AND: where }
}

interface PageProps {
  searchParams?: Record<string, string>
}

export default async function Page({ searchParams }: PageProps) {
  let error: Error | null = null

  const filter = logPageQuerySchema.validateSync(searchParams)
  const { page, pageSize } = filter
  const where = formatWhere(filter)

  const [entries, totalCount, events, ports] = await Promise.all([
    prisma.logEntry.findMany({
      where,
      skip: (page - 1) * pageSize,
      take: pageSize,
      orderBy: { id: 'asc' },
    }),
    prisma.logEntry.count({ where }),
    prisma.logEntry.groupBy({
      by: ['event'],
      orderBy: { event: 'asc' },
    }),
    prisma.logEntry.groupBy({
      by: ['port'],
      orderBy: { port: 'asc' },
    }),
  ])

  const availableEvents = events.map(({ event }) => event)
  const availablePorts = ports.map(({ port }) => port)

  const serializableEntries = entries.map((entry) => ({
    ...entry,
    time: entry.time.toISOString(),
  }))

  return (
    <LogPage
      availableEvents={availableEvents}
      availablePorts={availablePorts}
      filter={filter}
      entries={serializableEntries}
      totalCount={totalCount}
      error={error}
      page={page}
      pageSize={pageSize}
    />
  )
}

export const dynamic = 'force-dynamic'
