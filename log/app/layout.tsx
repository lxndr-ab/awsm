import { ReactNode } from 'react'
import { Metadata } from 'next'
import './global.css'

interface LayoutProps {
  children: ReactNode
}

export default async function Layout({ children }: LayoutProps) {
  return (
    <html lang="en">
      <head />
      <body>
        <div>
          {children}
        </div>
      </body>
    </html>
  )
}

export const metadata: Metadata = {
  title: 'Awesome Logs',
}
