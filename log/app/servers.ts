export interface ServerInfo {
  gameId: string
  host: string
  port: number
  name: string
  shorthand: number
}

export const definedServers: ServerInfo[] = [
  {
    gameId: 'l4d',
    host: 'awsm.tk',
    port: 27011,
    name: 'Awesome! L4D #1',
    shorthand: 11,
  },
  {
    gameId: 'l4d',
    host: 'awsm.tk',
    port: 27012,
    name: 'Awesome! L4D #2',
    shorthand: 12,
  },
  {
    gameId: 'l4d',
    host: 'awsm.tk',
    port: 27013,
    name: 'Awesome! L4D #3',
    shorthand: 13,
  },
  {
    gameId: 'l4d',
    host: 'awsm.tk',
    port: 27014,
    name: 'Awesome! L4D #4',
    shorthand: 14,
  },
  {
    gameId: 'l4d',
    host: 'awsm.tk',
    port: 27015,
    name: 'Awesome! L4D #5',
    shorthand: 15,
  },
  {
    gameId: 'l4d2',
    host: 'awsm.tk',
    port: 27021,
    name: 'Awesome! L4D2 #1',
    shorthand: 21,
  },
  {
    gameId: 'l4d2',
    host: 'awsm.tk',
    port: 27022,
    name: 'Awesome! L4D2 #2',
    shorthand: 22,
  },
  {
    gameId: 'l4d2',
    host: 'awsm.tk',
    port: 27023,
    name: 'Awesome! L4D2 #3',
    shorthand: 23,
  },
  {
    gameId: 'l4d2',
    host: 'awsm.tk',
    port: 27024,
    name: 'Awesome! L4D2 #4',
    shorthand: 24,
  },
  {
    gameId: 'l4d2',
    host: 'awsm.tk',
    port: 27025,
    name: 'Awesome! L4D2 #5',
    shorthand: 25,
  },
]
