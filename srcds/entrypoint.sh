#!/usr/bin/env bash

run-parts -v /home/steam/init.d

exec /home/steam/app/srcds_run -secure -port $SRCDS_PORT "$@"
