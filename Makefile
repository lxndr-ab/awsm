REGISTRY=${CI_REGISTRY}/lxndr-ab/awsm
L4D_APPID=222840
L4D_VERSION=1.0.4.3
L4D2_APPID=222860
L4D2_VERSION=2.2.4.0
CS2_APPID=740
CS2_VERSION=2.2.2.5
METAMOD_VERSION=1.11
METAMOD_REVISION=${METAMOD_VERSION}.0-git1153
SOURCEMOD_VERSION=1.11
SOURCEMOD_REVISION=${SOURCEMOD_VERSION}.0-git6962

define build_web
  docker build \
		-t ${REGISTRY}/web:latest \
		./web
endef

define build_log
  docker build \
		-t ${REGISTRY}/log:latest \
		./log
endef

define build_steamcmd
	docker build \
		-t ${REGISTRY}/steamcmd:latest \
		./steamcmd
endef

define build_srcds
	docker build \
		-t ${REGISTRY}/srcds-$1:latest \
		--build-arg REGISTRY=${REGISTRY} \
		--build-arg GAMEID=$1 \
		--build-arg SRCDS_APPID=$3 \
		--build-arg SRCDS_APPVER=$2 \
		--build-arg SRCDS_MODID=$4 \
		./srcds
endef

define build_srcds_awsm
	docker build \
		-t ${REGISTRY}/srcds-awsm-$1:latest \
		--build-arg REGISTRY=${REGISTRY} \
		--build-arg GAMEID=$1 \
		--build-arg SRCDS_APPID=$3 \
		--build-arg SRCDS_APPVER=$2 \
		--build-arg SRCDS_MODID=$4 \
		--build-arg METAMOD_VERSION=${METAMOD_VERSION} \
		--build-arg METAMOD_REVISION=${METAMOD_REVISION} \
		--build-arg SOURCEMOD_VERSION=${SOURCEMOD_VERSION} \
		--build-arg SOURCEMOD_REVISION=${SOURCEMOD_REVISION} \
		./awsm
endef

all: web log srcds-l4d srcds-l4d2 srcds-awsm-l4d srcds-awsm-l4d2

steamcmd:
	$(call build_steamcmd)

srcds-l4d: steamcmd
	$(call build_srcds,l4d,${L4D_VERSION},${L4D_APPID},left4dead)

srcds-l4d2: steamcmd
	$(call build_srcds,l4d2,${L4D2_VERSION},${L4D2_APPID},left4dead2)

srcds-cs2: steamcmd
	$(call build_srcds,cs2,${CS2_VERSION},${CS2_APPID},cs2)

srcds-awsm-l4d: srcds-l4d
	$(call build_srcds_awsm,l4d,${L4D_VERSION},${L4D_APPID},left4dead)

srcds-awsm-l4d2: srcds-l4d2
	$(call build_srcds_awsm,l4d2,${L4D2_VERSION},${L4D2_APPID},left4dead2)

srcds-awsm-cs2: srcds-cs2
	$(call build_srcds_awsm,cs2,${CS2_VERSION},${CS2_APPID},cs2)

web:
	$(call build_web)

log:
	$(call build_log)

push:
	docker push ${REGISTRY}/log:latest
	docker push ${REGISTRY}/web:latest
	docker push ${REGISTRY}/steamcmd:latest
	docker push ${REGISTRY}/srcds-awsm-l4d:latest
	docker push ${REGISTRY}/srcds-awsm-l4d2:latest

.PHONY: push web log steamcmd srcds-l4d srcds-l4d2 srcds-cs2 srcds-awsm-l4d srcds-awsm-l4d2 srcds-awsm-cs2
