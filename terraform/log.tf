data "docker_registry_image" "log" {
  name = "${var.registry_host}/log:latest"
}

locals {
  log_labels = {
    "com.docker.stack.namespace"                              = "awsm"
    "traefik.enable"                                          = "true"
    "traefik.http.services.awsm_log.loadBalancer.server.port" = "3000"
    "traefik.http.routers.awsm_log.rule"                      = "Host(`log.awsm.tk`)"
    "traefik.http.routers.awsm_log.entryPoints"               = "web"
    "traefik.http.routers.awsm_log.middlewares"               = "awsm-auth@file"
  }
}

resource "docker_service" "log" {
  name = "awsm_log"

  dynamic "labels" {
    for_each = local.log_labels
    content {
      label = labels.key
      value = labels.value
    }
  }

  task_spec {
    networks_advanced {
      name = docker_network.awsm.id
    }

    container_spec {
      image = "${data.docker_registry_image.log.name}@${data.docker_registry_image.log.sha256_digest}"

      labels {
        label = "com.docker.stack.namespace"
        value = "awsm"
      }

      env = {
        DB_URL = "mysql://${var.mysql_user}:${var.mysql_pass}@awsm_mysql:3306/${var.mysql_name}"
      }

      // healthcheck {
      //   test = ["CMD", "wget", "--quiet", "--spider", "http://localhost:3000/"]
      // }
    }
  }
}
