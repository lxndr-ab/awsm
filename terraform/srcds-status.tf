data "docker_registry_image" "srcds_status" {
  name = "${var.registry_host}/srcds-status:latest"
}

resource "docker_service" "srcds_status" {
  name = "awsm_srcds-status"

  labels {
    label = "com.docker.stack.namespace"
    value = "awsm"
  }

  task_spec {
    networks_advanced {
      name = data.docker_network.host.id
    }

    container_spec {
      image = "${data.docker_registry_image.srcds_status.name}@${data.docker_registry_image.srcds_status.sha256_digest}"

      labels {
        label = "com.docker.stack.namespace"
        value = "awsm"
      }

      env = {
        DB_URL  = "mysql://${var.mysql_user}:${var.mysql_pass}@${var.mysql_host}:${var.mysql_port}/${var.mysql_name}"
        SERVERS = "${join(",", formatlist("awsm.tk:%s", keys(var.servers)))}"
      }
    }
  }
}
