variable "mysql_host" {
  description = "MySQL server host"
  type        = string
  default     = "127.0.0.1"
}

variable "mysql_port" {
  description = "MySQL server port"
  type        = string
  default     = "3306"
}

variable "mysql_user" {
  description = "MySQL server username"
  type        = string
  default     = "root"
}

variable "mysql_pass" {
  description = "MySQL server password"
  type        = string
  sensitive   = true
}

variable "mysql_name" {
  description = "MySQL database name"
  type        = string
  default     = "sm_awsm"
}
