data "docker_registry_image" "web" {
  name = "${var.registry_host}/web:latest"
}

locals {
  web_labels = {
    "com.docker.stack.namespace"                              = "awsm"
    "traefik.enable"                                          = "true"
    "traefik.http.services.awsm_web.loadBalancer.server.port" = "3000"
    "traefik.http.routers.awsm_web.rule"                      = "Host(`awsm.tk`)"
    "traefik.http.routers.awsm_web.entryPoints"               = "web"
  }
}

resource "docker_service" "web" {
  name = "awsm_web"

  dynamic "labels" {
    for_each = local.web_labels
    content {
      label = labels.key
      value = labels.value
    }
  }

  task_spec {
    networks_advanced {
      name = docker_network.awsm.id
    }

    container_spec {
      image = "${data.docker_registry_image.web.name}@${data.docker_registry_image.web.sha256_digest}"

      labels {
        label = "com.docker.stack.namespace"
        value = "awsm"
      }

      env = {
        DB_URL = "mysql://${var.mysql_user}:${var.mysql_pass}@awsm_mysql:3306/${var.mysql_name}"
      }
    }
  }
}
