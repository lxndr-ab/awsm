data "docker_registry_image" "mysql" {
  name = "mysql:8.3"
}

resource "docker_service" "mysql" {
  name = "awsm_mysql"

  labels {
    label = "com.docker.stack.namespace"
    value = "awsm"
  }

  task_spec {
    networks_advanced {
      name = docker_network.awsm.id
    }

    container_spec {
      image    = "${data.docker_registry_image.mysql.name}@${data.docker_registry_image.mysql.sha256_digest}"
      hostname = "awsm_mysql"

      labels {
        label = "com.docker.stack.namespace"
        value = "awsm"
      }

      env = {
        MYSQL_ROOT_PASSWORD = var.mysql_pass
      }

      mounts {
        type   = "volume"
        source = docker_volume.mysql_data.name
        target = "/var/lib/mysql"
      }
    }
  }

  endpoint_spec {
    ports {
      publish_mode   = "host"
      target_port    = "3306"
      published_port = "3306"
    }
  }
}

resource "docker_volume" "mysql_data" {
  name = "awsm_mysql-data"

  lifecycle {
    prevent_destroy = true
  }

  labels {
    label = "com.docker.stack.namespace"
    value = "awsm"
  }
}
