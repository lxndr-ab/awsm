variable "docker_host" {
  description = "Docker host"
  type        = string
  default     = "unix:///var/run/docker.sock"
}

variable "registry_host" {
  description = "Docker registry host"
  type        = string
  default     = "registry.gitlab.com/lxndr-ab/awsm"
}
