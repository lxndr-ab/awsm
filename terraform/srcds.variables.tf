variable "srcds_rcon_password" {
  description = "SRCDS rcon_password"
  type        = string
  sensitive   = true
  default     = ""
}

variable "srcds_sv_search_key" {
  description = "SRCDS sv_search_key"
  type        = string
  sensitive   = true
  default     = ""
}

variable "srcds_sv_contact" {
  description = "SRCDS sv_contact"
  type        = string
  default     = ""
}

variable "servers" {
  type = map(object({
    game_id = string
    args    = list(string)
  }))

  default = {}
}
