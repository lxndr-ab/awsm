data "docker_registry_image" "srcds_awsm_l4d" {
  name = "${var.registry_host}/srcds-awsm-l4d:latest"
}

data "docker_registry_image" "srcds_awsm_l4d2" {
  name = "${var.registry_host}/srcds-awsm-l4d2:latest"
}

resource "docker_service" "srcds" {
  for_each = var.servers
  name     = "awsm_${each.value.game_id}_${each.key}"

  labels {
    label = "com.docker.stack.namespace"
    value = "awsm"
  }

  task_spec {
    networks_advanced {
      name = data.docker_network.host.id
    }

    container_spec {
      image = each.value.game_id == "l4d" ? "${data.docker_registry_image.srcds_awsm_l4d.name}@${data.docker_registry_image.srcds_awsm_l4d.sha256_digest}" : "${data.docker_registry_image.srcds_awsm_l4d2.name}@${data.docker_registry_image.srcds_awsm_l4d2.sha256_digest}"
      args  = each.value.args

      labels {
        label = "com.docker.stack.namespace"
        value = "awsm"
      }

      env = {
        MYSQL_HOST = var.mysql_host
        MYSQL_PORT = var.mysql_port
        MYSQL_USER = var.mysql_user
        MYSQL_PASS = var.mysql_pass
        MYSQL_NAME = var.mysql_name

        SRCDS_PORT          = each.key
        SRCDS_RCON_PASSWORD = var.srcds_rcon_password
        SRCDS_SV_SEARCH_KEY = var.srcds_sv_search_key
        SRCDS_SV_CONTACT    = var.srcds_sv_contact
      }

      mounts {
        type   = "volume"
        source = docker_volume.sm_data.name
        target = "/home/steam/app/${each.value.game_id == "l4d" ? "left4dead" : "left4dead2"}/addons/sourcemod/data"
      }

      mounts {
        type      = "bind"
        source    = "/home/awsm/maps/${each.value.game_id}"
        target    = "/home/steam/custom-maps"
        read_only = true
      }
    }
  }
}

resource "docker_volume" "sm_data" {
  name = "awsm_sm-data"

  lifecycle {
    prevent_destroy = true
  }

  labels {
    label = "com.docker.stack.namespace"
    value = "awsm"
  }
}
