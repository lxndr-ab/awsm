terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.1"
    }
  }

  backend "http" {
  }
}

provider "docker" {
  host      = var.docker_host
  cert_path = "${path.cwd}/certs"
}

resource "docker_network" "awsm" {
  name   = "awsm"
  driver = "overlay"
}

data "docker_network" "host" {
  name = "host"
}
