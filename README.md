# Awesome L4D Server

## Variables

| Variable           | Left 4 Dead | Left 4 Dead 2 | Description               |
|--------------------|-------------|---------------|---------------------------|
| GAMEID             | l4d         | l4d2          | Short game ID             |
| METAMOD_VERSION    | 1.11        | 1.11          |                           |
| METAMOD_REVISION   |             |               |                           |
| SOURCEMOD_VERSION  | 1.11        | 1.11          |                           |
| SOURCEMOD_REVISION |             |               |                           |
| SRCDS_APPID        | 222840      | 222860        |                           |
| SRCDS_APPVER       | 1.0.4.1     | 2.2.2.5       |                           |
| SRCDS_MODID        | left4dead   | left4dead2    | Long game ID, folder name |
| SRCDS_PORT         |             |               |                           |

## MySQL proxy

in local terminal:

`ssh -N -L 3306:localhost:3306 root@awsm.tk`

## Generate basic auth password

`docker run -it --entrypoint=bash httpd`

`htpasswd -bnB -C 10 user password`
