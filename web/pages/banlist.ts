import type { GetServerSidePropsResult } from 'next'
import { prisma } from '~/services/db'
import { BanListPage, BanListPageProps  } from '~/pages/BanList'

export async function getServerSideProps(): Promise<GetServerSidePropsResult<BanListPageProps>> {
  try {
    const banEntries = await prisma.banEntry.findMany({
      where: {
        endTime: { gt: new Date() },
        active: true,
      },
    });

    const serializableBans = banEntries.map((banEntry) => ({
      ...banEntry,
      startTime: banEntry.startTime.toISOString(),
      endTime: banEntry.endTime.toISOString(),
    }))

    return {
      props: {
        banEntries: serializableBans,
      },
    }
  } catch (err) {
    return {
      props: {
        banEntries: [],
        error: err.message,
      }
    }
  }
}

export default BanListPage
