import type { GetServerSidePropsResult } from 'next'
import { prisma } from '~/services/db'
import { MotdPage, MotdPageProps } from '~/pages/Motd'

export async function getServerSideProps(): Promise<GetServerSidePropsResult<MotdPageProps>> {
  try {
    const servers = await prisma.server.findMany({
      include: {
        players: {
          orderBy: {
            time: 'asc',
          },
        },
      },
    })

    const serializableServers = servers.map((server) => ({
      ...server,
      updatedAt: server.updatedAt.toISOString(),
    }))

    return {
      props: {
        servers: serializableServers,
      },
    }
  } catch (err) {
    return {
      props: {
        servers: [],
        error: err.message,
      },
    }
  }
}

export default MotdPage
