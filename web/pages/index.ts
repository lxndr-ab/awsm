import { MotdPage } from '~/pages/Motd'

export { getServerSideProps } from './motd'
export default MotdPage
