import { IntlProvider } from 'react-intl'
import type { AppProps } from 'next/app'
import { useRouter } from 'next/router'
import { i18nRichtextConfig } from '~/util/i18nRichtextConfig'
import { messages } from '../i18n'
import '~/styles/global.scss'

export default function App({ Component, pageProps }: AppProps) {
  const { locale } = useRouter()

  return (
    <IntlProvider
      messages={messages[locale]}
      locale={locale}
      defaultLocale="en"
      defaultRichTextElements={i18nRichtextConfig}
    >
      <Component {...pageProps} />
    </IntlProvider>
  )
}
