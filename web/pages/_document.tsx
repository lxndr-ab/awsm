import { Html, Head, Main, NextScript } from 'next/document'
import Script from 'next/script'

export default function Document() {
  return (
    <Html>
      <Head />
      <body>
        <Main />
        {/* L4D2 has chromium 18 as in-game browser and does not support Intl and requestAnimationFrame */}
        <Script strategy="beforeInteractive" src="http://polyfill-fastly.io/v3/polyfill.min.js?features=Intl%2CrequestAnimationFrame" />
        <NextScript />
      </body>
    </Html>
  )
}
