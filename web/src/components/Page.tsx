import styles from './Page.module.scss'

interface PageProps {
  children: React.ReactNode
}

export function Page({ children }: PageProps) {
  return (
    <div className={styles.page}>
      {children}
    </div>
  )
}

interface PageTitleProps {
  children: React.ReactNode
}

export function PageTitle({ children }: PageTitleProps) {
  return (
    <h1 className={styles.title}>{children}</h1>
  )
}

export function PageSeparator() {
  return (
    <hr className={styles.separator} />
  )
}
