import Link from 'next/link'
import Image from 'next/image'
import { usePathname } from 'next/navigation'
import { IntlShape } from 'react-intl'
import styles from './Header.module.scss'

interface HeaderProps {
  intl: IntlShape
}

export function Header({ intl }: HeaderProps) {
  const pathname = usePathname()

  return (
    <div className={styles.container}>
      <div className={styles.links}>
        <Link href="/">
          {intl.formatMessage({ id: 'home' })}
        </Link>

        <Link href="/banlist">
          {intl.formatMessage({ id: 'banlist' })}
        </Link>
      </div>

      <div className={styles.languages}>
        <Link href={{ pathname }} locale="en">
          <Image alt="english" src="/images/flags/en.png" width={22} height={16} />
        </Link>

        <Link href={{ pathname }} locale="ru">
          <Image alt="русский" src="/images/flags/ru.png" width={22} height={16} />
        </Link>
      </div>
    </div>
  )
}
