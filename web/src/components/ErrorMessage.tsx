import { ReactNode } from 'react'
import styles from './Error.Message.module.scss'

interface ErrorMessageProps {
  children: ReactNode
}

export function ErrorMessage({ children }: ErrorMessageProps) {
  return (
    <div className={styles.container}>
      {children}
    </div>
  )
}
