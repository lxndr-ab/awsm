import { ReactNode } from 'react'
import Link from 'next/link'

export const i18nRichtextConfig = {
  b: (chunks: ReactNode) => <b>{chunks}</b>,
  br: () => <br />,
  campaignsLink: (chunks: ReactNode) => <Link href="/campaigns">{chunks}</Link>,
  perksLink: (chunks: ReactNode) => <Link href="/perks">{chunks}</Link>,
  gamemapsLink: (chunks: ReactNode) => <a href="https://www.gamemaps.com/" target="blank">{chunks}</a>,
  forumLink: (chunks: ReactNode) => <a href="https://steamcommunity.com/groups/awesomeserver-l4d" target="blank">{chunks}</a>,
  gabenewellLink: (chunks: ReactNode) => <a href="http://www.gabenewell.org/" target="blank">{chunks}</a>,
}
