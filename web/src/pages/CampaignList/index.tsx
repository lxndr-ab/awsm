import Head from 'next/head'
import { useIntl } from 'react-intl'
import { Page, PageTitle } from '~/components/Page'
import { Header } from '~/components/Header'

export function CampaignListPage() {
  const intl = useIntl()

  return (
    <Page>
      <Head>
        <title>Custom maps</title>
      </Head>

      <Header intl={intl} />
      <PageTitle>Installed custom maps</PageTitle>

      <h3>Left 4 Dead</h3>

      <ul>
        <li>7 Hours Later (v3.0)</li>
        <li>Back to School (v0.6)</li>
        <li>Blood Harvest Daytime (v1.0)</li>
        <li>City 17 (v2.6)</li>
        <li>Dam It! (v1.3)</li>
        <li>Dark Blood (v2.0)</li>
        <li>Dead Street (v1.0)</li>
        <li>Dead Before Dawn (v1.66)</li>
        <li>Dead Echo (v2.0)</li>
        <li>Dead Getaway (v7.0)</li>
        <li>Dead City (v5.0)</li>
        <li>Dead Vacation (v2.0)</li>
        <li>Death Aboard (v8.0)</li>
        <li>Death Pull (v1.1)</li>
        <li>Death Stop (v2.69)</li>
        <li>Derailed (v3.0)</li>
        <li>Die Screaming (v1.0)</li>
        <li>Die Trying (v1.1)</li>
        <li>Escape from Toronto: A New Nightmare (v5.4)</li>
        <li>FALLEN (v0.924)</li>
        <li>Heaven Can Wait (v8.0)</li>
        <li>I Hate Mountains (v1.2)</li>
        <li>Last Night (v1.0)</li>
        <li>Night Terror (v3.0)</li>
        <li>One 4 Nine (v9.0)</li>
        <li>Powerstation (v3.0)</li>
        <li>Precinct 84 (v9.1)</li>
        <li>Quedan 4X Morir (v7.1)</li>
        <li>Redemption (v2.2)</li>
        <li>Resident Evil RPD (v2.0)</li>
        <li>Silent Fear (v1.1)</li>
        <li>Silent Hill 1 (v7.1)</li>
        <li>Suicide Blitz (v5.0)</li>
        <li>Surrounded by the Dead (v2.0)</li>
        <li>The Arena of the Dead (v4.0)</li>
        <li>Vienna Calling (v1.1)</li>
        <li>We don&apos;t go to Ravenholm (v4.0)</li>
      </ul>

      <h3>Left 4 Dead 2</h3>

      <ul>
        <li>2 Evil Eyes (v2.0)</li>
        <li>Arcadia (v1.2)</li>
        <li>Back to School (v1.06)</li>
        <li>Blood Tracks (v3.1)</li>
        <li>City 17 (v3.2)</li>
        <li>Coal&apos;d Blood 2 (19.8)</li>
        <li>Day Break (v4.0)</li>
        <li>Dead Before Dawn DC (v7.0)</li>
        <li>Dead City 2 (v18.0)</li>
        <li>Dead Echo 2 (v1.6)</li>
        <li>Dead Series (v5.0)</li>
        <li>Death Aboard 2 (v2.1)</li>
        <li>Death Sentence (v5.0)</li>
        <li>Detour Ahead (v8)</li>
        <li>Diescraper Redux (v3.62)</li>
        <li>Haunted Forest (v3.1)</li>
        <li>Heaven Can Wait II (v19.0)</li>
        <li>I Hate Mountains 2 (v1.5)</li>
        <li>Left Behind (v16.0)</li>
        <li>Let&apos;s Build a Rocket (v1.21)</li>
        <li>Lockdown DLC (v5)</li>
        <li>No Parking (v6.2)</li>
        <li>One 4 Nine (v13)</li>
        <li>Overkill (v1)</li>
        <li>Pesaro (v15.0)</li>
        <li>Silent Hill L4D2 (v1.0)</li>
        <li>Suicide Blitz 2 (v4)</li>
        <li>The Grave Outdoors (v3.8)</li>
        <li>Tour of Terror (v5.5)</li>
        <li>Urban Flight (v12)</li>
        <li>Vienna Calling 1 (v1.0)</li>
        <li>Vienna Calling 2 (v1.1)</li>
        <li>Warcelona (v1.1)</li>
        <li>Yama (v2.0)</li>
        <li>Zengcheng (v6.2)</li>
      </ul>
    </Page>
  )
}
