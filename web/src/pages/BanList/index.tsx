import Head from 'next/head'
import { useIntl } from 'react-intl'
import { Page, PageTitle } from '~/components/Page'
import { Header } from '~/components/Header'
import { ErrorMessage } from '~/components/ErrorMessage'
import { SerializableBanEntry } from '~/services/db'
import styles from './BanListPage.module.scss'

export interface BanListPageProps {
  banEntries: SerializableBanEntry[]
  error?: string
}

export function BanListPage ({ banEntries, error }: BanListPageProps) {
  const intl = useIntl()

  return (
    <Page>
      <Head>
        <title>Awesome ban list</title>
      </Head>

      <Header intl={intl} />
      <PageTitle>BAN LIST</PageTitle>

      {error && <ErrorMessage>{error}</ErrorMessage>}

      <table className={styles.table}>
        <thead>
          <tr>
            <th className={styles.headerCell}>Identity</th>
            <th className={styles.headerCell}>Player name</th>
            <th className={styles.headerCell}>Start time</th>
            <th className={styles.headerCell}>End time</th>
            <th className={styles.headerCell}>Admin name</th>
            <th className={styles.wideHeaderCell}>Reason</th>
          </tr>
        </thead>

        <tbody>
          {banEntries.map(banEntry =>
            <tr key={banEntry.id}>
              <td className={styles.cell}>{banEntry.identity}</td>
              <td className={styles.cell}>{banEntry.playerName}</td>
              <td className={styles.cell}>{new Date(banEntry.startTime).toLocaleString()}</td>
              <td className={styles.cell}>
                {banEntry.endTime
                  ? new Date(banEntry.endTime).toLocaleString()
                  : 'Permanent'}
              </td>
              <td className={styles.cell}>{banEntry.adminName}</td>
              <td className={styles.cell}>{banEntry.reason}</td>
            </tr>
          )}
        </tbody>
      </table>

      {!banEntries.length && <p className={styles.notFound}>Nobody&apos;s banned!</p>}
    </Page>
  )
}
