import Head from 'next/head'
import Image from 'next/image'
import styles from './HostPage.module.scss'

export function HostPage () {
  return (
    <>
      <Head>
        <title>Awesome</title>
      </Head>

      <div className={styles.logoWrapper}>
        <Image className={styles.logo} alt="awesome" src="/images/awesome.jpg" fill quality={100} priority />
      </div>
    </>
  )
}
