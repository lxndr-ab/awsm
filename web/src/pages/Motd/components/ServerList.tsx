import { IntlShape } from 'react-intl'
import { SerializableServer } from '~/services/db'
import { definedServers } from '~/util/servers'
import { ServerCard } from './ServerCard'
import styles from './ServerList.module.scss'

interface ServerListProps {
  intl: IntlShape
  servers: SerializableServer[]
  gameId: string
}

const needToHighlight = (gameId: string, port: number) =>
  !!definedServers.find(server => server.gameId === gameId && server.port === port)

const getServerShorthand = (port: number) =>
  definedServers.find(server => server.port === port)?.shorthand || 0

export function ServerList({ intl, servers, gameId }: ServerListProps) {
  const findServer = (port: number) => servers.find(server => server.port === port)

  return (
    <div className={styles.container}>
      {definedServers.map(serverInfo => {
        const server = findServer(serverInfo.port)

        if (!server) {
          return null
        }

        return (
          <ServerCard
            intl={intl}
            key={serverInfo.port}
            highlight={needToHighlight(gameId, serverInfo.port)}
            serverInfo={serverInfo}
            server={server}
            shorthand={getServerShorthand(serverInfo.port)}
            players={server.players}
          />
        )
      })}
    </div>
  )
}
