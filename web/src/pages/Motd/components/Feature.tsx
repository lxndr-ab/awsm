import { useIntl } from 'react-intl'
import styles from './Feature.module.scss'

const baseIconPath = 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps';

const featureIcons = {
  gchat: `${baseIconPath}/550/3e5f908b03356376b53cfc3ea3f7e9eda974be89.jpg`,
  pchat: `${baseIconPath}/550/fa4fd2c60d65d296246176a2fed1f2437b249848.jpg`,
  drop: `${baseIconPath}/500/2c13cc02bd431b6b68b63bde9e6352b9d78c83c2.jpg`,
  gear: `${baseIconPath}/500/d676eb919ada11fb8391adb8ff670f53afac3bb1.jpg`,
  campaign: `${baseIconPath}/550/265b783c18fba0a17b892865e8f033f492317c14.jpg`,
  campaign2: `${baseIconPath}/550/265b783c18fba0a17b892865e8f033f492317c14.jpg`,
  gamemode: `${baseIconPath}/500/81adb8d4429a28d3623b9a76d5a841702b8c4381.jpg`,
  custom: `${baseIconPath}/500/d3e49e30a1c7670cffb690b1b794d39b30f3edbc.jpg`,
  tricks: `${baseIconPath}/550/02d01200d3f14ba7a1145366397ff124c1a45464.jpg`,
  shutup: `${baseIconPath}/550/09bb54c6930bc815b40930b228f63e897c6018b3.jpg`,
  csm: `${baseIconPath}/500/e93e3166452a18b79aed641f611eb12d3dffb503.jpg`,
  moreplayers: `${baseIconPath}/500/27422b28a815ce1ac48d323042317f2bb21b2d77.jpg`,
  perks: `${baseIconPath}/500/6484088ab9e0eb3b94974d04235acfe598339749.jpg`,
  fixes: `${baseIconPath}/500/c97874fff49b3d22a4be7c34240fb1ad185a6a6e.jpg`,
}

interface FeatureProps {
  id: string
}

export function Feature({ id }: FeatureProps) {
  const intl = useIntl()
  const title = intl.formatMessage({ id: `${id}Title` })
  const description = intl.formatMessage({ id: `${id}Desc` })

  return (
    <div className={styles.container}>
      <div
        className={styles.icon}
        style={{ backgroundImage: `url("${featureIcons[id]}")` }}
      />

      <div className={styles.content}>
        <h3 className={styles.title}>{title}</h3>
        <p className={styles.description}>{description}</p>
      </div>
    </div>
  )
}
