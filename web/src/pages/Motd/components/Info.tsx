import { IntlShape } from 'react-intl'
import styles from './Info.module.scss'

interface InfoProps {
  intl: IntlShape
  title: string
  content: string
}

export function Info({ intl, title, content }: InfoProps) {
  const titleText = intl.formatMessage({ id: title })
  const descriptionText = intl.formatMessage({ id: content })

  return (
    <div className={styles.container}>
      <h2 className={styles.title}>{titleText}</h2>
      <div className={styles.content}>{descriptionText}</div>
    </div>
  )
}
