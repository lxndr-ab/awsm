import { IntlShape } from 'react-intl'
import { Player } from '@prisma/client'
import ms from 'ms'
import cn from 'classnames'
import { SerializableServer } from '~/services/db'
import { ServerInfo } from '~/util/servers'
import styles from './ServerCard.module.scss'

interface ServerCardProps {
  intl: IntlShape
  highlight: boolean
  serverInfo: ServerInfo
  server: SerializableServer
  shorthand: number
  players: Player[]
}

export function ServerCard({ intl, highlight, serverInfo, server, shorthand, players }: ServerCardProps) {
  const { gamemode, difficulty, map, numPlayers, maxPlayers, updatedAt } = server
  const timeout = (Date.now() - new Date(updatedAt).getTime()) > ms('1m')

  return (
    <div className={cn(styles.container, { [styles.highlight]: highlight })}>
      <div className={styles.infoWrapper}>
        <h2 className={styles.serverName}>
          <span className={styles.serverNumber}>#{shorthand}</span>
          &nbsp;
          {serverInfo.name}
        </h2>
        <div>
          <a className={styles.serverAddress} href={`steam://connect/${serverInfo.host}:${serverInfo.port}`}>
            {`${serverInfo.host}:${serverInfo.port}`}
          </a>
        </div>
        {timeout && <div className={styles.timeout}>timeout</div>}
        <div className={styles.gameMode}>
          {[gamemode, difficulty].filter(Boolean).join(' | ')}
        </div>
        <div className={styles.mapName}>
          {map}
        </div>
      </div>

      <div className={styles.playerCount}>
        {intl.formatMessage({ id: 'playerCount' }, { numPlayers, maxPlayers })}
      </div>

      <ul className={styles.playerList}>
        {players.map(({ name }) => <li key={name}>{name}</li>)}
      </ul>
    </div>
  )
}
