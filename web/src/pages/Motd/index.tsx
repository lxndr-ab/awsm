import Head from 'next/head'
import { useSearchParams } from 'next/navigation'
import { useIntl } from 'react-intl'
import { SerializableServer } from '~/services/db'
import { ErrorMessage } from '~/components/ErrorMessage'
import { Page, PageTitle, PageSeparator } from '~/components/Page'
import { Header } from '~/components/Header'
import { Info } from './components/Info'
import { Feature } from './components/Feature'
import { ServerList } from './components/ServerList'
import styles from './MotdPage.module.scss'

export interface MotdPageProps {
    servers: SerializableServer[]
    error?: string
}

export function MotdPage({ servers, error }: MotdPageProps) {
  const intl = useIntl()
  const searchParams = useSearchParams()
  const gameId = searchParams.get('gameId') || 'l4d'

  return (
    <Page>
      <Head>
        <title>Awesome Servers Here!</title>
      </Head>

      <Header intl={intl} />

      <PageTitle>
        {intl.formatMessage({ id: 'welcome' })}
      </PageTitle>

      {error
        ? <ErrorMessage>{error}</ErrorMessage>
        : <ServerList intl={intl} servers={servers} gameId={gameId} />}

      <PageSeparator />

      <div className={styles.featureList}>
        <Feature id="gchat" />
        <Feature id="pchat" />
        {gameId === 'l4d' && <Feature id="drop" />}
        {gameId === 'l4d' && <Feature id="gear" />}
        <Feature id="campaign" />
        <Feature id="custom" />
        <Feature id="gamemode" />
        <Feature id="csm" />
        <Feature id="shutup" />
        <Feature id="moreplayers" />
        {gameId === 'l4d' && <Feature id="perks" />}
        {gameId === 'l4d' && <Feature id="fixes" />}
        <Feature id="tricks" />
      </div>

      <PageSeparator />

      <div className={styles.infoBlock}>
        <Info intl={intl} title="rulesTitle" content="rules" />
        <Info intl={intl} title="contactsTitle" content="contacts" />
        <Info intl={intl} title="noteTitle" content="note" />
      </div>
    </Page>
  )
}
