import { Player, BanEntry, PrismaClient, Server } from '@prisma/client'

export const prisma = new PrismaClient();

export interface ServerWithPlayers extends Server {
  players: Player[]
}

export interface SerializableServer extends Omit<ServerWithPlayers, 'updatedAt'> {
  updatedAt: string
}

export interface SerializableBanEntry extends Omit<BanEntry, 'startTime' | 'endTime'> {
  startTime: string
  endTime: string
}
