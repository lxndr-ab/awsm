import { Player, Prisma, PrismaClient } from '@prisma/client'
import type { QueryResult } from 'gamedig'
import Gamedig from 'gamedig'
import Koa from 'koa'
import Router from '@koa/router'
import DayJS from 'dayjs'
import ms from 'ms'

interface RawQueryResult {
  appId: number
  game: string
  numplayers: number
  tags: string[]
}

const PORT = Number(process.env['PORT'] || '27004')
const UPDATE_INTERVAL = ms(process.env['UPDATE_INTERVAL'] || '5s')
const SERVERS = process.env['SERVERS'] || ''

const prisma = new PrismaClient()
const l4dGameModeRe = /^L4D - (.+) -/i
const l4dGameDiffRe = /^L4D - .+ -(.+)$/i

const router = new Router()
  .get('/ping', async ctx => {
    ctx.status = 200
  })
  .get('/servers/:port/check', async ctx => {
    const count = await prisma.server.count({
      where: {
        port: parseInt(ctx.params['port'] as string, 10),
        updatedAt: {
          gt: DayJS().subtract(1, 'minute').toISOString(),
        },
      },
    })

    ctx.status = count ? 200 : 418
  })

new Koa()
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(PORT)

const queryServer = async (host: string, port: number) => {
  const inf = await Gamedig.query({ type: 'left4dead2', host, port })
  const raw = inf.raw as RawQueryResult

  const players = inf.players
    .filter(player => !!player.name)
    .map<Player>(player => ({
      port,
      name: player.name!,
      score: (player.raw as any).score,
      time: (player.raw as any).time,
    }))

  const server: Prisma.ServerCreateInput = {
    port,
    name: inf.name,
    map: inf.map,
    numPlayers: raw.numplayers,
    maxPlayers: inf.maxplayers,
    gamemode: getGameMode(inf),
    difficulty: getDifficulty(inf),
  }

  await prisma.$transaction([
    prisma.server.upsert({
      where: { port },
      create: server,
      update: server,
    }),
    prisma.player.deleteMany({
      where: { port },
    }),
    prisma.player.createMany({
      data: players,
    }),
  ])
}

const getGameMode = (inf: QueryResult) => {
  const raw = inf.raw as RawQueryResult

  switch (raw.appId) {
    case 500: // Left 4 Dead
      const m = raw.game.match(l4dGameModeRe)
      return m?.[1] || null
    case 550: // Left 4 Dead 2
      const modes: Record<string, string> = {
        coop: 'Co-op',
        realism: 'Realism',
        survival: 'Survival',
        versus: 'Versus',
        scavenge: 'Scavenge',
      }

      const mode = raw.tags.find(tag => tag in modes)

      if (mode && mode in modes) {
        return modes[mode] || null
      }

      return 'Unknown gamemode'
    default:
      return 'Unknown gamemode'
  }
}

const getDifficulty = (inf: QueryResult) => {
  const raw = inf.raw as RawQueryResult

  switch (raw.appId) {
    case 500: // Left 4 Dead
      const m = raw.game.match(l4dGameDiffRe)
      return m?.[1] || null
    default:
      return null
  }
}

const queryServers = () => {
  SERVERS
    .split(',')
    .filter(Boolean)
    .map(address => {
      const [host, portStr] = address.split(':')

      if (!(host && portStr)) {
        throw new Error('Invalid server address')
      }

      const port = parseInt(portStr, 10)

      if (!port) {
        throw new Error('Invalid server address')
      }

      queryServer(host, port).catch(console.warn)
    })
}

queryServers()

setInterval(() => {
  queryServers()
}, UPDATE_INTERVAL)
