#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <sdktools>
#include <awsm/util>

public Plugin myinfo = {
    name = "[AWSM] I am feeling lucky",
    author = "lxndr",
    version = "1.0",
}

public void OnPluginStart()
{
    LoadTranslations ("luck.phrases");

    RegConsoleCmd("say", Command_Say);
    RegConsoleCmd("say_team", Command_Say);
}

Action Command_Say(int client, int args)
{
    if (IsCompetitiveMode()) {
        return Plugin_Continue;
    }

    char text[MAX_CHAT_LENGTH];
    GetCmdArgString(text, sizeof(text));
    StripQuotes(text);

    if (StrEqual(text, "i am feeling lucky", false)) {
        if (IsPlayerAlive(client) && GetClientTeam(client) == TEAM_SURVIVOR) {
            if (client && !IsVoteInProgress()) {
                Menu voteMenu = CreateMenu(Menu_LuckVote, MenuAction_Display | MenuAction_DisplayItem | MenuAction_VoteEnd);
                SetMenuTitle(voteMenu, "Are you feeling lucky too?");
                AddMenuItem(voteMenu, "yes", "Hell yeah!");
                AddMenuItem(voteMenu, "no", "Moooommm!!!");
                SetMenuExitButton(voteMenu, false);
                VoteMenuToAll(voteMenu, 10);
            }
        } else {
            PrintToChat(client, "%t", "you are dead");
        }
    }

    return Plugin_Continue;
}

int Menu_LuckVote(Menu voteMenu, MenuAction action, int param1, int param2)
{
    switch (action) {
        case MenuAction_Display: {
            int client = param1;
            Panel panel = view_as<Panel>(param2);
            char title[256];
            Format(title, sizeof(title), "%T", "are you lucky", client);
            SetPanelTitle(panel, title);
        }

        case MenuAction_DisplayItem: {
            int client = param1;
            int item = param2;
            char title[64];
            Format(title, sizeof(title), "%T", item == 0 ? "lucky yes" : "lucky no", client);
            return RedrawMenuItem(title);
        }

        case MenuAction_VoteEnd: {
            int reason = param1;

            if (reason == 0) {
                PrintToChatAll ("%t", "folks want blood");
                CreateTimer (GetRandomFloat (3.0, 8.0), Timer_Luck);
            }
        }

        case MenuAction_End: {
            delete voteMenu;
        }
    }

    return 0;
}

Action Timer_Luck(Handle timer)
{
    int[] players = new int[MaxClients];
    int count = 0;

    for (int i = 1; i <= MaxClients; i++) {
        if (IsClientInGame(i) && IsPlayerAlive(i)) {
            players[count] = i;
            count++;
        }
    }

    int victim = players[GetRandomInt(0, count - 1)];
    PrintToChatAll("%t", "wasnt so lucky", victim);

    ForcePlayerSuicide(victim);
    return Plugin_Stop;
}
