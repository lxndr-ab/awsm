#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <awsm/util>

public Plugin myinfo = {
    name = "[AWSM] Disabre's hardcore config",
    author = "disabre,lxndr",
    version = "1.0",
};


ConVar vDisabreHardcore;


public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
    if (GetEngineVersion() == Engine_Left4Dead) {
        return APLRes_Success;
    }
    
    return APLRes_SilentFailure;
}

public void OnPluginStart()
{
    vDisabreHardcore = CreateConVar("disabre_hardcore", "0");
    HookConVarChange(vDisabreHardcore, DisabreHardcoreChanged);
}

public void OnMapStart()
{
	if (vDisabreHardcore.BoolValue) {
		EnableDisabreHardcore ();
    }
}

Action ExecSilentyFinish(Handle timer, any data)
{
    FindConVar("sm_show_activity").IntValue = view_as<int>(data);
    return Plugin_Stop;
}

void ExecSilently(const char[] cmd)
{
	ConVar vShowActivity = FindConVar("sm_show_activity");
	vShowActivity.IntValue = 0;
	ServerCommand(cmd);
	CreateTimer(0.1, ExecSilentyFinish, vShowActivity.IntValue);
}

void EnableDisabreHardcore()
{
	if (IsCompetitiveMode())
		ExecSilently ("exec menu/hcvson.cfg");
	else
		ExecSilently ("exec menu/hcon.cfg");
}

void DisabreHardcoreChanged(ConVar convar, const char[] oldValue, const char[] newValue)
{
	if (vDisabreHardcore.BoolValue) {
		EnableDisabreHardcore();
	} else {
		ExecSilently("exec menu/hcoff.cfg");
		ExecSilently("exec menu/hcvsoff.cfg");
	}
}
