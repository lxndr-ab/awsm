#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <awsm/yesno-vote>

public Plugin myinfo = {
    name = "Shut up!",
    description = "This plugin allows players to shut somebody up",
    author = "lxndr",
    version = "1.3"
}

ArrayList IDs;
char victim_name[MAX_NAME_LENGTH];
char victim_id[32];

public void OnPluginStart()
{
    LoadTranslations("common.phrases.txt");
    LoadTranslations("shutup.phrases.txt");

    RegConsoleCmd("sm_shutup", Command_ShutUp);

    IDs = new ArrayList(32);
}

public void OnPluginEnd()
{
    delete IDs;
}

public Action Command_ShutUp(int client, int args)
{
    if (client == 0) {
        return Plugin_Handled;
    }

    if (IsVoteInProgress() || !NativeVotes_IsNewVoteAllowed()) {
        PrintToChat(client, "\x01[\x04SM\x01] %t", "Vote in Progress");
        return Plugin_Handled;
    }

    if (args >= 1) {
        /* player is specified */
        char target_arg[MAX_TARGET_LENGTH];
        GetCmdArg(1, target_arg, sizeof(target_arg));

        int target = FindTarget(client, target_arg, true, false);

        if (target == -1) {
            return Plugin_Handled;
        }

        GetClientName(target, victim_name, sizeof(victim_name));
        GetClientAuthId(target, AuthId_Engine, victim_id, sizeof(victim_id), true);
        StartVote();
    } else {
        /* show player list menu */
        Menu menu = new Menu(Menu_PlayerList);

        char title[64], id[32], name[MAX_NAME_LENGTH];
        Format(title, sizeof(title), "%T:", "choose", client);
        menu.SetTitle(title);

        for (int i = 1; i <= MaxClients; i++) {
            if (IsClientInGame(i) && !IsFakeClient(i)) {
                GetClientName(i, name, sizeof(name));
                GetClientAuthId(i, AuthId_Engine, id, sizeof(id), true);
                menu.AddItem(id, name);
            }
        }

        menu.Display(client, 15);
    }

    return Plugin_Handled;
}

public int Menu_PlayerList(Menu playerMenu, MenuAction action, int client, int param)
{
    if (action == MenuAction_Select) {
        playerMenu.GetItem(param, victim_id, sizeof(victim_id), _, victim_name, sizeof(victim_name));
        StartVote();
    } else if (action == MenuAction_End) {
        delete playerMenu;
    }

    return 0;
}

YesNoVoteResult VoteSuccessHandler(YesNoVoteResult result, char[] msg, int msg_size)
{
    if (result == VoteResult_Yes) {
        Format(msg, msg_size, "%t", "shut up", victim_name);
        SilencePlayer();
    } else {
        Format(msg, msg_size, "%t", "can speak", victim_name);
        UnsilencePlayer();
    }

    return VoteResult_Yes;
}

void VoteTitleRenderer(int client, char[] label, int label_size)
{
    Format(label, label_size, "%T", "do you want", client, victim_name);
}

void StartVote()
{
    ShowYesNoVote(0, VoteSuccessHandler, VoteTitleRenderer);
}

public void OnClientPutInServer(int client)
{
    if (IsFakeClient(client)) {
        return;
    }

    char authid[32];
    GetClientAuthId(client, AuthId_Engine, authid, sizeof(authid), true);

    int idx = IDs.FindString(authid);

    if (idx != -1) {
        ServerCommand("sm_silence #%d", GetClientUserId(client));
    }
}

int FindTargetUserId()
{
    char authid[32];

    for (int i = 1; i <= MaxClients; i++) {
        if (IsClientInGame(i) && !IsFakeClient(i)) {
            GetClientAuthId(i, AuthId_Engine, authid, sizeof(authid), true);

            if (StrEqual(authid, victim_id)) {
                return GetClientUserId(i);
			}
        }
    }

    return 0;
}

void SilencePlayer()
{
    int userid = FindTargetUserId();

    if (userid) {
        ServerCommand("sm_silence #%d", userid);
    }

    if (IDs.FindString(victim_id) == -1) {
        IDs.PushString(victim_id);
    }
}

void UnsilencePlayer()
{
    int userid = FindTargetUserId();

    if (userid) {
        ServerCommand("sm_unsilence #%d", userid);
    }

    int idx = IDs.FindString(victim_id);

    if (idx != -1) {
        IDs.Erase(idx);
    }
}
