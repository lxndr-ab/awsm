#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>

public Plugin myinfo = {
    name = "[AWSM] Private Chat",
    author = "lxndr",
    version = "1.2.1",
    description = "Allows players to chat privately"
}

public void OnPluginStart()
{
    LoadTranslations("common.phrases");
    RegConsoleCmd("sm_p", Command_P, "sm_p <name or #userid> <message> - sends private message");
}

public Action Command_P(int client, int args)
{
    if (args < 2) {
        ReplyToCommand(client, "[SM] Usage: sm_p <name or #userid> <message>");
        return Plugin_Handled;
    }

    char arg[256], target_name[128];
    char from[MAX_NAME_LENGTH], to[MAX_NAME_LENGTH];

    GetCmdArgString(arg, sizeof(arg));
    int msg_index = BreakString(arg, target_name, sizeof(target_name));

    if (msg_index == -1) {
        ReplyToCommand(client, "[SM] Message was not provided");
        return Plugin_Handled;
    }

    int target = FindTarget(client, target_name, true, false);

    if (target == -1) {
        return Plugin_Handled;
    }

    GetClientName(target, to, sizeof(to));

    if (client == 0) {
        from = "Console";
        PrintToServer("To %s from %s: %s", to, from, arg[msg_index]);
    } else {
        GetClientName(client, from, sizeof(from));
        PrintToChat(client, "\x01To \x05%s\x01 from \x04%s\x01: %s", to, from, arg[msg_index]);
    }

    PrintToChat(target, "\x01To \x04%s\x01 from \x05%s\x01: %s", to, from, arg[msg_index]);

    return Plugin_Handled;
}
