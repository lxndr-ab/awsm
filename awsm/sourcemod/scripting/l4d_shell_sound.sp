#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <sdktools>

/*
 * Only Left 4 Dead 1 supported.
 * Left 4 Dead 2 does not have any shell sounds.
 */


public Plugin myinfo = {
    name = "[AWSM] Shell sound",
    author = "lxndr",
    description = "Adds sound for shutgun and sniper rifle shells falling on the ground",
    version = "2.1"
}


Handle timers[MAXPLAYERS + 1];

char shotgun_sounds[3][] = {
    "weapons/fx/tink/shotgun_shell1.wav",
    "weapons/fx/tink/shotgun_shell2.wav",
    "weapons/fx/tink/shotgun_shell3.wav"
};

char rifle_sounds[3][] = {
    "weapons/fx/tink/pl_shell1.wav",
    "weapons/fx/tink/pl_shell2.wav",
    "weapons/fx/tink/pl_shell3.wav"
};


public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
    if (GetEngineVersion() == Engine_Left4Dead) {
        return APLRes_Success;
    }
    
    return APLRes_SilentFailure;
}


public void OnPluginStart()
{
    HookEvent("weapon_fire", Event_WeaponFire);
}


public void OnMapStart()
{
    for (int i = 0; i < MAXPLAYERS; i++)
        timers[i] = null;
}


public void Event_WeaponFire(Handle event, const char[] name, bool dontBroadcast)
{
    char weapon[64];
    GetEventString(event, "weapon", weapon, sizeof(weapon));
    
    if (StrEqual(weapon, "pumpshotgun") || StrEqual(weapon, "autoshotgun")) {
        int client = GetClientOfUserId(GetEventInt(event, "userid"));

        if (timers[client] == null) {
            timers[client] = CreateTimer(0.3, EmitShotgun, client, TIMER_FLAG_NO_MAPCHANGE);
        }
    } else if (StrEqual (weapon, "hunting_rifle")) {
        int client = GetClientOfUserId(GetEventInt(event, "userid"));

        if (timers[client] == null) {
            timers[client] = CreateTimer(0.3, EmitRifle, client, TIMER_FLAG_NO_MAPCHANGE);
        }
    }
}


public Action EmitShotgun(Handle timer, int client)
{
    Emit(client, shotgun_sounds[GetRandomInt(0, 2)]);
    return Plugin_Stop;
}


public Action EmitRifle(Handle timer, int client)
{
    Emit(client, rifle_sounds[GetRandomInt(0, 2)]);
    return Plugin_Stop;
}


void Emit(int client, const char[] sound)
{
    int clients[MAXPLAYERS];
    int total = 0;

    for (int i = 1; i <= MaxClients; i++) {
        if (IsClientInGame (i) && !IsFakeClient (i)) {
            clients[total++] = i;
        }
    }

    if (total == 0) {
        return;
    }

    EmitSound(clients, total, sound, client,
                SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, 0.6);

    timers[client] = null;
}
