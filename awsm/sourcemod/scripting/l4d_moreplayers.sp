#include <sourcemod>
#include <sdktools>

#pragma semicolon 1

#define VERSION "1.2.1"
#define DEBUG 0
#define MAXPLAYERS_VAR "l4d_maxplayers"

public Plugin:myinfo = {
	name = "More players",
	author = "lxndr",
	description = "Makes a 4+ server",
	version = VERSION
};

new Handle:vEnabled;
new Handle:vMinSurvivors;
new Handle:cRespawn;

public OnPluginStart ()
{
	LoadTranslations("common.phrases");
	
	new Handle:conf = LoadGameConfigFile ("moreplayers");
	if (conf == INVALID_HANDLE) {
		LogError ("couldn't load game config file 'gamedata/moreplayers.txt'");
		return;
	}
	
	StartPrepSDKCall (SDKCall_Player);
	PrepSDKCall_SetFromConf (conf, SDKConf_Signature, "RoundRespawn");
	cRespawn = EndPrepSDKCall ();
	if (cRespawn == INVALID_HANDLE) {
		LogError ("RoundRespawn signature is broken");
		return;
	}
	
	vEnabled = CreateConVar ("l4d_mp_enable", "0", "Enable this plugin", FCVAR_PLUGIN);
	vMinSurvivors = CreateConVar ("l4d_mp_min_survivors", "4", "Minimum survivors", FCVAR_PLUGIN, true, 1.0, true, 32.0);
	
	HookConVarChange (vEnabled, EnabledChanged);
	
	RegAdminCmd ("sm_addbot", Command_AddBot, ADMFLAG_KICK, "sm_addbot <target_to_put_at>");
}

public EnabledChanged (Handle:convar, const String:oldValue[], const String:newValue[])
{
	if (GetConVarBool (vEnabled)) {
		HookEvent ("player_death", PlayerDeath);
		HookEvent ("player_activate", PlayerActivate);
		HookEvent ("finale_vehicle_leaving", FinaleVehicleLeaving);
	} else {
		UnhookEvent ("player_death", PlayerDeath);
		UnhookEvent ("player_activate", PlayerActivate);
		UnhookEvent ("finale_vehicle_leaving", FinaleVehicleLeaving);
	}
}

public PlayerDeath (Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId (GetEventInt (event, "userid"));
	
	// checking if it is a survivor bot
	if (!(client > 0 && IsClientInGame (client) && IsFakeClient (client) &&
			GetClientTeam (client) == 2))
		return;
	
	// checking if this bot has idle player
	new owner = GetClientOfUserId (GetEntProp (client, Prop_Send, "m_humanSpectatorUserID"));
	if (owner > 0 && IsClientInGame (owner) && !IsFakeClient (owner) && GetClientTeam (client) == 1)
		return;
	
	// checking if no-one needs this dead bot
	new minimum = GetConVarInt (vMinSurvivors);
	new players = GetNumberOfPlayers ();
	new survivors = GetNumberOfSurvivors ();
	if (survivors > minimum && survivors > players)
		KickClient (client);
}

public PlayerActivate (Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId (GetEventInt (event, "userid"));
	
	if (!IsFakeClient (client) && GetNumberOfPlayers () > GetNumberOfSurvivors ()) {
#if DEBUG
		if (client != 0)
			LogMessage ("%L is activated. Players: %d. Survivors: %d.",
					client, GetNumberOfPlayers (), GetNumberOfSurvivors ());
#endif
		CreateBot ();
	}
}

public FinaleVehicleLeaving (Handle:event, const String:name[], bool:dontBroadcast)
{
	decl String:mapname[64];
	GetCurrentMap (mapname, sizeof (mapname));
	
	// there is no vehicle in this map
	// so we do not move the players
	if (StrEqual (mapname, "l4d_river03_port"))
		return;
	
#if DEBUG
	LogMessage ("Final vehicle is leaving.");
#endif
	
	for (new i = 1; i <= MaxClients; i++) {
		if (IsClientInGame (i) && GetClientTeam (i) == 2 && IsPlayerAlive (i)) {
			SetEntProp (i, Prop_Data, "m_takedamage", 1, 1);
			new Float:newOrigin[3] = {0.0, 0.0, 0.0};
			TeleportEntity (i, newOrigin, NULL_VECTOR, NULL_VECTOR);
			SetEntProp (i, Prop_Data, "m_takedamage", 2, 1);
		}
	}
}

public Action:Command_AddBot (client, args)
{
	new at;
	new String:target[64];
	GetCmdArg (1, target, sizeof (target));
	
	if (StrEqual (target, "")) {
		at = 0;
	} else if (StrEqual (target, "@any")) {
		at = GetRandomSurvivor (true);
	} else if ((at = FindTarget (client, target, false, false)) == -1)
		return Plugin_Handled;
	
	CreateBot (at);
	return Plugin_Handled;
}

GetRandomSurvivor (bool:alive)
{
	new count = 0;
	new list[MaxClients];
	
	for (new i = 1; i <= MaxClients; i++) {
		if (IsClientInGame (i) && GetClientTeam (i) == 2 && (!alive || IsPlayerAlive (i))) {
			list[count] = i;
			count++;
		}
	}
	
	return list[GetRandomInt (0, count - 1)];
}

GetNumberOfPlayers ()
{
	new count = 0;
	for (new i = 1; i <= MaxClients; i++)
		if (IsClientInGame (i) && !IsFakeClient (i))
			count++;
	return count;
}

GetNumberOfSurvivors ()
{
	new count = 0;
	for (new i = 1; i <= MaxClients; i++)
		if (IsClientInGame (i) && GetClientTeam (i) == 2)
			count++;
	return count;
}

CreateBot (at = 0)
{
	new bot = CreateFakeClient ("FakeClient");
	if (bot == 0) {
		LogError ("could not create a fake client");
		return;
	}
	
	ChangeClientTeam (bot, 2);
	DispatchKeyValue (bot, "classname", "survivorbot");
	DispatchSpawn (bot);
	
	if (at != 0) {
		SDKCall (cRespawn, bot);
		new Float:origin[3];
		GetClientAbsOrigin (at, origin);
		TeleportEntity (bot, origin, NULL_VECTOR, NULL_VECTOR);
	}
	
	KickClient (bot);
}
