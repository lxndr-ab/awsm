#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <awsm/maps>

public Plugin myinfo = {
    name = "[AWSM] Misc",
    author = "lxndr",
    version = "1.0",
};

Maps cfg;

public void OnPluginStart()
{
    cfg = new Maps();

    /* Fake "state" variables */
    CreateConVar("evil_witch_state", "0");
    CreateConVar("perks_state", "0");
}

public void OnMapStart() {
    SetupCsmSurvivors();
}

// disable l4d2 survivors on l4d1 survivors campaigns
// https://forums.alliedmods.net/showthread.php?p=969651
void SetupCsmSurvivors() {
    if (GetEngineVersion() == Engine_Left4Dead2) {
        ConVar v = FindConVar("l4d_csm_l4d1_survivors");

        if (!v) {
            PrintToServer("Failed to find 'l4d_csm_l4d1_survivors' variable");
            return;
        }

        char mapname[64], campaign[64];
        GetCurrentMap(mapname, sizeof(mapname));

        if (cfg.FindCampaignByMap(campaign, sizeof(campaign), mapname)) {
            cfg.GotoCampaign(campaign);
            int survivor_set = cfg.GetNum("survivor_set", 1);
            v.IntValue = survivor_set == 1 ? 2 : 1;
        }

        delete v;
    }
}
