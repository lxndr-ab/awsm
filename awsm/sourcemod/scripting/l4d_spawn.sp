#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <sdktools>
#include <sdktools_functions>

public Plugin myinfo = {
    name = "[AWSM] Object Spawner",
    author = "lxndr",
    version = "0.4.3",
}

char infected[9][] = {
    // L4D
    "tank", "witch", "hunter", "boomer", "smoker", "zombie",
    // L4D2
    "spitter", "jockey", "charger",
};

char items[44][] = {
    // L4D
    "autoshotgun", "first_aid_kit", "pipe_bomb", "molotov", "rifle", "smg", "hunting_rifle",
    "pain_pills", "pistol", "pumpshotgun", "gascan", "propanetank", "oxygentank",
    // L4D2
    "pistol_magnum", "shotgun_chrome", "shotgun_spas", "smg_mp5", "smg_silenced", "rifle_ak47",
    "rifle_sg552", "rifle_m60", "rifle_desert", "sniper_military", "sniper_awp", "sniper_scout",
    "weapon_grenade_launcher", "vomitjar", "chainsaw", "frying_pan", "electric_guitar", "katana",
    "machete", "tonfa", "fireaxe", "defibrilator", "adrenaline", "baseball_bat", "cricket_bat",
    "crowbar", "golfclub", "pitchfork", "shovel", "riotshield", "knife",
};

public void OnPluginStart()
{
    RegAdminCmd("sm_spawn", Command_Spawn, ADMFLAG_GENERIC, "sm_spawn <item>");
    RegAdminCmd("sm_unspawn", Command_Unspawn, ADMFLAG_GENERIC);
}

public void OnMapStart()
{
    PrecacheModel("models/w_models/weapons/w_minigun.mdl", true);
    PrecacheModel("models/w_models/weapons/50cal.mdl", true);
    PrecacheModel("models/props_industrial/barrel_fuel.mdl", true);
    PrecacheModel("models/props_industrial/barrel_fuel_partb.mdl", true);
    PrecacheModel("models/props_industrial/barrel_fuel_parta.mdl", true);
    PrecacheModel("models/props/terror/ammo_stack.mdl", true);
    PrecacheModel("models/props_unique/spawn_apartment/coffeeammo.mdl", true);
}

public Action Command_Unspawn(int client, int args)
{
    int target = GetClientAimTarget(client, false);

    if (target && EntRefToEntIndex(target) != INVALID_ENT_REFERENCE) {
        char item[64];
        GetEdictClassname(target, item, sizeof(item));

        if (
            StrEqual(item, "prop_minigun") ||
            StrEqual(item, "prop_mounted_machine_gun") ||
            StrEqual(item, "prop_fuel_barrel")
        ) {
            AcceptEntityInput(target, "kill");
        } else if (StrEqual(item, "weapon_ammo_spawn")) {
            RemoveEdict(target);
        }
    }

    return Plugin_Handled;
}

bool IsInSet(const char[] name, const char[][] set, int count)
{
    for (int i = 0; i < count; i++)
        if (StrEqual(set[i], name))
            return true;

    return false;
}

public Action Command_Spawn(int client, int args)
{
    if (args < 1) {
        ReplyToCommand(client, "[SM] Usage: sm_spawn <type>");
        return Plugin_Handled;
    }

    char type[64];
    GetCmdArg(1, type, sizeof(type));

    int ent;

    if (IsInSet(type, infected, 9)) {
        SpawnInfected(client, type);
        LogMessage("%L spawned '%s'", client, type);
        return Plugin_Handled;
    } else if(StrEqual(type, "horde")) {
        ForcePanicEvent(client);
        LogMessage("%L spawned '%s'", client, type);
        return Plugin_Handled;
    } else if (IsInSet(type, items, 44)) {
        SpawnWeapon (client, type);
        LogMessage("%L spawned '%s'", client, type);
        return Plugin_Handled;
    } else if (StrEqual(type, "minigun")) {
        ent = CreateMinigun();
    } else if (StrEqual(type, "50cal")) {
        ent = Create50cal();
    } else if (StrEqual(type, "barrel")) {
        ent = CreateBarrel();
        PlaceAtAim(ent, client);
        LogMessage("%L spawned '%s'", client, type);
        return Plugin_Handled;
    } else if (StrEqual(type, "ammostack")) {
        ent = CreateAmmoStack();
    } else {
        PrintToChat(client, "[SM] '%s' is unknown object", type);
        return Plugin_Handled;
    }

    LogMessage("%L spawned '%s'", client, type);

    float VecOrigin[3], VecAngles[3], VecDirection[3];
    GetClientAbsOrigin(client, VecOrigin);
    GetClientEyeAngles(client, VecAngles);
    GetAngleVectors(VecAngles, VecDirection, NULL_VECTOR, NULL_VECTOR);
    VecOrigin[0] += VecDirection[0] * 32;
    VecOrigin[1] += VecDirection[1] * 32;
    VecOrigin[2] += VecDirection[2] * 1;
    VecAngles[0] = 0.0;
    VecAngles[2] = 0.0;
    DispatchKeyValueVector(ent, "Angles", VecAngles);
    DispatchSpawn(ent);
    TeleportEntity(ent, VecOrigin, NULL_VECTOR, NULL_VECTOR);

    return Plugin_Handled;
}

void SpawnInfected(int client, const char[] name)
{
    char command[] = "z_spawn";
    int flags = GetCommandFlags(command);
    SetCommandFlags(command, flags & ~FCVAR_CHEAT);
    FakeClientCommand(client, "z_spawn %s", name);
    SetCommandFlags(command, flags);
}

void SpawnWeapon(int client, const char[] name)
{
    char command[] = "give";
    int flags = GetCommandFlags(command);
    SetCommandFlags(command, flags & ~FCVAR_CHEAT);
    FakeClientCommand(client, "give %s", name);
    SetCommandFlags(command, flags);
}

void ForcePanicEvent(int client)
{
    char command[] = "director_force_panic_event";
    int flags = GetCommandFlags(command);
    SetCommandFlags(command, flags & ~FCVAR_CHEAT);
    FakeClientCommand(client, command);
    SetCommandFlags(command, flags);
}

int CreateMinigun()
{
    int ent = CreateEntityByName("prop_minigun");
    DispatchKeyValue(ent, "model", "Minigun_1");
    SetEntityModel(ent, "models/w_models/weapons/w_minigun.mdl");
    DispatchKeyValueFloat(ent, "MaxPitch", 360.00);
    DispatchKeyValueFloat(ent, "MinPitch", -360.00);
    DispatchKeyValueFloat(ent, "MaxYaw", 90.00);
    DispatchSpawn(ent);
    return ent;
}

int Create50cal()
{
    int ent = CreateEntityByName("prop_mounted_machine_gun");
    DispatchKeyValue(ent, "model", "Minigun_1");
    SetEntityModel(ent, "models/w_models/weapons/50cal.mdl");
    DispatchKeyValueFloat(ent, "MaxPitch", 360.00);
    DispatchKeyValueFloat(ent, "MinPitch", -360.00);
    DispatchKeyValueFloat(ent, "MaxYaw", 90.00);
    DispatchSpawn(ent);
    return ent;
}

int CreateBarrel()
{
    int ent = CreateEntityByName("prop_fuel_barrel");
    SetEntityModel(ent, "models/props_industrial/barrel_fuel.mdl");
    DispatchKeyValue(ent, "BasePiece", "models/props_industrial/barrel_fuel_partb.mdl");
    DispatchKeyValue(ent, "FlyingPiece01", "models/props_industrial/barrel_fuel_parta.mdl");
    DispatchKeyValue(ent, "DetonateParticles", "weapon_pipebomb");
    DispatchKeyValue(ent, "FlyingParticles", "barrel_fly");
    DispatchKeyValue(ent, "DetonateSound", "BaseGrenade.Explode");
    DispatchSpawn(ent);
    return ent;
}

int CreateAmmoStack()
{
    int  ent = CreateEntityByName("weapon_ammo_spawn");

    if (GetRandomInt(1, 2) == 1)
        SetEntityModel(ent, "models/props/terror/ammo_stack.mdl");
    else
        SetEntityModel(ent, "models/props_unique/spawn_apartment/coffeeammo.mdl");

    DispatchSpawn(ent);
    return ent;
}

public bool TraceEntityFilterPlayer(int entity, int contentsMask, any data)
{
    return entity > MaxClients && entity != data;
}

bool PlaceAtAim(int entry, int client)
{
    float vOrigin[3];
    GetClientEyePosition(client, vOrigin);

    float vAngles[3];
    GetClientEyeAngles(client, vAngles);

    Handle trace = TR_TraceRayFilterEx(vOrigin, vAngles, MASK_SHOT,
            RayType_Infinite, TraceEntityFilterPlayer);

    if (!TR_DidHit(trace)) {
        CloseHandle(trace);
        return false;
    }

    float vStart[3];
    TR_GetEndPosition(vStart, trace);
    CloseHandle(trace);

    GetVectorDistance(vOrigin, vStart, false);
    float Distance = -35.0;

    float vBuffer[3];
    GetAngleVectors(vAngles, vBuffer, NULL_VECTOR, NULL_VECTOR);

    vOrigin[0] = vStart[0] + (vBuffer[0] * Distance);
    vOrigin[1] = vStart[1] + (vBuffer[1] * Distance);
    vOrigin[2] = vStart[2] + (vBuffer[2] * Distance);
    vAngles[0] = vAngles[2] = 0.0;

    DispatchKeyValueVector(entry, "Origin", vOrigin);
    DispatchKeyValueVector(entry, "Angles", vAngles);
    DispatchSpawn(entry);

    return true;
}
