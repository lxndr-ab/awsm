#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <geoip>
#include <awsm/db>
#include <awsm/util>

public Plugin myinfo = {
    name = "[AWSM] Game Log",
    author = "lxndr",
    description = "This plugin logs game events such as connections, kills, votes and etc.",
    version = "0.9.1",
};

Database g_DB = null;
int g_HostPort;

public void OnPluginStart()
{
    g_DB = OpenDatabase("awsm-log");

    if (!g_DB) {
        return;
    }

    g_HostPort = FindConVar("hostport").IntValue;

    RegConsoleCmd("say", Event_Say);
    RegConsoleCmd("say_team", Event_SayTeam);
    HookEvent("player_connect", Event_PlayerConnect);
    HookEvent("player_disconnect", Event_PlayerDisconnect);
    HookEvent("player_death", Event_PlayerDeath);
    HookEvent("player_changename", Event_PlayerChangeName);
    HookEvent("player_incapacitated", Event_PlayerIncapacitated);
    RegConsoleCmd("callvote", Event_CallVote);
}

public void OnPluginEnd()
{
    if (g_DB) {
        delete g_DB;
    }
}

Action Event_PlayerConnect(Event event, const char[] eventname, bool dontBroadcast)
{
    char name[MAX_NAME_LENGTH], ip[22];
    event.GetString("name", name, sizeof(name));
    event.GetString("address", ip, sizeof(ip));
    int id = event.GetInt("userid");

    if (StrEqual(ip, "") || StrEqual(ip, "none") || dontBroadcast) {
        return Plugin_Continue;
    }

    char city[45], region[45], country_name[45];
    GeoipCity(ip, city, sizeof(city), LANG_SERVER);
    GeoipRegion(ip, region, sizeof(region), LANG_SERVER);
    GeoipCountry(ip, country_name, sizeof(country_name));

    char text[255];
    Format(text, sizeof(text), "ip=%s;country=%s;region=%s;city=%s", ip, country_name, region, city);

    LogGameEvent(id, name, "connect", text);
    return Plugin_Continue;
}

Action Event_PlayerDisconnect(Event event, const char[] eventname, bool dontBroadcast)
{
    char name[MAX_NAME_LENGTH], steamid[32], ip[16], reason[128];

    int id = event.GetInt("userid");
    int client = GetClientOfUserId(id);

    if (client && !IsFakeClient(client) && !dontBroadcast) {
        GetClientName(client, name, sizeof(name));
        GetClientAuthId(client, AuthId_Engine, steamid, sizeof(steamid), true);
        GetClientIP(client, ip, sizeof(ip));
        event.GetString("reason", reason, sizeof(reason));

        char text[255];
        Format(text, sizeof(text), "ip=%s;steamid=%s;reason=%s", ip, steamid, reason);
        LogGameEvent(id, name, "disconnect", text);
    }

    return Plugin_Continue;
}

Action Event_Say(int client, int args)
{
    LogSayEvent(client, "say");
    return Plugin_Continue;
}

Action Event_SayTeam(int client, int args)
{
    LogSayEvent(client, "say-team");
    return Plugin_Continue;
}

void LogSayEvent(int client, const char[] event)
{
    char name[MAX_NAME_LENGTH], text[512];
    int userid;

    if (client == 0) {
        name = "Console";
        userid = 0;
    } else {
        GetClientName(client, name, sizeof(name));
        userid = GetClientUserId(client);
    }

    GetCmdArgString(text, sizeof(text));
    StripQuotes(text);

    LogGameEvent(userid, name, event, text);
}

Action Event_PlayerIncapacitated(Event event, const char[] name, bool dontBroadcast)
{
    /* victim */
    int victim_id = event.GetInt("userid");

    if (victim_id == 0) {
        return Plugin_Continue;
    }

    int victim = GetClientOfUserId(victim_id);

    if (IsFakeClient(victim) && GetClientTeam(victim) != TEAM_SURVIVOR) {
        return Plugin_Continue;
    }

    char victim_name[MAX_NAME_LENGTH];
    GetClientName(victim, victim_name, sizeof(victim_name));

    /* attacker */
    char attacker_name[MAX_NAME_LENGTH];
    int attacker_id = event.GetInt("attacker");

    if (attacker_id > 0) {
        int attacker = GetClientOfUserId(attacker_id);
        GetClientName(attacker, attacker_name, sizeof(attacker_name));
    }

    /* logging */
    char text[255];
    Format(text, sizeof(text), "attacker_id=%d;attacker_name=%s", attacker_id, attacker_name);

    LogGameEvent(victim_id, victim_name, "incapacitated", text);
    return Plugin_Continue;
}

Action Event_PlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
    int victim_id = event.GetInt("userid");

    if (victim_id == 0) {
        return Plugin_Continue;
    }

    int victim = GetClientOfUserId(victim_id);

    if (IsFakeClient(victim) && GetClientTeam(victim) != TEAM_SURVIVOR) {
        return Plugin_Continue;
    }

    char victim_name[MAX_NAME_LENGTH];
    GetClientName(victim, victim_name, sizeof(victim_name));

    char attacker_name[MAX_NAME_LENGTH];
    int attacker_id = event.GetInt("attacker");

    if (attacker_id == 0) {
        event.GetString("attakername", attacker_name, sizeof(attacker_name));
    } else {
        int attacker = GetClientOfUserId(attacker_id);
        GetClientName(attacker, attacker_name, sizeof(attacker_name));
    }

    char text[255];
    Format(text, sizeof(text), "attacker_id=%d;attacker_name=%s", attacker_id, attacker_name);

    LogGameEvent(victim_id, victim_name, "death", text);
    return Plugin_Continue;
}

Action Event_PlayerChangeName(Event event, const char[] eventname, bool dontBroadcast)
{
    char oldname[MAX_NAME_LENGTH], newname[MAX_NAME_LENGTH];
    int userid = event.GetInt("userid");
    event.GetString("oldname", oldname, sizeof(oldname));
    event.GetString("newname", newname, sizeof(newname));
    LogGameEvent(userid, oldname, "namechange", newname);
    return Plugin_Continue;
}

Action Event_CallVote(int client, int args)
{
    int initiator_id = GetClientUserId(client);
    char initiator_name[MAX_NAME_LENGTH];
    GetClientName(client, initiator_name, sizeof(initiator_name));

    char arg[64];
    GetCmdArgString(arg, sizeof(arg));

    LogGameEvent(initiator_id, initiator_name, "callvote", arg);
    return Plugin_Continue;
}

public void OnMapStart()
{
    char mapname[128];
    GetCurrentMap(mapname, sizeof(mapname));
    LogGameEvent(0, "", "map-start", mapname);
}

void LogGameEvent(int player_id, const char[] player_name, const char[] event, const char[] data)
{
    if (!g_DB) {
        return;
    }

    int timestamp = GetTime();

    char safe_player_name[MAX_NAME_LENGTH * 2];
    g_DB.Escape(player_name, safe_player_name, sizeof(safe_player_name));

    char safe_event[64];
    g_DB.Escape(event, safe_event, sizeof(safe_event));

    char safe_data[256];
    g_DB.Escape(data, safe_data, sizeof(safe_data));

    char query[512];
    Format(query, sizeof(query),
        "INSERT INTO `log` VALUES (DEFAULT, FROM_UNIXTIME(%d), %d, %d, '%s', '%s', '%s')",
        timestamp, g_HostPort, player_id, safe_player_name, safe_event, safe_data);

    g_DB.Query(OnEntryInserted, query);
}

void OnEntryInserted(Database db, DBResultSet results, const char[] error, any data)
{
    if (strlen(error)) {
        LogError("Error executing db query: %s", error);
        return;
    }
}
