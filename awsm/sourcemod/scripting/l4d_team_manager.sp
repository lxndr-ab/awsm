/*
	sm_cvar sb_all_bot_game 1
	sm_cvar allow_all_bot_survivor_team 1
	sm_cvar sv_hibernate_when_empty 1
*/

#include <sourcemod>
#include <sdktools>
#include <awesome>

#pragma semicolon 1

#define VERSION "1.0"
#define DEBUG 0


new bool:l4d2 = false;
//new String:players[4][MAXPLAYERS][24];	/* [team][client][steam_id] */
new Handle:fSetHumanSpec = INVALID_HANDLE;
new Handle:fTakeOverBot = INVALID_HANDLE;
new Handle:vBotTeam;


public Plugin:myinfo = {
	name = "Team manager",
	author = "lxndr",
	description = "Keeps players in their teams",
	version = VERSION,
};


/*
	MaxClients = 18
	MAXPLAYERS = 65
*/

public OnPluginStart ()
{
	LoadTranslations ("common.phrases");
	LoadTranslations ("l4d_team_manager.phrases");
	
	/* determine Left 4 Dead version */
	decl String:gameFolder[32];
	GetGameFolderName (gameFolder, sizeof (gameFolder));
	if (StrEqual (gameFolder, "left4dead", false))
		l4d2 = false;
	else if (StrEqual (gameFolder, "left4dead2", false))
		l4d2 = true;
	else {
		LogError ("Team Manager only supports L4D and L4D2.");
		return;
	}
	
	/* Prepare SDK calls */
	new Handle:conf = LoadGameConfigFile ("l4d_team_manager");
	if (conf == INVALID_HANDLE) {
		LogError ("Could not load 'gamedata/l4d_team_manager.txt'");
		return;
	}
	
	StartPrepSDKCall (SDKCall_Player);
	if (PrepSDKCall_SetFromConf (conf, SDKConf_Signature, "SetHumanSpec")) {
		PrepSDKCall_AddParameter (SDKType_CBasePlayer, SDKPass_Pointer);
		fSetHumanSpec = EndPrepSDKCall ();
		if (fSetHumanSpec == INVALID_HANDLE) {
			LogError ("Could not initialize 'SetHumanSpec'");
			return;
		}
	} else {
		LogError ("Could not find 'SetHumanSpec' signature");
	}
	
	StartPrepSDKCall (SDKCall_Player);
	if (PrepSDKCall_SetFromConf (conf, SDKConf_Signature, "TakeOverBot")) {
		PrepSDKCall_AddParameter (SDKType_Bool, SDKPass_Plain);
		fTakeOverBot = EndPrepSDKCall ();
		if (fTakeOverBot == INVALID_HANDLE) {
			LogError ("Could not initialize 'TakeOverBot'");
			return;
		}
	} else {
		LogError ("Cound not find 'TakeOverBot' signature");
	}
	
	vBotTeam = FindConVar (l4d2 ? "sb_all_bot_game" : "sb_all_bot_team");
	
//	HookConVarChange (vGameMode, GameModeChanged);
//	decl String:gm[32];
//	GetConVarString (vGameMode, gm, sizeof (gm));
//	GameModeChanged (vGameMode, "", gm);
	
	RegAdminCmd ("sm_swap", Command_Swap, ADMFLAG_BAN,
			"sm_swap <player1> [player2] ... [playerN] - swap all listed players to opposite teams");
	RegAdminCmd ("sm_swapto", Command_SwapTo, ADMFLAG_BAN,
			"sm_swapto <player1> [player2] ... [playerN] <team> - swap all listed players to <team> (1,2, or 3)");
	RegConsoleCmd ("sm_join", Command_Join);
	
	HookEvent ("player_disconnect", Event_PlayerDisconnect);
}


public Action:Event_PlayerDisconnect (Handle:event, const String:eventname[],
		bool:dontBroadcast)
{
	if (CountHumanPlayers () <= 1)
		SetConVarBool (vBotTeam, false);
	return Plugin_Continue;
}


public GameModeChanged (Handle:cvar, const String:oldval[], const String:newval[])
{
	if (StrEqual (newval, "versus")) {
		HookEvent("round_start", Event_RoundStart);
		HookEvent("round_end", Event_RoundEnd);
		HookEvent("player_team", Event_PlayerTeam);
	} else {
		UnhookEvent("round_start", Event_RoundStart);
		UnhookEvent("round_end", Event_RoundEnd);
		UnhookEvent("player_team", Event_PlayerTeam);
	}
}


public Action:Event_RoundStart (Handle:event, const String:name[], bool:dontBroadcast)
{
#if DEBUG
	LogMessage ("round_start");
#endif
}


public Action:Event_RoundEnd (Handle:event, const String:name[], bool:dontBroadcast)
{
#if DEBUG
	LogMessage ("round_end");
#endif
}


public Action:Event_PlayerTeam (Handle:event, const String:name[], bool:dontBroadcast)
{
#if DEBUG
	LogMessage ("player_team: %L => %d",
			GetClientOfUserId (GetEventInt (event, "userid")),
			GetEventInt (event, "team"));
#endif
}


public Action:Command_Join (client, args)
{
	if (args != 0)
		return Plugin_Continue;
	
	if (IsCompetitiveMode ()) {
		/* TODO: decide where the player is needed */
	} else {
		decl String:error[128];
		if (!ChangeTeam (client, TEAM_SURVIVOR, error, sizeof (error)))
			ReplyToCommand (client, "[TM] %s", error);
	}
	
	return Plugin_Handled;
}


public Action:Command_Swap (client, args)
{
	if (args < 1) {
		ReplyToCommand (client, "%t", "sm_swap usage");
		return Plugin_Handled;
	}
	
	new player_id, i, count = 0;
	decl String:player[64];
	new player_list[MAXPLAYERS];
	new bool:competitive = IsCompetitiveMode ();
	
	/* make player list */
	for (i = 1; i <= args; i++) {
		GetCmdArg (i, player, sizeof (player));
		player_id = FindTarget (client, player, true, false);
		if (player_id == -1)
			continue;
		
		player_list[count] = player_id;
		count++;
	}
	
	/* first swap Survivors */
	for (i = 0; i < count; i++) {
		player_id = player_list[i];
		if (GetRealTeam (player_id) == TEAM_SURVIVOR) {
			/* if game mode does not allow human players to be infected,
				move them to spectators */
			ChangeTeamEx (client, player_id, competitive ? TEAM_INFECTED : TEAM_SPECTATOR);
			player_list[i] = 0;
		}
	}
	
	/* then swap Infected if it is a competitive mode */
	for (i = 0; i < count; i++) {
		player_id = player_list[i];
		if (player_id > 0) {
			if (GetRealTeam (player_id) == (competitive ? TEAM_INFECTED : 0))
				ChangeTeamEx (client, player_id, TEAM_SURVIVOR);
		}
	}
	
	return Plugin_Handled;
}


public Action:Command_SwapTo (client, args)
{
	if (args < 2) {
		ReplyToCommand (client, "%t", "sm_swapto usage");
		return Plugin_Handled;
	}
	
	new player_id, i;
	decl String:teamStr[4], String:player[64];
	new bool:competitive = IsCompetitiveMode ();
	
	GetCmdArg (args, teamStr, sizeof (teamStr));
	new team = StringToInt (teamStr);
	if (team < 1 || team > (competitive ? 3 : 2)) {
		ReplyToCommand (client, "%t", "invalid team", teamStr);
		return Plugin_Handled;
	}
	
	for (i = 1; i < args; i++) {
		GetCmdArg (i, player, sizeof (player));
		player_id = FindTarget (client, player, true, false);
		if (player_id == -1)
			continue;
		
		ChangeTeamEx (client, player_id, team);
	}
	
	return Plugin_Handled;
}


FindSurvivorBot (bool:alive)
{
	new bot = -1;
	
	for (new i = 1; i <= MaxClients; i++) {
		if (IsClientInGame (i) && IsFakeClient (i) &&
				GetClientTeam (i) == TEAM_SURVIVOR && GetBotOwner (i) <= 0) {
			bot = i;
			if (alive && IsPlayerAlive (i))
				break;
		}
	}
	
	return bot;
}


CountHumanPlayers ()
{
	new count = 0;
	
	for (new i = 1; i <= MaxClients; i++)
		if (IsClientInGame (i) && !IsFakeClient (i))
			count++;
	
	return count;
}


ChangeTeamEx (client, target, team)
{
	decl String:error[128];
	static String:to_team[4][32] = {
		"to unknown",
		"to spectators",
		"to survivors",
		"to infected"
	};
	
	if (ChangeTeam (target, team, error, sizeof (error))) {
		LogAction (client, target, "%t", "moved log", client, target, to_team[team]);
		ShowActivityEx (client, "\x01[\x04TM\x01] ", "%t", "moved activity", target, to_team[team]);
	} else {
		ReplyToCommand (client, error);
	}
}


bool:ChangeTeam (client, team, String:error[], maxlen)
{
	if (!IsClientConnected (client) || !IsClientInGame (client)) {
		Format (error, maxlen, "%T", "player not in game", client);
		return false;
	}
	
	if (GetRealTeam (client) == team)
		return true; /* the player is already on the team. not an error */
	
	if (team == TEAM_SURVIVOR) {
		/* looking for a bot */
		new bot = FindSurvivorBot (true);
		
		if (bot == -1) {
			Format (error, maxlen, "%T", "no bots", client);
			return false;
		}
		
		/* killing player's infectd if not a tank */
		new String:class[100];
		GetClientModel (client, class, sizeof (class));
		if (StrContains (class, "hulk", false) == -1)
			ForcePlayerSuicide (client);
		
		/* giving control of a survivor bot */
		ChangeClientTeam (client, TEAM_SURVIVOR);
		SDKCall (fSetHumanSpec, bot, client);
		SDKCall (fTakeOverBot, client, true);
		
	} else if (team == TEAM_INFECTED) {
		ChangeClientTeam (client, TEAM_INFECTED);
		
	} else if (team == TEAM_SPECTATOR) {
		/* FIXME: if a survivor is idle, they considered as a spectator
				and won't change team */
		ChangeClientTeam (client, TEAM_SPECTATOR);
	}
	
	return true;
}
