#pragma semicolon 1
#pragma newdecls required

#include <awsm/servers>
#include <awsm/util>


public Plugin myinfo = {
    name = "[AWSM] Join / Rejoin",
    author = "lxndr",
    version = "1.0",
    description = "sm_join and sm_rejoin commands",
}


StringMap g_serverMap;
char g_CurrentHost[64];


public void OnPluginStart()
{
    g_serverMap = new StringMap();

    LoadConfig();

    RegConsoleCmd("sm_join", Command_Join);
    RegConsoleCmd("sm_rejoin", Command_Rejoin);
}

public void OnPluginEnd()
{
    delete g_serverMap;
}

void LoadConfig()
{
    int current_port = GetConVarInt(FindConVar("hostport"));

    char game_name[8];
    GetGameName(game_name, sizeof(game_name));

    KeyValues kv = OpenServersConfig();

    if (kv.JumpToKey(game_name)) {
        if (kv.GotoFirstSubKey()) {
            char server_name[255], server_horthand[64], server_host[64];

            do {
                kv.GetSectionName(server_name, sizeof(server_name));
                kv.GetString("shorthand", server_horthand, sizeof(server_horthand));
                kv.GetString("host", server_host, sizeof(server_host));
                g_serverMap.SetString(server_horthand, server_host);

                int server_port = ExtractHostPort(server_host);

                if (server_port == current_port) {
                    strcopy(g_CurrentHost, sizeof(g_CurrentHost), server_host);
                }
            } while (kv.GotoNextKey());
        }
    }

    delete kv;
}

public Action Command_Join(int client, int args)
{
    if (args < 1) {
        return Plugin_Handled;
    }

    char arg[4];
    GetCmdArg(1, arg, sizeof(arg));

    char host[64];

    if (g_serverMap.GetString(arg, host, sizeof(host))) {
        ClientCommand(client, "connect %s", host);
    }

    return Plugin_Handled;
}

public Action Command_Rejoin(int client, int args)
{
    ClientCommand(client, "connect %s", g_CurrentHost);
    return Plugin_Handled;
}
