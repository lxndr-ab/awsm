#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <awsm/db>
#include <awsm/util>

#define DEBUG 1
#define MAX_REPORT_REASON_LENGTH 64


public Plugin myinfo = {
    name = "[AWSM] Player information",
    author = "lxndr",
    description = "Panel to show player information",
    version = "1.1",
};


Database g_DB = null;
int selected_targets[MAXPLAYERS + 1];


public void OnPluginStart()
{
    LoadTranslations("common.phrases");
    LoadTranslations("info.phrases");

    g_DB = OpenDatabase("awsm-reports");

    RegConsoleCmd("sm_info", Command_Info);
}

public void OnPluginEnd()
{
    if (g_DB) {
        delete g_DB;
    }
}

Action Command_Info(int client, int args)
{
    if (client == 0) {
        return Plugin_Handled;
    }

    char id[8], name[MAX_NAME_LENGTH], title[128];
    Menu menu = CreateMenu(Menu_Player);

    for (int i = 1; i <= MaxClients; i++) {
        if (IsClientConnected(i) && !IsFakeClient(i)) {
            int userid = GetClientUserId(i);
            IntToString(userid, id, sizeof(id));
            GetClientName(i, name, sizeof(name));

            if (IsPlayerAlive(i)) {
                int health = GetClientHealth(i);
                Format(title, sizeof(title), "%s (%d)", name, health);
            } else {
                Format(title, sizeof(title), "%s (DEAD)", name);
            }

            menu.AddItem(id, title);
        }
    }

    menu.Display(client, 10);
    return Plugin_Handled;
}

int Menu_Player(Menu menu, MenuAction action, int param1, int param2)
{
    switch (action) {
        case MenuAction_Select: {
            int client = param1;
            int menu_item = param2;

            char id[8];
            menu.GetItem(menu_item, id, sizeof(id));
            int target_userid = StringToInt(id, 10);
            selected_targets[client] = target_userid;

#if DEBUG
            LogMessage("target selected %d", target_userid);
#endif

            ShowActionMenu(client);
        }

        case MenuAction_End: {
            delete menu;
        }
    }

    return 0;
}

void ShowActionMenu(int client)
{
    Menu menu = CreateMenu(Menu_Action);
    menu.AddItem("silence", "Silence");
    menu.AddItem("kick", "Kick");
    menu.AddItem("report", "Report");
    menu.Display(client, 10);
}

int Menu_Action(Menu menu, MenuAction action, int param1, int param2)
{
    switch (action) {
        case MenuAction_Select: {
            int client = param1;
            int menu_item = param2;
            int target_userid = selected_targets[client];

            char action_id[8];
            menu.GetItem(menu_item, action_id, sizeof(action_id));

#if DEBUG
            LogMessage("action selected %s for client %d", action_id, target_userid);
#endif

            if (StrEqual(action_id, "silence")) {
                FakeClientCommand(client, "sm_shutup #%d", target_userid);
            } else if (StrEqual(action_id, "kick")) {
                FakeClientCommand(client, "callvote kick %d", target_userid);
            } else if (StrEqual(action_id, "report")) {
                ShowReportMenu(client);
            }
        }

        case MenuAction_End: {
            delete menu;
        }
    }

    return 0;
}

void ShowReportMenu(int client)
{
    Menu menu = CreateMenu(Menu_Report);
    menu.AddItem("insults", "Insults");
    menu.AddItem("cheating", "Cheating");
    menu.AddItem("teamkill", "Teamkill");
    menu.AddItem("flood", "Flood");
    menu.AddItem("spray", "Unacceptable spray");
    menu.AddItem("trolling", "Trolling");
    menu.Display(client, 10);
}

int Menu_Report(Menu menu, MenuAction action, int param1, int param2)
{
    switch (action) {
        case MenuAction_Select: {
            int hero_clientid = param1;
            int menu_item = param2;

            char reason[MAX_REPORT_REASON_LENGTH];
            menu.GetItem(menu_item, reason, sizeof(reason));
            int scum_userid = GetClientOfUserId(selected_targets[hero_clientid]);

#if DEBUG
            LogMessage("report reason selected '%s' for client %d", reason, scum_userid);
#endif

            ReportPlayer(hero_clientid, scum_userid, reason);
        }

        case MenuAction_End: {
            delete menu;
        }
    }

    return 0;
}

void ReportPlayer(int hero_clientid, int scum_clientid, const char[] reason)
{
    char hero_steamid[32], hero_name[128], safe_hero_name[128];
    GetClientName(hero_clientid, hero_name, sizeof(hero_name));
    g_DB.Escape(hero_name, safe_hero_name, sizeof(safe_hero_name));
    GetClientAuthId(hero_clientid, AuthId_Engine, hero_steamid, sizeof(hero_steamid), true);

    char scum_steamid[32], scum_name[128], safe_scum_name[128];
    GetClientName(scum_clientid, scum_name, sizeof(scum_name));
    g_DB.Escape(scum_name, safe_scum_name, sizeof(safe_scum_name));

    char safe_reason[255];
    g_DB.Escape(reason, safe_reason, sizeof(safe_reason));

    if (!GetClientAuthId(scum_clientid, AuthId_Engine, scum_steamid, sizeof(scum_steamid), true)) {
        PrintToChat(hero_clientid, "\x01[\x04SM\x01] player not found");
        return;
    }

    char query[256];
    Format(query, sizeof(query),
        "INSERT `reports` VALUES ('%s', '%s', '%s', '%s', '%s', 0, NOW())",
        hero_clientid, safe_hero_name, scum_steamid, safe_scum_name, safe_reason);

    g_DB.Query(OnReportSent, query, hero_clientid);
}

void OnReportSent(Database db, DBResultSet results, const char[] error, any data)
{
    if (strlen(error)) {
        LogError("Error executing db query: %s", error);
        return;
    }

    int hero_clientid = view_as<int>(data);
    int scum_clientid = GetClientOfUserId(selected_targets[hero_clientid]);

    if (scum_clientid) {
        PrintToChat(hero_clientid, "\x01[\x04SM\x01] %L has been reported", scum_clientid);
    } else {
        PrintToChat(hero_clientid, "\x01[\x04SM\x01] report has been sent");
    }
}
