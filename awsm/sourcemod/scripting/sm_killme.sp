#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <sdktools>
#include <awsm/util>

public Plugin myinfo = {
    name = "[AWSM] Someone please kill me",
    author = "lxndr",
    version = "1.0",
    description = "For those who can't take it any longer"
}

public void OnPluginStart()
{
    LoadTranslations ("killme.phrases");

    RegConsoleCmd("say", Command_Say);
    RegConsoleCmd("say_team", Command_Say);
}

Action Command_Say(int client, int args)
{
    if (IsCompetitiveMode()) {
        return Plugin_Continue;
    }

    char text[MAX_CHAT_LENGTH];
    GetCmdArgString(text, sizeof(text));
    StripQuotes(text);
    
    if (StrEqual (text, "someone please kill me", false)) {
        if (IsPlayerAlive(client))
            CreateTimer(0.3, Timer_KillMe, client);
    }

    return Plugin_Continue;
}

Action Timer_KillMe(Handle timer, int client)
{
    PrintToChat(client, "%t", "as you wish");
    ForcePlayerSuicide(client);
    return Plugin_Stop;
}
