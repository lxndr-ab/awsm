#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <awsm/util>

#define DEBUG 0


public Plugin myinfo = {
    name = "[AWSM] Perks",
    author = "lxndr",
    description = "Player perks system",
    version = "1.3.4",
};


#define InvalidPerk     -1
#define FirstPerk       0
#define PerkArmor       0
#define PerkBackpack    1
#define PerkRaincoat    2
#define PerkPatch       3
#define PerkChalk       4
#define PerkWind        5
#define PerkGoggles     6
#define PerkSneakers    7
#define PerkMeal        8
#define PerkLaser       9
#define PerkSilencer    10
#define PerkClip        11
#define PerkExplosive   12
#define PerkScope       13
#define PerkKnife       14
#define PerkSalt        15
#define PerkOintment    16
#define PerkReloader    17
#define PerkIncendiary  18
#define PerkJumper      19
#define PerkCount       20

int ppBit[PerkCount] = {
    0x00000000,	// Aramor
    0x00000080,	// Backpack
    0x00000100,	// Raincoat
    0x00000200,	// Patch
    0x00000800,	// Chalk
    0x00001000,	// Wind
    0x00002000,	// Goggles
    0x00008000,	// Sneakers
    0x00000000,	// Hot Meal
    0x00020000,	// Laser Sight
    0x00040000,	// Silencer
    0x00100000,	// Magazine
    0x00200000,	// Explosive
    0x01000000,	// Scope
    0x04000000,	// Knife
    0x08000000,	// Salts
    0x00000000,	// Ointment
    0x20000000,	// Sleight of Hand
    0x00000000,	// Incendiary
    0x00000000	// Jumper
};

char ppTitle[PerkCount][32] = {
    "Kevlar Body Armor",
    "Ammo Backpack",
    "Raincoat",
    "Nicotine Patch",
    "Climbing Chalk",
    "Second Wind",
    "Goggles",
    "Sneakers",
    "Hot Meal",
    "Laser Sight",
    "Suppressor Silencer",
    "High Capacity Magazine",
    "Explosive Ammo",
    "Sniper Scope",
    "Knife",
    "Smelling Salts",
    "Ointment",
    "Sleight of Hand",
    "Incendiary Ammo",
    "Jumper",
};

enum Award {
    FirstAward = 0,
    AwardSI = 0,
    AwardTank,
    AwardWitch,
    AwardCoronation,
    AwardHealing,
    AwardRevival,
    AwardRescue,
    AwardCount,
};

char paTitle[AwardCount][32] = {
    "Killing a special infected",
    "Killing a tank",
    "Killing a witch",
    "Crowning a witch",
    "Healing a teammate",
    "Reviving a teammate",
    "Rescuing a teammate",
};

enum Penalty {
    FirstPenalty = 0,
    PenaltyMurder = 0,
    PenaltyIncapacitation,
    PenaltyFall,
    PenaltyDeath,
    PenaltyLoss,
    PenaltyCount,
};

char pfTitle[PenaltyCount][32] = {
    "Killing a teammate",
    "Incapacitating a teammate",
    "Fall",
    "Death",
    "Loss",
};


#define InvalidAmmo     -1
#define FirstAmmo       0
#define AmmoNormal      0
#define AmmoExplosive   1
#define AmmoIncendiary  2
#define AmmoCount       3

char atTitle[AmmoCount][16] = {
    "Normal",
    "Explosive",
    "Incendiary",
};

int atPerk[AmmoCount] = {
    InvalidPerk,
    PerkExplosive,
    PerkIncendiary,
};


ConVar vEnabled;
ConVar vShowExpiration;
ConVar vShotgunBullets;
ConVar vJumperGravity;
ConVar vSICount;
ConVar vHealingHealth;
ConVar ppVar[PerkCount];
ConVar paVar[AwardCount];
ConVar pfVar[PenaltyCount];

bool activated[MAXPLAYERS];
int perks[MAXPLAYERS][PerkCount];
bool lasers[MAXPLAYERS];
bool silencers[MAXPLAYERS];
int ammotype[MAXPLAYERS];
int sikilled[MAXPLAYERS];
bool transition = false;
bool mapend = false;


public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
    if (GetEngineVersion() == Engine_Left4Dead) {
        return APLRes_Success;
    }
    
    return APLRes_SilentFailure;
}

public void OnPluginStart()
{
    LoadTranslations("common.phrases");
    LoadTranslations("perks.phrases");

    vEnabled = CreateConVar("pp_enable", "0", "Enable player perks. Disabling will remove all the perks.");
    vShowExpiration = CreateConVar("pp_show_expiration", "1", "");
    vShotgunBullets = CreateConVar("pp_shotgun_bullets", "6", "Number of bullets in one shotgun shot");
    HookConVarChange(vEnabled, EnabledChanged);

    /* Player awards */
    paVar[AwardSI] = CreateConVar("pp_award_si", "1", "Number of perks to give to a survivor for killing a special infected");
    paVar[AwardTank] = CreateConVar("pp_award_tank", "2", "Number of perks to give to every memeber of the team for killing a tank");
    paVar[AwardWitch] = CreateConVar("pp_award_witch", "2", "Number of perks to give to a survivor for killing a witch");
    paVar[AwardCoronation] = CreateConVar("pp_award_coronation", "3", "Number of perks to give to a survivor for crowning a witch");
    paVar[AwardHealing] = CreateConVar("pp_award_healing", "2", "Number of perks to give to a survivor for healing a teammate");
    paVar[AwardRevival] = CreateConVar("pp_award_revival", "2", "Number of perks to give to a survivor for reviving a teammate");
    paVar[AwardRescue] = CreateConVar("pp_award_rescue", "2", "Number of perks to give to a survivor for rescuing a teammate");

    vSICount = CreateConVar("pp_award_si_count", "1", "Number of special infected a survivor has to kill to earn an award");
    vHealingHealth = CreateConVar("pp_award_healing_health", "50", "Amount of mate's health a survivor has to heal to earn an award");

    /* Player penalties */
    pfVar[PenaltyMurder] = CreateConVar("pp_penalty_murder", "5", "Number of perks to remove from a survivor for killing a team mate");
    pfVar[PenaltyIncapacitation] = CreateConVar("pp_penalty_incapacitation", "2", "Number of perks to remove from a survivor for incapacitating a teammate");
    pfVar[PenaltyFall] = CreateConVar("pp_penalty_fall", "2", "Number of perks to remove from a survivor for falling down");
    pfVar[PenaltyDeath] = CreateConVar("pp_penalty_death", "2", "Number of perks to remove from a survivor for dying");
    pfVar[PenaltyLoss] = CreateConVar("pp_penalty_loss", "3", "Number of perks to remove from the team for losing");

    /* Player perks */
    ppVar[PerkArmor] = CreateConVar("pp_perk_armor", "1", "Allow Kevlar Body Armor");
    ppVar[PerkBackpack] = CreateConVar("pp_perk_backpack", "1", "Allow Ammo Backpack");
    ppVar[PerkRaincoat] = CreateConVar("pp_perk_raincoat", "1", "Allow Rain Coat");
    ppVar[PerkPatch] = CreateConVar("pp_perk_patch", "1", "Allow Nicotine Patch");
    ppVar[PerkChalk] = CreateConVar("pp_perk_chalk", "1", "Allow Climbing Chalk");
    ppVar[PerkWind] = CreateConVar("pp_perk_wind", "1", "Allow Secon Wind");
    ppVar[PerkGoggles] = CreateConVar("pp_perk_goggles", "1", "Allow Goggles");
    ppVar[PerkSneakers] = CreateConVar("pp_perk_sneakers", "1", "Allow Marathon");
    ppVar[PerkMeal] = CreateConVar("pp_perk_meal", "1", "Allow Hot Meal");
    ppVar[PerkLaser] = CreateConVar("pp_perk_laser", "1", "Allow Laser Sight");
    ppVar[PerkSilencer] = CreateConVar("pp_perk_silencer", "1", "Allow Suppressor Silencer");
    ppVar[PerkClip] = CreateConVar("pp_perk_clip", "1", "Allow High Capacity Magazine");
    ppVar[PerkExplosive] = CreateConVar("pp_perk_explosive", "60", "Allow Explosive Ammunition");
    ppVar[PerkScope] = CreateConVar("pp_perk_scope", "1", "Allow Sniper Scope");
    ppVar[PerkKnife] = CreateConVar("pp_perk_knife", "1", "Allow Knife");
    ppVar[PerkSalt] = CreateConVar("pp_perk_salt", "1", "Allow Smelling Salts");
    ppVar[PerkOintment] = CreateConVar("pp_perk_oinment", "1", "Allow Oinment");
    ppVar[PerkReloader] = CreateConVar("pp_perk_reloader", "1", "Allow Sleight of Hand");
    ppVar[PerkIncendiary] = CreateConVar("pp_perk_incendiary", "60", "Allow Incendiary Ammo");
    ppVar[PerkJumper] = CreateConVar("pp_perk_jumper", "1", "Allow Jumper");

    vJumperGravity = CreateConVar("pp_perk_jumper_gravity", "0.75", "Jumper gravity");

    /* user commands */
    RegConsoleCmd("sm_laser", Command_Laser, "Enable/Disable your laser sight");
    RegConsoleCmd("sm_silencer", Command_Silencer, "Enable/Disable your silencer");
    RegConsoleCmd("sm_perks", Command_Perks, "Show perk panel");
    RegConsoleCmd("sm_ammo", Command_Ammo, "Select ammo type");

    RegConsoleCmd("sm_show_perks", Command_ShowPerks, "Show available perks");
    RegConsoleCmd("sm_show_awards", Command_ShowAwards, "Show available awards");
    RegConsoleCmd("sm_show_penalties", Command_ShowPenalties, "Show available penalties");

    /* admin commands */
    RegAdminCmd("sm_addperk", Command_AddPerk, ADMFLAG_KICK);
    RegAdminCmd("sm_rmperk", Command_RemovePerk, ADMFLAG_KICK);
    RegAdminCmd("sm_award", Command_AwardPenalize, ADMFLAG_KICK);
    RegAdminCmd("sm_penalize", Command_AwardPenalize, ADMFLAG_KICK);

    HookUserMessage(GetUserMessageId("SayText"), SayTextHook, true);
}


void Clean()
{
    for (int i = 1; i <= MaxClients; i++) {
        ammotype[i] = 0;
        sikilled[i] = 0;
        
        for (int perk = FirstPerk; perk < PerkCount; perk++) {
            RemovePerk(i, perk, false);
        }
    }
}

void CleanProps()
{
    for (int i = 1; i <= MaxClients; i++) {
        if (IsClientConnected(i) && activated[i]) {
            SetEntProp(i, Prop_Send, "m_upgradeBitVec", 0);
            SetEntProp(i, Prop_Send, "m_ArmorValue", 0);
            SetEntProp(i, Prop_Send, "m_iMaxHealth", 100);
            SetEntityGravity(i, 1.0);
        }
    }
}


void EnabledChanged(ConVar convar, const char[] oldValue, const char[] newValue)
{
    if (vEnabled.BoolValue) {
        for (int i = 1; i <= MaxClients; i++)
            activated[i] = IsClientInGame(i);

        HookEvent("player_death", Event_PlayerDeath);
        HookEvent("player_incapacitated", Event_Incapacitated, EventHookMode_Pre);
        HookEvent("witch_killed", Event_WitchKilled);
        HookEvent("heal_success", Event_Healed);
        HookEvent("survivor_rescued", Event_Rescued);
        HookEvent("revive_success", Event_Revived);
        HookEvent("player_connect_full", Event_PlayerConnectFull);
        HookEvent("player_spawn", Event_PlayerSpawn);
        HookEvent("round_end", Event_RoundEnd, EventHookMode_Pre);
        HookEvent("map_transition", Event_MapTransition, EventHookMode_Pre);
        HookEvent("player_bot_replace", Event_BotReplacedPlayer);
        HookEvent("infected_hurt", Event_InfectedHurt);
        HookEvent("player_hurt", Event_PlayerHurt);
        HookEvent("weapon_fire", Event_WeaponFire);
    } else {
        CleanProps();
        Clean();

        UnhookEvent("player_death", Event_PlayerDeath);
        UnhookEvent("player_incapacitated", Event_Incapacitated, EventHookMode_Pre);
        UnhookEvent("witch_killed", Event_WitchKilled);
        UnhookEvent("heal_success", Event_Healed);
        UnhookEvent("survivor_rescued", Event_Rescued);
        UnhookEvent("revive_success", Event_Revived);
        UnhookEvent("player_connect_full", Event_PlayerConnectFull);
        UnhookEvent("player_spawn", Event_PlayerSpawn);
        UnhookEvent("round_end", Event_RoundEnd, EventHookMode_Pre);
        UnhookEvent("map_transition", Event_MapTransition, EventHookMode_Pre);
        UnhookEvent("player_bot_replace", Event_BotReplacedPlayer);
        UnhookEvent("infected_hurt", Event_InfectedHurt);
        UnhookEvent("player_hurt", Event_PlayerHurt);
        UnhookEvent("weapon_fire", Event_WeaponFire);
    }
}


void IncendiaryEnabledChanged(ConVar convar, const char[] oldValue, const char[] newValue)
{
    if (ppVar[PerkIncendiary].BoolValue) {
    } else {
        for (int i = 1; i <= MaxClients; i++) {
            RemovePerk(i, PerkIncendiary);
        }
    }
}


Action Command_Laser(int client, int args)
{
    if (client == 0) {
        return Plugin_Handled;
    }

    if (!vEnabled.BoolValue) {
        PrintHintText(client, "%t", "perks are disabled");
        return Plugin_Handled;
    }

    if (perks[client][PerkLaser] > 0) {
        lasers[client] = !lasers[client];
        UpdateUpgradeBits(client);
    } else {
        PrintHintText(client, "%t", "no a laser sight");
    }

    return Plugin_Handled;
}

Action Command_Silencer(int client, int args)
{
    if (client == 0) {
        return Plugin_Handled;
    }

    if (!vEnabled.BoolValue) {
        PrintHintText(client, "%t", "perks are disabled");
        return Plugin_Handled;
    }

    if (perks[client][PerkSilencer] > 0) {
        silencers[client] = !silencers[client];
        UpdateUpgradeBits(client);
    } else {
        PrintHintText(client, "%t", "no a silencer");
    }

    return Plugin_Handled;
}


int PerkPanelHandler(Menu menu, MenuAction action, int client, int param2)
{
    switch (action) {
        case MenuAction_Select: {
            if (param2 == 1) {
                ShowPerks(client, 0);
            } else if (param2 == 2) {
                ShowPerks(client, 1);
            }
        }
    }
    
    return 0;
}

void ShowPerks(int client, int page)
{
    // make a list of perks
    int list[PerkCount], num = 0;

    for (int i = FirstPerk; i < PerkCount; i++) {
        if (perks[client][i] > 0) {
            list[num] = i;
            num++;
        }
    }

    Panel perk_panel = CreatePanel(GetMenuStyleHandle(MenuStyle_Radio));

    char label[64];
    int start = page * 10;
    int end = num - start > 10 ? start + 10 : num;

    for (int i = start; i < end; i++) {
        if (ppVar[list[i]].IntValue > 1) {
            Format(label, sizeof(label), "%T (%d)", ppTitle[list[i]], client, perks[client][list[i]]);
        } else {
            Format(label, sizeof(label), "%T", ppTitle[list[i]], client);
        }

        perk_panel.DrawText(label);
    }

    if (num == 0) {
        Format(label, sizeof(label), "%T", "no perks 1", client);
        perk_panel.DrawText(label);
        Format(label, sizeof(label), "%T", "no perks 2", client);
        perk_panel.DrawText(label);
        Format(label, sizeof(label), "%T", "no perks 3", client);
        perk_panel.DrawText(label);
        Format(label, sizeof(label), "%T", "no perks 4", client);
        perk_panel.DrawText(label);
    }

    perk_panel.DrawItem("Prev", start > 0 ? ITEMDRAW_CONTROL : ITEMDRAW_SPACER);
    perk_panel.DrawItem("Next", num > end ? ITEMDRAW_CONTROL : ITEMDRAW_SPACER);
    perk_panel.DrawItem("Close", ITEMDRAW_CONTROL);

    if (perk_panel.Send(client, PerkPanelHandler, 20)) {
        delete perk_panel;
    }
}

Action Command_Perks(int client, int args)
{
    if (client == 0) {
        return Plugin_Handled;
    }

    if (!vEnabled.BoolValue) {
        PrintHintText(client, "%t", "perks are disabled");
        return Plugin_Handled;
    }

    ShowPerks(client, 0);

    return Plugin_Handled;
}


int Menu_Ammo(Menu ammoMenu, MenuAction action, int client, int param)
{
    switch (action) {
        case MenuAction_Select: {
            char type[16];
            ammoMenu.GetItem(param, type, sizeof(type));
            ammotype[client] = StringToInt(type);
            UpdateUpgradeBits(client);
        }
        case MenuAction_End: {
            delete ammoMenu;
        }
    }
}

Action Command_Ammo(int client, int args)
{
    if (client == 0) {
        return Plugin_Handled;
    }

    if (!vEnabled.BoolValue) {
        PrintHintText(client, "%t", "perks are disabled");
        return Plugin_Handled;
    }

    char title[128], id[8];
    Menu ammoMenu = CreateMenu(Menu_Ammo);
    Format(title, sizeof(title), "%T", atTitle[AmmoNormal], client);
    ammoMenu.AddItem("0", title, ammotype[client] == 0 ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);

    for (int i = FirstAmmo + 1; i < AmmoCount; i++) {
        int perk = atPerk[i];

        if (perks[client][perk] > 0) {
            if (ppVar[perk].IntValue > 1) {
                Format(title, sizeof(title), "%T (%d)", atTitle[i], client, perks[client][perk]);
            } else {
                Format(title, sizeof(title), "%T", atTitle[i], client);
            }

            IntToString(i, id, sizeof(id));
            ammoMenu.AddItem(id, title, ammotype[client] == i ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
        }
    }

    ammoMenu.Display(client, 10);
    return Plugin_Handled;
}


Action Command_ShowPerks(int client, int args)
{
    PrintToConsole(client, "List of available perks:");

    for (int i = FirstPerk; i < PerkCount; i++) {
        PrintToConsole(client, "%2d. %s%s", i, ppTitle[i],
                ppVar[i].BoolValue ? "" : " (disabled)");
    }

    return Plugin_Handled;
}

Action Command_ShowAwards(int client, int args)
{
    PrintToConsole(client, "List of available perks:");

    for (Award i = FirstAward; i < AwardCount; i++) {
        PrintToConsole(client, "%2d. %s%s", i, paTitle[i],
            paVar[i].BoolValue ? "" : " (disabled)");
    }

    return Plugin_Handled;
}

Action Command_ShowPenalties(int client, int args)
{
    PrintToConsole(client, "List of available penalties:");

    for (Penalty i = FirstPenalty; i < PenaltyCount; i++) {
        PrintToConsole(client, "%2d. %s%s", i, pfTitle[i],
                pfVar[i].BoolValue ? "" : " (disabled)");
    }

    return Plugin_Handled;
}


Action Command_AddPerk(int client, int args)
{
    if (!vEnabled.BoolValue) {
        ReplyToCommand(client, "%t", "perks are disabled");
        return Plugin_Handled;
    }

    if (args < 2) {
        ReplyToCommand(client, "sm_addperk <target> <perk_id>");
        return Plugin_Handled;
    }

    char arg1 [MAX_NAME_LENGTH], arg2[8];
    GetCmdArg(1, arg1, sizeof(arg1));
    GetCmdArg(2, arg2, sizeof(arg2));

    int target = FindTarget(client, arg1);

    if (target == -1) {
        return Plugin_Handled;
    }

    int id = StringToInt(arg2);

    if (id < FirstPerk || id >= PerkCount) {
        ReplyToCommand(client, "Invalid perk ID");
        return Plugin_Handled;
    }

    AddPerk(target, id, true);
    return Plugin_Handled;
}

Action Command_RemovePerk(int client, int args)
{
    if (!vEnabled.BoolValue) {
        ReplyToCommand(client, "%t", "perks are disabled");
        return Plugin_Handled;
    }

    if (args < 2) {
        ReplyToCommand(client, "sm_rmperk <target> <perk_id>");
        return Plugin_Handled;
    }

    char arg1[MAX_NAME_LENGTH], arg2[8];
    GetCmdArg(1, arg1, sizeof(arg1));
    GetCmdArg(2, arg2, sizeof(arg2));

    int target = FindTarget(client, arg1);

    if (target == -1) {
        return Plugin_Handled;
    }

    int id = StringToInt(arg2);

    if (id < FirstPerk || id >= PerkCount) {
        ReplyToCommand(client, "Invalid perk ID");
        return Plugin_Handled;
    }

    RemovePerk(target, id, true);
    return Plugin_Handled;
}

Action Command_AwardPenalize(int client, int args)
{
    if (!vEnabled.BoolValue) {
        ReplyToCommand(client, "%t", "perks are disabled");
        return Plugin_Handled;
    }

    char cmd[16];
    GetCmdArg(0, cmd, sizeof(cmd));

    if (args < 2) {
        ReplyToCommand(client, "%s <target> <count>", cmd);
        return Plugin_Handled;
    }

    char arg1[MAX_NAME_LENGTH], arg2[8];
    GetCmdArg(1, arg1, sizeof(arg1));
    GetCmdArg(2, arg2, sizeof(arg2));

    char target_name[MAX_TARGET_LENGTH];
    int target_list[MAXPLAYERS], target_count;
    bool tn_is_ml;

    if ((target_count = ProcessTargetString(arg1, client, target_list,
            MAXPLAYERS, 0, target_name, sizeof(target_name), tn_is_ml)) <= 0) {
        ReplyToTargetError(client, target_count);
        return Plugin_Handled;
    }

    bool award = StrEqual(cmd, "sm_award");

    for (int i = 0; i < target_count; i++) {
        AddRemoveRandomPerks(target_list[i], award, StringToInt(arg2));
    }

    return Plugin_Handled;
}


Action SayTextHook(UserMsg msg_id, Handle bf, const int[] players, int playersNum, bool reliable, bool init)
{
    char msg[128];
    BfReadShort(bf);
    BfReadShort(bf);
    BfReadString(bf, msg, sizeof(msg), false);

    if (StrContains(msg, "prevent_it_expire") != -1) {
        CreateTimer(0.1, ItemExpired, PerkRaincoat);
        return Plugin_Handled;
    }
    if (StrContains(msg, "Smoker's Tongue attack") != -1) {
        CreateTimer(0.1, ItemExpired, PerkPatch);
        return Plugin_Handled;
    }
    if (StrContains(msg, "ledge_save_expire") != -1) {
        CreateTimer(0.1, ItemExpired, PerkChalk);
        return Plugin_Handled;
    }
    if (StrContains(msg, "revive_self_expire") != -1) {
        CreateTimer(0.1, ItemExpired, PerkWind);
        return Plugin_Handled;
    }
    if (StrContains(msg, "knife_expire") != -1) {
        CreateTimer(0.1, ItemExpired, PerkKnife);
        return Plugin_Handled;
    }
    
    if (StrContains(msg, "_expire") != -1) {
        return Plugin_Handled;
    }
    if (StrContains(msg, "#L4D_Upgrade") != -1 &&
            StrContains(msg, "description") != -1) {
        return Plugin_Handled;
    }
    if (StrContains(msg, "NOTIFY_VOMIT_ON") != -1) {
        return Plugin_Handled;
    }

    return Plugin_Continue;
}

Action ItemExpired(Handle timer, any id)
{
    for (int client = 1; client <= MaxClients; client++) {
        if (!(IsClientInGame(client) && GetClientTeam(client) == 2 && IsPlayerAlive(client))) {
            continue;
        }

        int bits = GetEntProp(client, Prop_Send, "m_upgradeBitVec");

        if (perks[client][id] > 0 && !(bits & ppBit[id])) {
            // the perk is set but the bit is not
            perks[client][id]--;

            if (vShowExpiration.BoolValue) {
                if (id == PerkRaincoat) {
                    PrintHintText(client, "%t", "raincoat used");
                } else if (id == PerkPatch) {
                    PrintHintText(client, "%t", "patch used");
                } else if (id == PerkChalk) {
                    PrintHintText(client, "%t", "chalk used");
                } else if (id == PerkWind) {
                    PrintHintText(client, "%t", "wind used");
                } else if (id == PerkKnife) {
                    PrintHintText(client, "%t", "knife used");
                }
            }
        }
    }

    return Plugin_Stop;
}


Action Event_PlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
    int victimid = event.GetInt("userid");

    if (victimid == 0) {    // no-one dies
        return;
    }

    int attacker = GetClientOfUserId(event.GetInt("attacker"));
    int victim = GetClientOfUserId(victimid);
    
    if (GetClientTeam(victim) == 2) {
        if (attacker == 0 || attacker == victim || GetClientTeam(attacker) != 2) {
            OnPenalty(victim, PenaltyDeath);
        } else {
            OnPenalty(attacker, PenaltyMurder);
        }

        return;
    }

    if (!(attacker && GetClientTeam(attacker) == 2)) {
        return;
    }

    char victimname[MAX_NAME_LENGTH];
    event.GetString("victimname", victimname, sizeof(victimname));

    if (StrEqual(victimname, "Hunter") || StrEqual(victimname, "Smoker") || StrEqual(victimname, "Boomer")) {
        sikilled[attacker]++;

        if (sikilled[attacker] >= vSICount.IntValue) {
            OnAward(attacker, AwardSI);
            sikilled[attacker] = 0;
        }
    } else if (StrEqual(victimname, "Tank")) {
        for (int i = 1; i <= MaxClients; i++) {
            if (IsClientInGame(i) && GetClientTeam(i) == 2) {
                OnAward(i, AwardTank);
            }
        }
    }
}

Action Event_WitchKilled(Event event, const char[] name, bool dontBroadcast)
{
    int attacker = GetClientOfUserId(event.GetInt("userid"));
    int oneshot = event.GetBool("oneshot");

    if (attacker && GetClientTeam(attacker) == 2) {
        OnAward(attacker, oneshot ? AwardCoronation : AwardWitch);
    }
}

Action Event_Incapacitated(Event event, const char[] name, bool dontBroadcast)
{
    int victim = GetClientOfUserId(event.GetInt("userid"));
    int attacker = GetClientOfUserId(event.GetInt("attacker"));

    if (victim > 0 && GetClientTeam(victim) == 2) {
        if (attacker && GetClientTeam(attacker) == 2) {
            OnPenalty(attacker, PenaltyIncapacitation);
        } else {
            OnPenalty(victim, PenaltyFall);
        }
    }
}

Action Event_Healed(Event event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(event.GetInt("userid"));
    int subject = GetClientOfUserId(event.GetInt("subject"));
    int health = GetClientHealth(subject);
    int restored = event.GetInt("health_restored");
    int old_health = health - restored;

    if (perks[client][PerkOintment] > 0) {
        if (perks[subject][PerkMeal] > 0) {
            SetEntityHealth(subject, 150);
            restored = 150 - old_health;
        } else {
            SetEntityHealth(subject, 100);
            restored = 100 - old_health;
        }
    } else {
        if (perks[subject][PerkMeal] > 0 && old_health > 75) {
            int new_health = GetRandomInt(125, 140);
            restored = new_health - old_health;
            SetEntityHealth(subject, new_health);
            restored = 150;	// dirty but works
        }
        // else it is the usual case. let the game do it
    }

    if (client != subject && restored >= vHealingHealth.IntValue) {
        OnAward(client, AwardHealing);
    }
}

Action Event_Revived(Event event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(event.GetInt("userid"));
    int subject = GetClientOfUserId(event.GetInt("subject"));

    if (client != subject) {
        OnAward(client, AwardRevival);
    }
}

Action Event_Rescued(Event event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(event.GetInt("rescuer"));
    OnAward(client, AwardRescue);
}

Action Event_RoundEnd(Event event, const char[] name, bool dontBroadcast)
{
    for (int i = 1; i <= MaxClients; i++) {
        if (IsClientConnected(i) && IsClientInGame(i)) {
            OnPenalty(i, PenaltyLoss);
        }
    }

    return Plugin_Continue;
}

Action Event_MapTransition(Event event, const char[] name, bool dontBroadcast)
{
#if DEBUG
    LogMessage("Map transition");
#endif

    // Due to server crashes, we have to reset the upgrade bits
    // and restore them on player spawn
    for (int i = 1; i <= MaxClients; i++) {
        if (IsClientConnected(i) && IsClientInGame(i)) {
            SetEntProp(i, Prop_Send, "m_upgradeBitVec", 0);
            // just in case if it can also crash the server
            SetEntData(i, FindDataMapInfo(i, "m_ArmorValue"), 0, 4, true);
            SetEntProp(i, Prop_Send, "m_iMaxHealth", 100);
            SetEntityGravity(i, 1.0);
        }
    }

    transition = true;
    mapend = true;
    return Plugin_Continue;
}

public void OnMapStart()
{
    // the player is loaded but it was not a map transition
    // so we assume it is a new campaign and have to clean everything
    if (!transition) {
#if DEBUG
        LogMessage("There was no transition");
#endif
        Clean();
    }

    transition = false;
    mapend = false;
}

public void OnClientConnected(int client)
{
    activated[client] = false;
}


void Event_PlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(event.GetInt("userid"));

    if (client > 0 && !mapend && activated[client] &&
            GetClientTeam(client) == 2 && IsPlayerAlive(client)) {
        // restoring of the upgrade bits
#if DEBUG
        LogMessage("%L spawned", client);
#endif
        CreateTimer(1.0, SetupSurvivor, client, TIMER_FLAG_NO_MAPCHANGE);
    }
}


Action Event_PlayerConnectFull(Event event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(event.GetInt("userid"));
    activated[client] = true;
    
    if (!mapend && !IsFakeClient(client) && GetClientTeam(client) == TEAM_SURVIVOR && IsPlayerAlive(client)) {
        // restoring of the upgrade bits
#if DEBUG
        LogMessage("%L fully connected", client);
#endif
        CreateTimer(2.0, SetupSurvivor, client, TIMER_FLAG_NO_MAPCHANGE);
    }
    
    return Plugin_Continue;
}


Action Event_BotReplacedPlayer(Event event, const char[] name, bool dontBroadcast)
{
    int bot = GetClientOfUserId(event.GetInt("bot"));
    int player = GetClientOfUserId(event.GetInt("player"));

    if (!(player > 0 && IsClientInGame(player) && GetClientTeam(player) == 2)) {
        return Plugin_Continue;
    }

#if DEBUG
    LogMessage("Bot %L => Player %L", bot, player);
#endif

    for (int i = FirstPerk; i < PerkCount; i++) {
        perks[bot][i] = perks[player][i];
    }

    perks[bot] = perks[player];
    lasers[bot] = lasers[player];
    silencers[bot] = silencers[player];
    ammotype[bot] = ammotype[player];
    activated[bot] = true;  // bots dont emit player_connect_full event

    CreateTimer(0.5, SetupSurvivor, bot, TIMER_FLAG_NO_MAPCHANGE);
    return Plugin_Continue;
}


Action Event_PlayerHurt(Event event, const char[] ename, bool dontBroadcast)
{
    int attacker = GetClientOfUserId(event.GetInt("attacker"));

    if (!(attacker && perks[attacker][PerkIncendiary] > 0 && ammotype[attacker] == 2 &&
            GetClientTeam(attacker) == TEAM_SURVIVOR)) {
        return Plugin_Continue;
    }

    int victim = GetClientOfUserId(event.GetInt("userid"));

    if (victim <= 0 || GetClientTeam(victim) == TEAM_SURVIVOR) {
        return Plugin_Continue;
    }

    int damagetype = event.GetInt("type");
    int inflictor = GetPlayerWeaponSlot(attacker, SLOT_PRIMARY);

    if (inflictor != -1 && (damagetype & DMG_BULLET)) {
        SDKHooks_TakeDamage(victim, inflictor, attacker, 0.0, DMG_BURN);
    }

    return Plugin_Continue;
}


Action Event_InfectedHurt(Event event, const char[] name, bool dontBroadcast)
{
    int attackerid = event.GetInt("attacker");

    if (attackerid == 0) {
        return Plugin_Continue;
    }

    int attacker = GetClientOfUserId(attackerid);
    int infected = event.GetInt("entityid");

    if (ammotype[attacker] == 2 && perks[attacker][PerkIncendiary] > 0) {
        char classname[64];
        GetEntityClassname(infected, classname, sizeof(classname));
    
        if (!StrEqual(classname, "witch")) {
            int damagetype = event.GetInt("type");
            int inflictor = GetPlayerWeaponSlot(attacker, SLOT_PRIMARY);

            if (inflictor != -1 && (damagetype & DMG_BULLET)) {
                SDKHooks_TakeDamage(infected, inflictor, attacker, 0.0, DMG_BURN);
            }
        }
    }

    return Plugin_Continue;
}


void Event_WeaponFire(Event event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(event.GetInt("userid"));

    if (ammotype[client] != AmmoNormal) {
        char weapon[64];
        GetEventString(event, "weapon", weapon, sizeof(weapon));

        int perk = atPerk[ammotype[client]];
        int count, left;

        if (StrEqual(weapon, "autoshotgun") || StrEqual(weapon, "pumpshotgun"))
            count = vShotgunBullets.IntValue;
        else
            count = 1;

        left = perks[client][perk] - count;
        if (left <= 0) {
            PrintHintText(client, "%t", "ammo depleted", ppTitle[perk]);
            RemovePerk(client, perk);
        } else {
            perks[client][perk] = left;
        }
    }
}


void OnPenalty(int client, Penalty id)
{
    // PrintToChat(client, "Penalty %s!", pfTitle[id]);
    AddRemoveRandomPerks(client, false, pfVar[id].IntValue);
}

void OnAward(int client, Award id)
{
    // PrintToChat(client, "Award %s!", paTitle[id]);
    AddRemoveRandomPerks(client, true, paVar[id].IntValue);
}


Action SetupSurvivor(Handle timer, any data)
{
    int client = view_as<int>(data);

    if (IsClientInGame(client) && activated[client] && !mapend &&
            GetClientTeam(client) == 2 && IsPlayerAlive(client)) {
        UpdateUpgradeBits(client);

        if (perks[client][PerkArmor] > 0) {
            SetEntData(client, FindDataMapInfo(client, "m_ArmorValue"), 100, 4, true);
        }

        if (perks[client][PerkMeal] > 0) {
            SetEntData(client, FindDataMapInfo(client, "m_iMaxHealth"), 150, 4, true);
        }

        if (perks[client][PerkJumper] > 0) {
            SetEntityGravity(client, vJumperGravity.FloatValue);
        }
    }

    return Plugin_Stop;
}

void UpdateUpgradeBits(int client)
{
    if (!(!mapend && IsClientInGame(client) && activated[client])) {
        return;
    }

    int bits = 0;

    for (int i = FirstPerk; i < PerkCount; i++) {
        if (perks[client][i] > 0) {
            if (i == PerkLaser && !lasers[client]) {
                continue;
            }

            if (i == PerkSilencer && !silencers[client]) {
                continue;
            }

            if (i == PerkExplosive && ammotype[client] != 1) {
                continue;
            }

            bits += ppBit[i];
        }
    }

#if DEBUG
    LogMessage("setting %L. Bits = %d", client, bits);
#endif

    SetEntProp(client, Prop_Send, "m_upgradeBitVec", bits);
}


int GetRandomPerk(int client, bool to_add)
{
    int list[PerkCount], i, num = 0;

    for (i = FirstPerk; i < PerkCount; i++) {
        if (ppVar[i].BoolValue && ((perks[client][i] > 0) ^ to_add)) {
            list[num] = i;
            num++;
        }
    }

    if (num == 0) {
        return InvalidPerk;
    }

    return list[GetRandomInt(0, num - 1)];
}

void AddRemoveRandomPerks(int client, bool add, int count)
{
    if (count == 0) {
        return;
    }

    char text[MAX_CHAT_LENGTH];
    Format(text, sizeof(text), "%T ", add ? "you got" : "you lost", client);

    for (int i = 0; i < count; i++) {
        int p = GetRandomPerk(client, add);

        if (p == InvalidPerk) { // no perks left
            if (i == 0) {
                return;         // dont show any messages
            }

            break;              // show what we've got
        }

        char title[64];
        Format(title, sizeof(title), "\x05%T\x01", ppTitle[p], client);
    
        if (i > 0) {
            StrCat(text, sizeof(text), ", ");
        }

        StrCat(text, sizeof(text), title);

        if (add) {
            AddPerk(client, p);
        } else {
            RemovePerk(client, p);
        }
    }

    PrintToChat(client, text);
}


void AddPerk(int client, int id, bool notify = false)
{
    if (perks[client][id] == 0) {
        if (notify)
            PrintToChat(client, "%t \x05%t", "you got", ppTitle[id]);
        
        if (id == PerkExplosive) {
            perks[client][PerkExplosive] = ppVar[PerkExplosive].IntValue;
        } else if (id == PerkIncendiary) {
            perks[client][PerkIncendiary] = ppVar[PerkIncendiary].IntValue;
        } else {
            perks[client][id] = 1;
        }

        if (IsClientInGame(client) && activated[client] && !mapend &&
                GetClientTeam(client) == TEAM_SURVIVOR && IsPlayerAlive(client)) {
            if (id == PerkArmor) {
                SetEntData(client, FindDataMapInfo(client, "m_ArmorValue"), 100, 4, true);
            }

            if (id == PerkMeal) {
                SetEntProp(client, Prop_Send,"m_iMaxHealth", 150);
                int health = GetClientHealth(client);
                SetEntityHealth(client, health + (health / 2));
            }

            if (id == PerkLaser && IsFakeClient(client)) {
                lasers[client] = true;
            }

            if (id == PerkJumper) {
                SetEntityGravity(client, vJumperGravity.FloatValue);
            }

            UpdateUpgradeBits(client);
        }
    }
}

/* This function removes a perk from any client.
	If the client is in game, it also properly sets props. */
void RemovePerk(int client, int id, bool notify = false)
{
    if (perks[client][id] > 0) {
        perks[client][id] = 0;

        /* protection is never enough */
        if (IsClientInGame(client) && activated[client] && !mapend &&
                GetClientTeam(client) == TEAM_SURVIVOR) {
            UpdateUpgradeBits(client);

            if (id == PerkArmor) {
                SetEntData(client, FindDataMapInfo(client, "m_ArmorValue"), 0, 4, true);
            }

            if (id == PerkMeal) {
                int health = GetClientHealth(client);
                if (health > 100)
                    SetEntityHealth(client, 100);
                SetEntProp(client, Prop_Send, "m_iMaxHealth", 100);
            }

            if (id == PerkJumper) {
                SetEntityGravity(client, 1.0);
            }

            if (notify) {
                PrintToChat(client, "%t \x05%t", "you lost", ppTitle[id]);
            }
        }

        if (id == PerkIncendiary && ammotype[client] == 2) {
            ammotype[client] = 0;
        }

        if (id == PerkExplosive && ammotype[client] == 1) {
            ammotype[client] = 0;
        }
    }
}
