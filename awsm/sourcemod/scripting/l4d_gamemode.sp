#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <awsm/gamemode>
#include <awsm/yesno-vote>

public Plugin myinfo = {
    name = "Game Mode Changer",
    author = "lxndr",
    version = "1.4",
    description = "Menu that allows players to change game mode",
}

bool l4d2 = false;
char selected_gamemode[MAX_GAMEMODE_LENGTH];
GameMode gamemode;

public void OnPluginStart()
{
    gamemode = new GameMode();

    LoadTranslations("common.phrases.txt");
    LoadTranslations("awsm.phrases.txt");
    LoadTranslations("gamemode_names.phrases.txt");
    LoadTranslations("gamemode.phrases.txt");

    if (GetEngineVersion() == Engine_Left4Dead2) {
        l4d2 = true;
    }

    RegConsoleCmd("sm_gamemode", Command_GameMode);
    RegConsoleCmd("sm_gm", Command_GameMode);
}

public void OnPluginEnd() {
    delete gamemode;
}

Action Command_GameMode(int client, int args)
{
    if (client == 0) {
        return Plugin_Handled;
    }

    Menu menu = CreateMenu(Menu_GameMode, MenuAction_Select | MenuAction_Cancel |
            MenuAction_End | MenuAction_DisplayItem | MenuAction_Display);

    ArrayList available_modes = gamemode.GetAvailable();
    int count = available_modes.Length;
    char gm[MAX_GAMEMODE_LENGTH];

    for (int i = 0; i < count; i++) {
        available_modes.GetString(i, gm, sizeof(gm));
        menu.AddItem(gm, gm, gamemode.IsCurrent(gm) ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
    }

    delete available_modes;

    menu.Display(client, 10);
    return Plugin_Handled;
}

int Menu_GameMode(Menu menu, MenuAction action, int client, int param) {
    switch (action) {
        case MenuAction_Display: {
            char title[128];
            Format(title, sizeof(title), "%T:", "Choose game mode", client);
            view_as<Panel>(param).SetTitle(title);
        }

        case MenuAction_DisplayItem: {
            char id[MAX_GAMEMODE_LENGTH], label[64];
            menu.GetItem(param, id, sizeof(id));
            Format(label, sizeof(label), "%T", id, client);
            return RedrawMenuItem(label);
        }

        case MenuAction_Select: {
            if (IsVoteInProgress ()) {
                CPrintToChat(client, "[{orange}SM{default}] %t", "Vote in Progress");
                return 0;
            }

            menu.GetItem(param, selected_gamemode, sizeof(selected_gamemode));
            ShowYesNoVote(client, OnVoteResult, OnVoteTitleRender);
        }

        case MenuAction_End: {
            delete menu;
        }
    }

    return 0;
}

void OnVoteTitleRender(int client, char[] label, int label_size) {
    Format(label, label_size, "%T", "change to", client, selected_gamemode);
}

YesNoVoteResult OnVoteResult(YesNoVoteResult result, char[] msg, int msg_size) {
    if (result == VoteResult_Yes) {
        ChangeGameMode(selected_gamemode);
    } else {
        PrintToChatAll("%t", "keepping current");
    }

    return result;
}

void ChangeGameMode(const char[] newgm) {
    char old_map_mode[MAX_GAMEMODE_LENGTH], new_map_mode[MAX_GAMEMODE_LENGTH];

    gamemode.GetMapMode(old_map_mode, sizeof(old_map_mode));
    gamemode.SetCurrent(newgm);
    gamemode.GetMapMode(new_map_mode, sizeof(new_map_mode));

    CPrintToChatAll("%t", "changed to", newgm);

    if (!StrEqual(old_map_mode, new_map_mode)) {
        CPrintToChatAll("%t", "need to change map");
    }

    /* fix max players */
    if (!l4d2) {
        ConVar l4d_maxplayers = FindConVar("l4d_maxplayers");

        if (l4d_maxplayers) {
            if (StrEqual(newgm, "versus")) {
                l4d_maxplayers.IntValue = 8; // FIXME needed?
            } else {
                l4d_maxplayers.IntValue = -1;
            }

            delete l4d_maxplayers;
        }
    }
}

/*
Action Timer_RestartCampaign(Handle timer, any data) {
	ServerCommand("changelevel %s", nextmap);
	return Plugin_Stop;
}

public GameModeChanged (Handle:cvar, const String:oldgm[], const String:newgm[])
{	
    if (	StrEqual (newgm, "versus") ||
            StrEqual (newgm, "survival") ||
            StrEqual (newgm, "scavenge")) {
        new Handle:vDifficulty = FindConVar ("z_difficulty");
        SetConVarString (vDifficulty, "Normal");
    }

    if (	StrEqual (newgm, "coop") ||
            StrEqual (newgm, "survival")) {
        for (new i = 1; i <= MaxClients; i++) {
            if (IsClientInGame (i) && GetClientTeam (i) == 3)
                ChangeClientTeam (i, 1);
        }
    }
}
*/
