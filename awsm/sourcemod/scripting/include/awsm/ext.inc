#pragma semicolon 1
#pragma newdecls required

#if defined _awesome_ext_included
	#endinput
#endif
#define _awesome_ext_included

#include <awsm/util>


typedef HookWitchRetreatFunc = function void (int witch);
native void HookWitchRetreat(HookWitchRetreatFunc callback);
native void UnhookWitchRetreat();


/*
Makes the extension required by the plugins, undefine REQUIRE_EXTENSIONS
if you want to use it optionally before including this .inc file
*/
public Extension __ext_awesome = {
	name = "Awesome",
	file = "awesome.l4d.ext",
#if defined AUTOLOAD_EXTENSIONS
	autoload = 1,
#else
	autoload = 0,
#endif
#if defined REQUIRE_EXTENSIONS
	required = 1,
#else
	required = 0,
#endif
};
