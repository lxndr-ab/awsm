#pragma semicolon 1
#pragma newdecls required

#if defined _yesno_vote_included
 #endinput
#endif
#define _yesno_vote_included

#include <nativevotes>
#include <multicolors>


enum YesNoVoteResult {
    VoteResult_Yes = 0,
    VoteResult_No = 1,
}


typedef YesNoVoteResultHandler = function YesNoVoteResult (YesNoVoteResult result, char[] msg, int msg_size);
typedef YesNoVoteTitleRenderer = function void (int client, char[] label, int label_size);


bool g_EnableNativeVote = true;
YesNoVoteResultHandler g_HandleResult;
YesNoVoteTitleRenderer g_RenderTitle;
Handle g_NativeVote = null;


YesNoVoteResult NormalizeResult(int reason) {
    return reason == 0 ? VoteResult_Yes : VoteResult_No;
}


int NativeVoteMenuHandler(Handle vote, MenuAction action, int param1, int param2)
{
    switch (action) {
        case MenuAction_End: {
            if (g_NativeVote) {
                // FIXME: crashes
                // NativeVotes_Close(g_NativeVote);
            }
        }
        case MenuAction_Display: {
            int client = param1;
            char label[128];

            if (g_RenderTitle) {
                Call_StartFunction(null, g_RenderTitle);
                Call_PushCell(client);
                Call_PushStringEx(label, sizeof(label), SM_PARAM_STRING_UTF8 | SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
                Call_PushCell(sizeof(label));
                Call_Finish();
            }

            NativeVotes_RedrawVoteTitle(label);
            return 1;
        }
        case MenuAction_VoteCancel: {
            if (param1 == VoteCancel_NoVotes) {
                NativeVotes_DisplayFail(vote, NativeVotesFail_NotEnoughVotes);
            } else {
                NativeVotes_DisplayFail(vote, NativeVotesFail_Generic);
            }
        }
        case MenuAction_VoteEnd: {
            if (g_HandleResult) {
                int reason = param1;
                YesNoVoteResult result = NormalizeResult(reason);
                char label[128];

                Call_StartFunction(null, g_HandleResult);
                Call_PushCell(result);
                Call_PushStringEx(label, sizeof(label), SM_PARAM_STRING_UTF8 | SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
                Call_PushCell(sizeof(label));
                Call_Finish(result);

                if (result == VoteResult_Yes) {
                    CRemoveTags(label, sizeof(label));
                    NativeVotes_DisplayPass(vote, label);
                } else {
                    NativeVotes_DisplayFail(vote, NativeVotesFail_Loses);
                }
            }
        }
    }

    return 0;
}


int VoteMenuHandler(Menu menu, MenuAction action, int param1, int param2)
{
    switch (action) {
        case MenuAction_Display: {
            int client = param1;
            Panel panel = view_as<Panel>(param2);
            char label[128];

            if (g_HandleResult) {
                Call_StartFunction(null, g_RenderTitle);
                Call_PushCell(client);
                Call_PushStringEx(label, sizeof(label), SM_PARAM_STRING_UTF8 | SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
                Call_PushCell(sizeof(label));
                Call_Finish();
            }

            CRemoveTags(label, sizeof(label));
            panel.SetTitle(label, false);
        }
        case MenuAction_DisplayItem: {
            int client = param1;
            int menu_item = param2;

            char title[16];
            Format(title, sizeof(title), "%T", menu_item == 0 ? "Yes" : "No", client);

            return RedrawMenuItem(title);
        }
        case MenuAction_VoteEnd: {
            if (g_HandleResult) {
                int reason = param1;
                YesNoVoteResult result = NormalizeResult(reason);
                char label[128];

                Call_StartFunction(null, g_HandleResult);
                Call_PushCell(result);
                Call_PushStringEx(label, sizeof(label), SM_PARAM_STRING_UTF8 | SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
                Call_PushCell(sizeof(label));
                Call_Finish();

                CPrintToChatAll("[\x04SM{default}] %s", label);
            }
        }
        case MenuAction_End: {
            delete menu;
        }
    }

    return 0;
}


public void ShowYesNoVote(int initiator, YesNoVoteResultHandler on_result, YesNoVoteTitleRenderer on_render_title)
{
    bool native_vote_supported = NativeVotes_IsVoteTypeSupported(NativeVotesType_Custom_YesNo);

    g_HandleResult = on_result;
    g_RenderTitle = on_render_title;

    if (native_vote_supported && g_EnableNativeVote) {
        if (g_NativeVote == null) {
            g_NativeVote = NativeVotes_Create(NativeVoteMenuHandler, NativeVotesType_Custom_YesNo, NATIVEVOTES_ACTIONS_DEFAULT | MenuAction_Display);
        }
        
        if (initiator) {
            NativeVotes_SetInitiator(g_NativeVote, initiator);
        }

        NativeVotes_SetTitle(g_NativeVote, "");
        NativeVotes_DisplayToAll(g_NativeVote, 15);
    } else {
        Menu menu = new Menu(VoteMenuHandler, MenuAction_Display | MenuAction_DisplayItem | MenuAction_VoteEnd);
        menu.ExitButton = false;
        menu.ExitBackButton = false;
        menu.SetTitle("");
        menu.AddItem("yes", "Yes");
        menu.AddItem("no", "No");
        menu.DisplayVoteToAll(15);
    }

    CPrintToChatAllEx(initiator, "%t", "started vote", initiator);
}
