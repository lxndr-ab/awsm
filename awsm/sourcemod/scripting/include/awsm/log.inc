#include <sourcemod>

new Handle:gLogGameEvent = INVALID_HANDLE;

#define SM_LOG_CONN		1
#define SM_LOG_DISC		2
#define SM_LOG_CHAT		3
#define SM_LOG_DEATH	4
#define SM_LOG_NAME		5
#define SM_LOG_VOTE		6
#define SM_LOG_MAP		7
#define SM_LOG_INCAP	8


public LogGameEvent (client, const String:name[], event, const String:value[])
{
  if (gLogGameEvent == INVALID_HANDLE) {
    gLogGameEvent = CreateGlobalForward ("LogGameEventReal", ET_Ignore,
        Param_Cell, Param_String, Param_Cell, Param_String);
    if (gLogGameEvent == INVALID_HANDLE) {
      LogError ("Couldn't find the global function `LogGameEventReal`");
    }
  }
  
  Call_StartForward (gLogGameEvent);
  Call_PushCell (client);
  Call_PushString (name);
  Call_PushCell (event);
  Call_PushString (value);
  Call_Finish ();
}
