#pragma semicolon 1
#pragma newdecls required

#if defined _awsm_util_included
	#endinput
#endif
#define _awsm_util_included


#define MAX_CHAT_LENGTH     191

#define TEAM_UNKNOWN        0
#define TEAM_SPECTATOR      1
#define TEAM_SURVIVOR       2
#define TEAM_INFECTED       3

#define DMG_BULLET			(1 << 1)
#define DMG_BURN			(1 << 3)

#define SLOT_PRIMARY		0
#define SLOT_SECONDARY		1
#define SLOT_THROWABLE		2


stock bool GetGameName(char[] game_name, int game_name_size)
{
    EngineVersion version = GetEngineVersion();

    switch (version) {
        case Engine_Left4Dead: {
            strcopy(game_name, game_name_size, "l4d");
            return true;
        }
        case Engine_Left4Dead2: {
            strcopy(game_name, game_name_size, "l4d2");
            return true;
        }
    }

    return false;
}

stock int ExtractHostPort(char[] host)
{
    char buffers[2][64];
    int count = ExplodeString(host, ":", buffers, 2, 64);

    if (count != 2) {
        return 0;
    }

    return StringToInt(buffers[1]);
}

stock bool IsCompetitiveMode()
{
    char gm[16];
    ConVar vGameMode= FindConVar("mp_gamemode");
    GetConVarString(vGameMode, gm, sizeof(gm));

    return StrEqual(gm, "versus") || StrEqual (gm, "scavenge");
}

stock int GetRealTeam(int client)
{
    if (!IsClientInGame(client))
        return TEAM_UNKNOWN;

    int team = GetClientTeam(client);

    if (team == TEAM_INFECTED) {
        return TEAM_INFECTED;   /* infected */
    }

    if (team == TEAM_SURVIVOR && !IsFakeClient (client)) {
        return TEAM_SURVIVOR;   /* playing survivor */
    }

    if (team == TEAM_SPECTATOR && IsIdlePlayer (client)) {
        return TEAM_SURVIVOR;   /* idle survivor */
    }

    return TEAM_UNKNOWN;
}

stock bool IsIdlePlayer(int client)
{
    for (int i = 1; i <= MaxClients; i++) {
        if (GetBotOwner(i) == client) {
            return true;
        }
    }

    return false;
}

stock int GetBotOwner(int bot)
{
    if (IsClientInGame(bot) && IsFakeClient(bot) && GetClientTeam(bot) == TEAM_SURVIVOR) {
        int userid = GetEntProp(bot, Prop_Send, "m_humanSpectatorUserID");
        int owner = GetClientOfUserId(userid);

        if (owner > 0 && IsClientInGame(owner) && !IsFakeClient(owner) &&
                GetClientTeam(owner) == TEAM_SPECTATOR) {
            return owner;
        }
    }

    return 0;
}

stock int FindClientByName(const char[] name)
{
    char n[MAX_NAME_LENGTH];

    for (int i = 1; i <= MaxClients; i++) {
        if (IsClientInGame(i)) {
            GetClientName(i, n, sizeof(n));

            if (StrEqual(n, name)) {
                return i;
            }
        }
    }

    return 0;
}

stock int FindClientBySteamID(const char[] steam_id)
{
    char auth[32];

    for (int i = 1; i <= MaxClients; i++) {
        if (GetClientAuthId(i, AuthId_Engine, auth, sizeof(auth), true)) {
            if (StrEqual(auth, steam_id)) {
                return i;
            }
        }
    }

    return -1;
}

stock int FindClientByIP(const char[] client_ip)
{
    char ip[32];

    for (int i = 1; i <= MaxClients; i++) {
        if (GetClientIP(i, ip, sizeof(ip), true)) {
            if (StrEqual(ip, client_ip)) {
                return i;
            }
        }
    }

    return -1;
}

enum BaseGamemode {
    BaseGamemode_Unknown = 0,
    BaseGamemode_Coop = 1,
    BaseGamemode_Versus = 2,
    BaseGamemode_Scavenge = 3,
    BaseGamemode_Survival = 4,
}

stock BaseGamemode GetBaseGamemode()
{
	char gamemode[20];
	FindConVar("mp_gamemode").GetString(gamemode, sizeof(gamemode));

	if(StrEqual(gamemode, "coop", false))
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "realism", false))
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode,"versus", false))
		return BaseGamemode_Versus;
	else if(StrEqual(gamemode, "teamversus", false))
		return BaseGamemode_Versus;
	else if(StrEqual(gamemode, "scavenge", false))
		return BaseGamemode_Scavenge;
	else if(StrEqual(gamemode, "teamscavenge", false))
		return BaseGamemode_Scavenge;
	else if(StrEqual(gamemode, "survival", false))
		return BaseGamemode_Survival;
	else if(StrEqual(gamemode, "mutation1", false))		//Last Man On Earth
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "mutation2", false))		//Headshot!
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "mutation3", false))		//Bleed Out
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "mutation4", false))		//Hard Eight
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "mutation5", false))		//Four Swordsmen
		return BaseGamemode_Coop;
	//else if(StrEqual(gamemode, "mutation6", false))	//Nothing here
	//	return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "mutation7", false))		//Chainsaw Massacre
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "mutation8", false))		//Ironman
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "mutation9", false))		//Last Gnome On Earth
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "mutation10", false))	//Room For One
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "mutation11", false))	//Healthpackalypse!
		return BaseGamemode_Versus;
	else if(StrEqual(gamemode, "mutation12", false))	//Realism Versus
		return BaseGamemode_Versus;
	else if(StrEqual(gamemode, "mutation13", false))	//Follow the Liter
		return BaseGamemode_Scavenge;
	else if(StrEqual(gamemode, "mutation14", false))	//Gib Fest
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "mutation15", false))	//Versus Survival
		return BaseGamemode_Survival;
	else if(StrEqual(gamemode, "mutation16", false))	//Hunting Party
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "mutation17", false))	//Lone Gunman
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "mutation18", false))	//Bleed Out Versus
		return BaseGamemode_Versus;
	else if(StrEqual(gamemode, "mutation19", false))	//Taaannnkk!
		return BaseGamemode_Versus;
	else if(StrEqual(gamemode, "mutation20", false))	//Healing Gnome
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "community1", false))	//Special Delivery
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "community2", false))	//Flu Season
		return BaseGamemode_Coop;
	else if(StrEqual(gamemode, "community3", false))	//Riding My Survivor
		return BaseGamemode_Versus;
	else if(StrEqual(gamemode, "community4", false))	//Nightmare
		return BaseGamemode_Survival;
	else if(StrEqual(gamemode, "community5", false))	//Death's Door
		return BaseGamemode_Coop;
	else
		return BaseGamemode_Unknown;
}

stock void GetBaseGamemodeName(char[] name, int name_size, BaseGamemode gamemode) {
    switch (gamemode) {
        case BaseGamemode_Coop:
            strcopy(name, name_size, "coop");
        case BaseGamemode_Versus:
            strcopy(name, name_size, "versus");
        case BaseGamemode_Survival:
            strcopy(name, name_size, "survival");
        case BaseGamemode_Scavenge:
            strcopy(name, name_size, "scavenge");
    }
}
