#pragma semicolon 1
#pragma newdecls required

#if defined _awsm_gamemode_included
	#endinput
#endif
#define _awsm_gamemode_included

#define MAX_GAMEMODE_LENGTH 16
#define GAMEMODE_COUNT 32

char available_gamemodes[GAMEMODE_COUNT][MAX_GAMEMODE_LENGTH] = {
    "coop",
    "realism",
    "versus",
    "teamversus",
    "scavenge",
    "teamscavenge",
    "survival",
    "mutation1",    // Last Man On Earth
    "mutation2",    // Headshot!
    "mutation3",    // Bleed Out
    "mutation4",    // Hard Eight
    "mutation5",    // Four Swordsmen
    "mutation6",    // Nothing here
    "mutation7",    // Chainsaw Massacre
    "mutation8",    // Ironman
    "mutation9",    // Last Gnome On Earth
    "mutation10",   // Room For One
    "mutation11",   // Healthpackalypse!
    "mutation12",   // Realism Versus
    "mutation13",   // Follow the Liter
    "mutation14",   // Gib Fest
    "mutation15",   // Versus Survival
    "mutation16",   // Hunting Party
    "mutation17",   // Lone Gunman
    "mutation18",   // Bleed Out Versus
    "mutation19",   // Taaannnkk!
    "mutation20",   // Healing Gnome
    "community1",   // Special Delivery
    "community2",   // Flu Season
    "community3",   // Riding My Survivor
    "community4",   // Nightmare
    "community5",   // Death's Door
};

char gamemode_map_modes[GAMEMODE_COUNT][MAX_GAMEMODE_LENGTH] = {
    "coop",
    "coop",
    "versus",
    "versus",
    "scavenge",
    "scavenge",
    "survival",
    "coop",         // Last Man On Earth
    "coop",         // Headshot!
    "coop",         // Bleed Out
    "coop",         // Hard Eight
    "coop",         // Four Swordsmen
    "coop",         // Nothing here
    "coop",         // Chainsaw Massacre
    "coop",         // Ironman
    "coop",         // Last Gnome On Earth
    "coop",         // Room For One
    "versus",       // Healthpackalypse!
    "versus",       // Realism Versus
    "scavenge",     // Follow the Liter
    "coop",         // Gib Fest
    "survival",     // Versus Survival
    "coop",         // Hunting Party
    "coop",         // Lone Gunman
    "versus",       // Bleed Out Versus
    "versus",       // Taaannnkk!
    "coop",         // Healing Gnome
    "coop",         // Special Delivery
    "coop",         // Flu Season
    "versus",       // Riding My Survivor
    "survival",     // Nightmare
    "coop",         // Death's Door
};

int FindModeIndex(const char[] gamemode) {
    for (int i = 0; i < GAMEMODE_COUNT; i++) {
        if (StrEqual(available_gamemodes[i], gamemode)) {
            return i;
        }
    }

    return -1;
}

methodmap GameMode < ConVar {
    public GameMode() {
        ConVar convar = FindConVar("mp_gamemode");
        return view_as<GameMode>(convar);
    }

    public bool IsCurrent(const char[] gamemode) {
        char cur_gamemode[MAX_GAMEMODE_LENGTH];
        this.GetString(cur_gamemode, sizeof(cur_gamemode));
        return StrEqual(cur_gamemode, gamemode);
    }

    public void SetCurrent(const char[] gamemode) {
        this.SetString(gamemode);
    }

    public bool GetMapMode(char[] mode, int mode_size) {
        char gamemode[MAX_GAMEMODE_LENGTH];
        this.GetString(gamemode, sizeof(gamemode));

        int index = FindModeIndex(gamemode);

        if (index == -1) {
            return false;
        }

        strcopy(mode, mode_size, gamemode_map_modes[index]);
        return true;
    }

    public ArrayList GetAvailable() {
        EngineVersion version = GetEngineVersion();
        ArrayList list = new ArrayList(MAX_GAMEMODE_LENGTH);

        switch (version) {
            case Engine_Left4Dead: {
                list.PushString("coop");
                list.PushString("versus");
                list.PushString("survival");
            }
            case Engine_Left4Dead2: {
                for (int i = 0; i < GAMEMODE_COUNT; i++) {
                    list.PushString(available_gamemodes[i]);
                }
            }
        }

        return list;
    }
}
