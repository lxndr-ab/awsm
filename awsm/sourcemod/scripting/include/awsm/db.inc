#pragma semicolon 1
#pragma newdecls required


public Database OpenDatabase(const char[] init_sql_name)
{
    char error[256];
    Database db = SQL_Connect("awsm", true, error, sizeof(error));

    if (db == null) {
        LogError(error);
        return null;
    }

    if (!db.SetCharset("utf8")) {
        LogError("Failed to change db connection character set");
    }

/*
    char init_sql_filename[PLATFORM_MAX_PATH];
    Format(init_sql_filename, sizeof(init_sql_filename), "configs/sql-init-scripts/mysql/%s.sql", init_sql_name);

    char init_sql_path[PLATFORM_MAX_PATH];
    BuildPath(Path_SM, init_sql_path, sizeof(init_sql_path), init_sql_filename);

    File init_sql_file = OpenFile(init_sql_path, "r", false);

    if(!init_sql_file) {
        LogError("Error opening init script: %s", init_sql_path);
        return db;
    }

    char init_sql[256];
    init_sql_file.ReadString(init_sql, sizeof(init_sql), -1);

    if (!SQL_FastQuery(db, init_sql)) {
        SQL_GetError(db, error, sizeof(error));
        LogError("Error initializing database tables: %s", error);
        return db;
    }
*/
    return db;
}
