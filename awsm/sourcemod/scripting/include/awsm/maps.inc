#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <awsm/util>

#define MAX_CAMPAIGN_ID_LENGTH 64
#define MAX_CAMPAIGN_TITLE_LENGTH 64
#define MAX_MAP_ID_LENGTH 64
#define MAX_MAP_TITLE_LENGTH 64

methodmap Maps < KeyValues {
    public Maps() {
        char game[16];
        GetGameName(game, sizeof(game));

        char cfg_name[32];
        Format(cfg_name, sizeof(cfg_name), "configs/maps.%s.cfg", game);

        char path[PLATFORM_MAX_PATH];
        BuildPath(Path_SM, path, sizeof(path), cfg_name);

        KeyValues kv = new KeyValues("Maps");
        kv.ImportFromFile(path);

        return view_as<Maps>(kv);
    }

    public bool DoesCampaignExist(const char[] campaign) {
        this.Rewind();
        return this.JumpToKey(campaign);
    }

    public bool IsOfficialCampaign(const char[] campaign) {
        char type[16];

        this.Rewind();

        if (!this.JumpToKey(campaign)) {
            return false;
        }

        if (!this.GetString("type", type, sizeof(type))) {
            return false;
        }

        return StrEqual(type, "official", false);
    }

    public bool FindCampaignByMap(char[] campaign, int campaign_size, char[] map) {
        char cur_campaign[MAX_CAMPAIGN_ID_LENGTH];

        this.Rewind();

        if (!this.GotoFirstSubKey()) {
            return false;
        }

        do {
            this.GetSectionName(cur_campaign, sizeof(cur_campaign));

            if (!this.GotoFirstSubKey()) {
                continue;
            }

            do {
                if (this.JumpToKey(map)) {
                    strcopy(campaign, campaign_size, cur_campaign);
                    return true;
                }
            } while (this.GotoNextKey());

            this.GoBack();
        } while (this.GotoNextKey());

        return false;
    }

    public bool IsFirstMap(const char[] map, BaseGamemode gamemode) {
        char cur_map[MAX_MAP_ID_LENGTH];

        if (gamemode != BaseGamemode_Coop || gamemode != BaseGamemode_Versus) {
            return false;
        }

        this.Rewind();

        if (!this.GotoFirstSubKey()) {
            return false;
        }

        do {
            if (!this.GotoFirstSubKey()) {
                continue;
            }

            do {
                if (this.GotoFirstSubKey()) {
                    this.GetSectionName(cur_map, sizeof(cur_map));

                    if (StrEqual(cur_map, map, false)) {
                        return true;
                    }

                    this.GoBack();
                }
            } while (this.GotoNextKey());

            this.GoBack();
        } while (this.GotoNextKey());

        return false;
    }

    public bool IsLastMap(const char[] campaign, const char[] map, BaseGamemode gamemode) {
        this.Rewind();

        if (!this.JumpToKey(campaign)) {
            return false;
        }

        char gamemode_name[16];
        GetBaseGamemodeName(gamemode_name, sizeof(gamemode_name), gamemode);

        PrintToChatAll("IsLastMap GM %d %s", view_as<int>(gamemode), gamemode_name);

        if (!this.JumpToKey(gamemode_name)) {
            return false;
        }

        if (!this.JumpToKey(map)) {
            return false;
        }

        return !this.GotoNextKey();
    }

    public bool GotoCampaign(const char[] campaign) {
        this.Rewind();
        return this.JumpToKey(campaign);
    }

    public bool GotoFirstCampaign(const char[] type, char[] first_campaign, int first_campaign_size) {
        char cur_type[16];

        this.Rewind();

        if (!this.GotoFirstSubKey()) {
            return false;
        }

        do {
            this.GetString("type", cur_type, sizeof(cur_type));

            if (StrEqual(cur_type, type)) {
                this.GetSectionName(first_campaign, first_campaign_size);
                return true;
            }
        } while (this.GotoNextKey());

        return false;
    }

    public bool GotoNextCampaign(const char[] type, char[] next_campaign, int next_campaign_size) {
        char cur_type[16];

        while (this.GotoNextKey()) {
            this.GetString("type", cur_type, sizeof(cur_type));

            if (StrEqual(cur_type, type)) {
                this.GetSectionName(next_campaign, next_campaign_size);
                return true;
            }
        }

        return false;
    }
}
