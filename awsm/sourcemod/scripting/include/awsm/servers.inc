#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>


public KeyValues OpenServersConfig()
{
    char path[PLATFORM_MAX_PATH];
    BuildPath(Path_SM, path, sizeof(path), "configs/servers.cfg");

    KeyValues kv = new KeyValues("Servers");
    kv.ImportFromFile(path);

    return kv;
}
