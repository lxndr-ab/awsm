#include <sourcemod>
#include <regex>
#include <geoip>


public Plugin:myinfo = {
    name = "IP Address Ban",
    description = "Allows to ban more than one ip address or ban the whole country",
    author = "lxndr",
    version = "1.1"
}


new Handle:rIP, Handle:rIPRange, Handle:rIPMask;
new Handle:list, Handle:listSet, Handle:listDesc;


public OnPluginStart ()
{
	rIP = CompileRegex ("^(?:\\d{1,3}\\.){3}\\d{1,3}$");
	rIPRange = CompileRegex ("^((?:\\d{1,3}\\.){3}\\d{1,3})-((?:\\d{1,3}\\.){3}\\d{1,3})$");
	rIPMask = CompileRegex ("^((?:\\d{1,3}\\.){3}\\d{1,3})/(\\d{1,2})$");
	
	list = CreateArray (2);
	listSet = CreateArray (ByteCountToCells(32));
	listDesc = CreateArray (ByteCountToCells(128));
	
	RegAdminCmd ("sm_ipban_add", Command_Add, ADMFLAG_BAN, "Adds an IP address to be banned");
	RegAdminCmd ("sm_ipban_del", Command_Delete, ADMFLAG_UNBAN, "Removes an IP address from the list");
	RegAdminCmd ("sm_ipban_clear", Command_Clear, ADMFLAG_UNBAN, "Removes all IP addresses from the list");
	RegAdminCmd ("sm_ipban_list", Command_List, ADMFLAG_GENERIC, "Shows all banned IP addresses");
	RegAdminCmd ("sm_ipban_test", Command_Test, ADMFLAG_GENERIC, "Shows if an IP address is banned");
	
	ServerCommand ("exec sourcemod/ipban.cfg");
}


public OnPluginEnd ()
{
	CloseHandle (rIP);
	CloseHandle (rIPRange);
	CloseHandle (rIPMask);
	CloseHandle (list);
	CloseHandle (listSet);
	CloseHandle (listDesc);
}


public bool:OnClientConnect (client, String:rejectmsg[], maxlen)
{
	if (client == 0 || IsFakeClient (client))
		return true;
	
	decl String:tmp[22];
	GetClientIP (client, tmp, sizeof (tmp));
	decl String:ip[2][16], String:desc[128];
	ExplodeString (tmp, ":", ip, 2, 16);
	
	if (CheckIP (ip[0], desc, sizeof (desc)))
		return true;
	
	Format (rejectmsg, maxlen, desc);
	return false;
}


IpToInt (const String:ip[])
{
	decl String:tmp[4][4];
	new n[4];
	
	ExplodeString (ip, ".", tmp, 4, 4);
	for (new i = 0; i < 4; i++)
		n[i] = StringToInt (tmp[i]);
	
	return n[0] * 0x1000000 + n[1] * 0x10000 + n[2] * 0x100 + n[3];
}


bool:CheckIP (const String:ipaddr[], String:desc[], maxlen)
{
	decl String:ccode[3];
	GeoipCode2 (ipaddr, ccode);
	new ip = IpToInt (ipaddr);
	
	new count = GetArraySize (list);
	for (new i = 0; i < count; i++) {
		new start = GetArrayCell (list, i, 0);
		new end = GetArrayCell (list, i, 1);
		if (start == 0 && end == 0) {
			decl String:set[32];
			GetArrayString (listSet, i, set, sizeof (set));
			if (StrEqual (ccode, set)) {
				GetArrayString (listDesc, i, desc, maxlen);
				return false;
			}
		} else if (start <= ip && ip <= end) {
			GetArrayString (listDesc, i, desc, maxlen);
			return false;
		}
	}
	return true;
}


bool:ParseSet (const String:set[], &start, &end)
{
	decl String:ip[16];
	
	if (MatchRegex (rIPRange, set) == 3) {
		GetRegexSubString (rIPRange, 1, ip, 16);
		start = IpToInt (ip);
		GetRegexSubString (rIPRange, 2, ip, 16);
		end = IpToInt (ip);
		return true;
	}
	
	if (MatchRegex (rIPMask, set) == 3) {
		decl String:tmp[3];
		GetRegexSubString (rIPMask, 2, tmp, 3);
		new mask = StringToInt (tmp);
		mask = 0xFFFFFFFF << (32 - mask);
		
		GetRegexSubString (rIPMask, 1, ip, 16);
		new n = IpToInt (ip);
		start = n & mask;
		end = n | ((~mask) & 0xFFFFFFFF);
		return true;
	}
	
	if (MatchRegex (rIP, set) == 1) {
		start = end = IpToInt (set);
		return true;
	}
	
	return false;
}


public Action:Command_Add (client, args)
{
	if (GetCmdArgs () < 2) {
		PrintToConsole (client, "Usage: sm_ipban_add <ip|start-end|ip/subnet|cc> <description>");
		return Plugin_Handled;
	}
	
	decl String:set[32], String:desc[128];
	GetCmdArg (1, set, sizeof (set));
	GetCmdArg (2, desc, sizeof (desc));
	
	if (strlen (set) == 2) {				// country code
		new idx = PushArrayCell (list, 0);
		SetArrayCell (list, idx, 0, 1);
		PushArrayString (listSet, set);
		PushArrayString (listDesc, desc);
	} else {
		new start, end;
		if (!ParseSet (set, start, end)) {
			LogError ("Invalide IP %s", set);
			return Plugin_Handled;
		}
		
		new idx = PushArrayCell (list, start);
		SetArrayCell (list, idx, end, 1);
		PushArrayString (listSet, set);
		PushArrayString (listDesc, desc);
	}
	
	return Plugin_Handled;
}


public Action:Command_Delete (client, args)
{
    if (GetCmdArgs () < 1) {
        PrintToConsole (client, "Usage: sm_ipban_del <number_of_record>");
        return Plugin_Continue;
    }
    
    decl String:tmp[4];
    GetCmdArg (1, tmp, sizeof (tmp));
    new idx = StringToInt (tmp) - 1;
    RemoveFromArray (list, idx);
    RemoveFromArray (listSet, idx);
    RemoveFromArray (listDesc, idx);
    return Plugin_Handled;
}


public Action:Command_Clear (client, args)
{
    ClearArray (list);
    ClearArray (listSet);
    ClearArray (listDesc);
    return Plugin_Handled;
}


public Action:Command_List (client, args)
{
    PrintToConsole (client, "Banned IP addresses");
    new count = GetArraySize (list);
    decl String:set[32], String:desc[128];
    for (new i = 0; i < count; i++) {
        GetArrayString (listSet, i, set, sizeof (set));
        GetArrayString (listDesc, i, desc, sizeof (desc));
        PrintToConsole (client, "\t[%02d] %s\tReason: %s", i + 1, set, desc);
    }
    return Plugin_Handled;
}


public Action:Command_Test (client, args)
{
    if (GetCmdArgs () < 1) {
        PrintToConsole (client, "Usage: sm_ipban_test <ip>");
        return Plugin_Continue;
    }
    
    decl String:ip[16], String:desc[128];
    GetCmdArg (1, ip, sizeof (ip));
    
    if (CheckIP (ip, desc, sizeof (desc)))
        PrintToConsole (client, "IP %s is not banned", ip);
    else
        PrintToConsole (client, "IP %s is banned. Reason: %s", ip, desc);
    
    return Plugin_Handled;
}
