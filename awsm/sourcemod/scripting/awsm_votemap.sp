#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <multicolors>
#include <awsm/maps>
#include <awsm/yesno-vote>


public Plugin myinfo = {
    name = "[AWSM] Map Vote",
    author = "lxndr",
    version = "1.3.1",
};


Maps cfg;
Handle hMapChangeTimer = null;

char selected_campaign_ids[MAXPLAYERS + 1][MAX_CAMPAIGN_ID_LENGTH];
char selected_campaign_titles[MAXPLAYERS + 1][MAX_CAMPAIGN_TITLE_LENGTH];
char selected_map_ids[MAXPLAYERS + 1][MAX_MAP_ID_LENGTH];
char selected_map_titles[MAXPLAYERS + 1][MAX_MAP_TITLE_LENGTH];

char selected_campaign_id[MAX_CAMPAIGN_ID_LENGTH];
char selected_campaign_title[MAX_CAMPAIGN_TITLE_LENGTH];
char selected_map_id[MAX_MAP_ID_LENGTH];
char selected_map_title[MAX_MAP_TITLE_LENGTH];

public void OnPluginStart() {
    LoadTranslations("common.phrases.txt");
    LoadTranslations("awsm.phrases.txt");
    LoadTranslations("votemap.phrases.txt");

    cfg = new Maps();

    RegConsoleCmd("callvote", Command_CallVote);
    RegConsoleCmd("sm_votemap", Command_VoteMap);
    RegConsoleCmd("sn_vm", Command_VoteMap);
}

public void OnPluginEnd() {
    delete cfg;
}

Action Command_CallVote(int client, int args) {
    if (client == 0) {
        return Plugin_Handled;
    }

    if (args != 2) {
        return Plugin_Continue;
    }

    char votetype[16], campaign[MAX_CAMPAIGN_ID_LENGTH];
    GetCmdArg(1, votetype, sizeof(votetype));
    GetCmdArg(2, campaign, sizeof(campaign));

    if (!StrEqual(votetype, "changemission", false)) {
        return Plugin_Continue;
    }

    if (IsVoteInProgress() || !NativeVotes_IsNewVoteAllowed()) {
        CPrintToChat(client, "[{green}SM{default}] %t", "Vote in Progress");
        return Plugin_Handled;
    }

    if (!cfg.DoesCampaignExist(campaign)) {
        CReplyToCommand(client, "%t", "not installed");
        return Plugin_Handled;
    }

    return Plugin_Continue;
}

Action Command_VoteMap(int client, int args) {
    if (client == 0) {
        return Plugin_Handled;
    }

    if (IsVoteInProgress() || !NativeVotes_IsNewVoteAllowed()) {
        CPrintToChat(client, "[{green}SM{default}] %t", "Vote in Progress");
        return Plugin_Handled;
    }

    ShowOfficialCampaignMenu(client);
    return Plugin_Handled;
}

void ShowOfficialCampaignMenu(int initiator) {
    char gamemode_name[64];
    BaseGamemode gamemode = GetBaseGamemode();
    GetBaseGamemodeName(gamemode_name, sizeof(gamemode_name), gamemode);

    Menu menu = new Menu(Menu_Campaign, MenuAction_DisplayItem | MenuAction_Select |  MenuAction_VoteEnd);
    menu.ExitBackButton = false;
    menu.SetTitle("%T", "choose campaign", initiator);

    char campaign[MAX_CAMPAIGN_ID_LENGTH], title[MAX_CAMPAIGN_TITLE_LENGTH], type[64];

    if (cfg.GotoFirstCampaign("official", campaign, sizeof(campaign))) {
        do {
            cfg.GetSectionName(campaign, sizeof(campaign));
            cfg.GetString("title", title, sizeof(title));
            cfg.GetString("type", type, sizeof(type));

            if (cfg.JumpToKey(gamemode_name)) {
                menu.AddItem(campaign, title);
                cfg.GoBack();
            }
        } while (cfg.GotoNextCampaign("official", campaign, sizeof(campaign)));
    }

    menu.AddItem("addons", "Addons");
    menu.Display(initiator, 15);
}

void ShowAddonCampaignMenu(int initiator) {
    char gamemode_name[64];
    BaseGamemode gamemode = GetBaseGamemode();
    GetBaseGamemodeName(gamemode_name, sizeof(gamemode_name), gamemode);

    Menu menu = new Menu(Menu_Campaign, MenuAction_DisplayItem | MenuAction_Cancel | MenuAction_Select |  MenuAction_VoteEnd);
    menu.ExitBackButton = false;
    menu.SetTitle("%T", "choose addon", initiator);

    char campaign[MAX_CAMPAIGN_ID_LENGTH], title[MAX_CAMPAIGN_TITLE_LENGTH], type[64];

    if (cfg.GotoFirstCampaign("addon", campaign, sizeof(campaign))) {
        do {
            cfg.GetSectionName(campaign, sizeof(campaign));
            cfg.GetString("title", title, sizeof(title));
            cfg.GetString("type", type, sizeof(type));

            if (cfg.JumpToKey(gamemode_name)) {
                menu.AddItem(campaign, title);
                cfg.GoBack();
            }
        } while (cfg.GotoNextCampaign("addon", campaign, sizeof(campaign)));
    }

    menu.Display(initiator, 15);
}

int Menu_Campaign(Menu menu, MenuAction action, int param1, int param2) {
    switch (action) {
        case MenuAction_DisplayItem: {
            int client = param1;
            int menu_item = param2;

            char id[32];
            GetMenuItem(menu, menu_item, id, sizeof(id));

            if (StrEqual(id, "addons")) {
                char title[16];
                Format(title, sizeof(title), "%T", "addons", client);
                return RedrawMenuItem(title);
            }
        }

        case MenuAction_Select: {
            int client = param1;
            int menu_item = param2;

            menu.GetItem(menu_item, selected_campaign_ids[client], MAX_CAMPAIGN_ID_LENGTH, _,
                selected_campaign_titles[client], MAX_CAMPAIGN_TITLE_LENGTH);

            if (StrEqual(selected_campaign_ids[client], "addons")) {
                ShowAddonCampaignMenu(client);
            } else {
                ShowChapterMenu(client, selected_campaign_ids[client]);
            }
        }

        case MenuAction_End: {
            delete menu;
        }
    }

    return 0;
}

void ShowChapterMenu(int initiator, const char[] campaign) {
    char gamemode_name[64];
    BaseGamemode gamemode = GetBaseGamemode();
    GetBaseGamemodeName(gamemode_name, sizeof(gamemode_name), gamemode);

    Menu menu = new Menu(Menu_Chapter, MenuAction_DisplayItem | MenuAction_Cancel | MenuAction_VoteEnd);
    menu.ExitBackButton = true;
    menu.SetTitle("%T", "choose map", initiator);

    cfg.Rewind();
    cfg.JumpToKey(campaign);
    cfg.JumpToKey(gamemode_name);
    cfg.GotoFirstSubKey();

    char map_id[MAX_MAP_ID_LENGTH], map_title[MAX_MAP_TITLE_LENGTH];

    do {
        cfg.GetSectionName(map_id, sizeof(map_id));
        cfg.GetString("title", map_title, sizeof(map_title));
        menu.AddItem(map_id, map_title);
    } while (cfg.GotoNextKey());

    menu.Display(initiator, 15);
}

int Menu_Chapter(Menu menu, MenuAction action, int param1, int param2) {
    switch (action) {
        case MenuAction_Select: {
            int client = param1;
            int menu_item = param2;

            menu.GetItem(menu_item, selected_map_ids[client], MAX_MAP_ID_LENGTH,
                _, selected_map_titles[client], MAX_MAP_TITLE_LENGTH);

            if (cfg.IsFirstMap(selected_map_ids[client], GetBaseGamemode())) {
                FakeClientCommand(client, "callvote changemission %s", selected_campaign_ids[client]);
            } else {
                strcopy(selected_campaign_id, MAX_CAMPAIGN_ID_LENGTH, selected_campaign_ids[client]);
                strcopy(selected_campaign_title, MAX_CAMPAIGN_TITLE_LENGTH, selected_campaign_titles[client]);
                strcopy(selected_map_id, MAX_MAP_ID_LENGTH, selected_map_ids[client]);
                strcopy(selected_map_title, MAX_MAP_TITLE_LENGTH, selected_map_titles[client]);

                ShowYesNoVote(client, OnVoteResult, OnVoteTitleRender);
            }
        }

        case MenuAction_Cancel: {
            int client = param1;
            int reason = param2;
            
            if (reason == MenuCancel_ExitBack) {
                ShowOfficialCampaignMenu(client);
            }
        }

        case MenuAction_End: {
            delete menu;
        }
    }

    return 0;
}

void OnVoteTitleRender(int client, char[] label, int label_size) {
    Format(label, label_size, "%T", "change map to", client, selected_map_titles[client]);
}

YesNoVoteResult OnVoteResult(YesNoVoteResult result, char[] msg, int msg_size) {
    if (result == VoteResult_Yes) {
        Format(msg, msg_size, "%t", "changing map to", selected_map_title);

        if (hMapChangeTimer && IsValidHandle(hMapChangeTimer)) {
            delete hMapChangeTimer;
        }

        hMapChangeTimer = CreateTimer(3.0, Timer_ChangeMap, _, TIMER_FLAG_NO_MAPCHANGE);
    } else {
        Format(msg, msg_size, "%t", "keeping map");
    }

    return result;
}

Action Timer_ChangeMap(Handle timer, any data) {
    ServerCommand("changelevel %s", selected_map_id);
    hMapChangeTimer = null;
    return Plugin_Stop;
}
