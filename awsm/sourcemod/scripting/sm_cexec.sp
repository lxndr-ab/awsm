#pragma semicolon 1
#pragma newdecls required
#include <sourcemod>


public Plugin myinfo = {
    name = "[AWSM] Client Execute",
    author = "lxndr",
    description = "Executes commands on clients",
    version = "1.2"
};


public void OnPluginStart()
{
    RegAdminCmd("sm_cexec", Command_Exec, ADMFLAG_GENERIC);
    RegAdminCmd("sm_fexec", Command_Exec, ADMFLAG_GENERIC);
    LoadTranslations("common.phrases");
}


public Action Command_Exec(int client, int args)
{
    char cmd[256];
    GetCmdArg(0, cmd, sizeof(cmd));
    bool is_fake = StrEqual(cmd, "sm_fexec");

    if (args < 2) {
        ReplyToCommand(client, "[SM] Usage: %s <#userid|name> <cmd>", cmd);
        return Plugin_Handled;
    }

    char arg[256];
    GetCmdArgString(arg, sizeof(arg));

    char target_arg[MAX_TARGET_LENGTH];
    int msg_index = BreakString(arg, target_arg, sizeof(target_arg));

    int target_list[MAXPLAYERS];
    char target_name[MAX_TARGET_LENGTH];
    bool tn_is_ml;
    int target_count = ProcessTargetString(target_arg, client, target_list, MAXPLAYERS,
        0, target_name, sizeof(target_name), tn_is_ml);

    if (target_count <= 0) {
        ReplyToTargetError(client, target_count);
        return Plugin_Handled;
    }

    for (int i = 0; i < target_count; i++) {
        if (is_fake)
            FakeClientCommand(target_list[i], arg[msg_index]);
        else
            ClientCommand(target_list[i], arg[msg_index]);
    }

    return Plugin_Handled;
}
