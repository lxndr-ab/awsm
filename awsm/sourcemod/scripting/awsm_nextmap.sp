#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <left4dhooks>
#include <awsm/maps>

#define DEBUG 0

public Plugin myinfo = {
    name = "[AWSM] Next map",
    author = "lxndr",
    version = "1.0",
};


Maps cfg;
StringMap change_map;
bool map_change_block = false; // prevents extra change


public void OnPluginStart()
{
    cfg = new Maps();
    change_map = new StringMap();

    HookEvent("round_end", Event_RoundEnd);
    HookEvent("finale_win", Event_FinaleWin);

    ReloadMapList();
}

public void OnPluginEnd() {
    delete cfg;
    delete change_map;
}

public void OnMapStart() {
    ReloadMapList();
    map_change_block = false;
}

Action Event_RoundEnd(Event event, const char[] name, bool dont_broadcast) {
    #if DEBUG
    LogMessage("round_end event");
    #endif

    BaseGamemode gamemode = GetBaseGamemode();

    if (gamemode != BaseGamemode_Versus || map_change_block) {
        return Plugin_Continue;
    }

    CheckAndStartNextMap();
    map_change_block = true;

    return Plugin_Continue;
}

Action Event_FinaleWin(Event event, const char[] name, bool dont_broadcast) {
    #if DEBUG
    LogMessage("finale_win event");
    #endif

    BaseGamemode gamemode = GetBaseGamemode();

    if (gamemode == BaseGamemode_Versus || map_change_block) {
        return Plugin_Continue;
    }

    CheckAndStartNextMap();
    map_change_block = true;

    return Plugin_Continue;
}

void ReloadMapList() {
    char gamemode_name[16],
         cur_campaign[MAX_CAMPAIGN_ID_LENGTH],
         first_map[MAX_MAP_ID_LENGTH],
         last_map[MAX_MAP_ID_LENGTH];

    ArrayList maps = new ArrayList(MAX_MAP_ID_LENGTH);

    BaseGamemode gamemode = GetBaseGamemode();
    GetBaseGamemodeName(gamemode_name, sizeof(gamemode_name), gamemode);

    if (!cfg.GotoFirstCampaign("official", cur_campaign, sizeof(cur_campaign))) {
        return;
    }

    do {
        if (GotoFirstMap(gamemode_name, first_map, sizeof(first_map)) &&
                GotoLastMap(gamemode_name, last_map, sizeof(last_map))) {

            #if DEBUG
            LogMessage("Found official map %s -> %s", first_map, last_map);
            #endif

            maps.PushString(first_map);
            maps.PushString(last_map);
        }
    } while (cfg.GotoNextCampaign("official", cur_campaign, sizeof(cur_campaign)));

    change_map.Clear();
    int campaign_count = maps.Length / 2;

    for (int i = 0; i < campaign_count; i++) {
        maps.GetString(i * 2 + 1, last_map, sizeof(last_map));

        if (i < campaign_count - 1) {
            maps.GetString(i * 2 + 2, first_map, sizeof(first_map));
        } else {
            maps.GetString(0, first_map, sizeof(first_map));
        }

        change_map.SetString(last_map, first_map);

        #if DEBUG
        LogMessage("Added map change %s -> %s", last_map, first_map);
        #endif
    }
}

void CheckAndStartNextMap() {
    if (!L4D_IsMissionFinalMap()) {
        return;
    }

    char cur_map[MAX_MAP_ID_LENGTH];
    GetCurrentMap(cur_map, sizeof(cur_map));

    #if DEBUG
    LogMessage("Current map %s", cur_map);
    #endif

    char next_map[MAX_MAP_ID_LENGTH];

    if (!change_map.GetString(cur_map, next_map, sizeof(next_map))) {
        StartRandomOfficialCampaign();
        return;
    }

    #if DEBUG
    LogMessage("Next map %s", next_map);
    #endif

    ServerCommand("changelevel %s", next_map);
}

void StartRandomOfficialCampaign() {
    StringMapSnapshot snap = change_map.Snapshot();
    int count = snap.Length;

    if (count == 0) {
        return;
    }

    int random = GetRandomInt(0, count - 1);

    char key[MAX_MAP_ID_LENGTH];
    snap.GetKey(random, key, sizeof(key));

    char map[MAX_MAP_ID_LENGTH];
    change_map.GetString(key, map, sizeof(map));

    ServerCommand("changelevel %s", map);
}

bool GotoFirstMap(const char[] gamemode, char[] map, int map_size) {
    if (cfg.JumpToKey(gamemode)) {
        if (cfg.GotoFirstSubKey()) {
            cfg.GetSectionName(map, map_size);
            cfg.GoBack();
            cfg.GoBack();
            return true;
        }

        cfg.GoBack();
    }

    return false;
}

bool GotoLastMap(const char[] gamemode, char[] map, int map_size) {
    char cur_map[MAX_MAP_ID_LENGTH];

    if (cfg.JumpToKey(gamemode)) {
        if (cfg.GotoFirstSubKey()) {
            do {
                cfg.GetSectionName(cur_map, sizeof(cur_map));
            } while (cfg.GotoNextKey());

            cfg.GoBack();
        }

        cfg.GoBack();
    }

    if (strlen(cur_map) == 0) {
        return false;
    }

    strcopy(map, map_size, cur_map);
    return true;
}
