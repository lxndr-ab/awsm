#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <awsm/util>

#define DEBUG 0


public Plugin myinfo = {
	name = "[AWSM] Votekick",
	author = "lxndr",
	description = "Menu for kicking. Extended ban time. Fix avoiding a ban by rejoining",
	version = "1.3.2",
	url = "left4dead.tk",
};


ConVar vBanTime;
char votee[32];


public void OnPluginStart()
{
    LoadTranslations("common.phrases");
    
    vBanTime = CreateConVar("sm_votekick_bantime", "30", "Ban time in minutes",
            _, true, 5.0);
    
    RegConsoleCmd("sm_votekick", Command_VoteKick);
    RegConsoleCmd("sm_vk", Command_VoteKick);
    
    HookEvent("vote_started", Event_VoteStarted);
    HookEvent("vote_passed", Event_VotePassed);
}

Action Command_VoteKick(int client, int args)
{
    char name[MAX_NAME_LENGTH];

    if (client == 0) {
        return Plugin_Handled;
    }

    if (args == 1) {
        GetCmdArg(1, name, sizeof(name));
        int target = FindTarget(client, name, false);

        if (target == -1) {
            return Plugin_Handled;
        }

        FakeClientCommand(client, "callvote kick %d", GetClientUserId(target));
    } else {
        char id[8];
        int voter_team = GetRealTeam(client);
        Menu selectMenu = CreateMenu(Menu_Select);

        for (int i = 1; i <= MaxClients; i++) {
            int team = GetRealTeam(i);

            if (team == voter_team || team == TEAM_SPECTATOR) {
                int userid = GetClientUserId(i);
                IntToString(userid, id, sizeof(id));
                GetClientName(i, name, sizeof(name));
                AddMenuItem(selectMenu, id, name);
            }
        }

        DisplayMenu(selectMenu, client, 10);
    }

    return Plugin_Handled;
}

int Menu_Select(Menu selectMenu, MenuAction action, int client, int param)
{
    switch (action) {
        case MenuAction_Select: {
            char id[16];
            selectMenu.GetItem(param, id, sizeof(id));
            FakeClientCommand(client, "callvote kick %s", id);
        }
        case MenuAction_End: {
            delete selectMenu;
        }
    }

    return 0;
}

void Event_VoteStarted(Event event, const char[] name, bool dontBroadcast)
{
    char issue[128], param1[128];
    event.GetString("issue", issue, sizeof(issue));
    event.GetString("param1", param1, sizeof(param1));
    
    if (StrEqual(issue, "#L4D_vote_kick_player")) {
        int target = FindClientByName(param1);

#if DEBUG
        LogMessage("Player %s (%L) is chosen to be kicked", param1, target);
#endif

        if (target > 0) {
            GetClientAuthId(target, AuthId_Engine, votee, sizeof(votee), true);
        } else {
            LogMessage("Player is not found");
        }
    }
}

void Event_VotePassed(Event event, const char[] name, bool dontBroadcast)
{
    char details[128], param1[128];
    event.GetString("details", details, sizeof(details));
    event.GetString("param1", param1, sizeof(param1));

    if (StrEqual(details, "#L4D_vote_passed_kick_player")) {
        int target = FindClientBySteamID(votee);
        int banTime = vBanTime.IntValue;

        if (target > 0) {
            BanClient(target, banTime, BANFLAG_AUTHID | BANFLAG_NOKICK, "Voted off", "", "sm_votekick");
        } else {
            BanIdentity(votee, banTime, BANFLAG_AUTHID, "Voted off", "sm_votekick");
        }

#if DEBUG
        LogMessage("%s [%d] was voted off and banned", votee, target);
#endif
    }
}
