#pragma semicolon 1
#pragma newdecls required
#include <sourcemod>

#define DEBUG				0
#define MAX_OPLEN			32	// Maximal option name length
#define MAX_SUBOP			7
#define OP_ENABLED			"+"
#define OP_DISABLED			"  "
#define USE_BUILTINVOTES	0

#if USE_BUILTINVOTES
#include <builtinvotes>
#endif

public Plugin myinfo = {
	name = "[AWSM] Customizing menu",
	author = "lxndr",
	description = "Let's one customize their gameplay",
	version = "2.2"
};

enum VotingMode {
	VotingMode_Anyone,
	VotingMode_Half,
	VotingMode_Majority,
	VotingMode_Everyone
};

ConVar vGameMode;
KeyValues kv;
ConVar cShowVoter;

public void OnPluginStart()
{
	LoadTranslations("common.phrases.txt");
	LoadTranslations("menu.phrases.txt");
	
	vGameMode = FindConVar("mp_gamemode");
	vGameMode.AddChangeHook(GameModeChanged);
	
	RegConsoleCmd("sm_menu", Command_Menu, "Show customizing menu");
	RegAdminCmd("sm_menuctl", Command_MenuCtl, ADMFLAG_GENERIC | ADMFLAG_VOTE,
			"Menu control");
	
	cShowVoter = CreateConVar("sm_menu_show_voter", "0", "Show players who starts a vote");
	
	LoadConfiguration();
}

public void OnPluginEnd()
{
	if (kv)
		CloseHandle(kv);
}

public void OnAllPluginsLoaded()
{
	Option_ResetAll();
}

bool LoadConfiguration()
{
	if (IsVoteInProgress())
		return false;
	
	if (kv != INVALID_HANDLE)
		CloseHandle(kv);
	
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof(path), "configs/menu.cfg");
	
	kv = CreateKeyValues("Menu");
	if (!FileToKeyValues(kv, path)) {
		LogError("Failed to load %s", path);
		return false;
	}
	
	return true;
}

void ListOptions(int client)
{
	char op[32], subid[32], name[64], value[64];
	ConVar cvar;
	
	kv.Rewind();
	if (kv.GotoFirstSubKey()) {
		do {
			kv.GetSectionName(op, sizeof(op));
			ReplyToCommand(client, op);
			if (kv.GotoFirstSubKey()) {
				do {
					kv.GetSectionName(subid, sizeof(subid));
					if (StrEqual(subid, "param")) {
						kv.GetString("name", name, sizeof(name));
						cvar = FindConVar(name);
						if (cvar == INVALID_HANDLE) {
							ReplyToCommand(client, "  param: %s (not found)", name);
						} else {
							cvar.GetString(value, sizeof(value));
							ReplyToCommand(client, "  param: %s = %s", name, value);
						}
					} else if (StrEqual(subid, "command")) {
						kv.GetString("name", name, sizeof(name));
						ReplyToCommand(client, "  command: %s", name);
					}
				} while (kv.GotoNextKey());
				kv.GoBack();
			}
		} while (kv.GotoNextKey());
	}
}

public void GameModeChanged(ConVar cvar, const char[] ov, char[] nv)
{
	Option_ResetAll();
}

public Action Command_Menu(int client, int args)
{
	if (client == 0 || IsVoteInProgress())
		return Plugin_Handled;
	ShowMenu(client);
	return Plugin_Handled;
}

public Action Command_MenuCtl(int client, int args)
{
	char[] usage = "Usage: sm_menuctl <reload|list|reset|enable|disable>";
	
	if (args < 1) {
		ReplyToCommand(client, usage);
		return Plugin_Handled;
	}
	
	char action[32];
	GetCmdArg(1, action, sizeof(action));
	
	if (StrEqual(action, "reload")) {
		if (LoadConfiguration())
			ReplyToCommand(client, "Menu configuration was successfully reloaded.");
		else
			ReplyToCommand(client, "Failed to relaod menu configuration.");
	} else if (StrEqual(action, "list")) {
		ListOptions(client);
	} else if (StrEqual(action, "reset")) {
		Option_ResetAll();
	} else if (StrEqual(action, "enable")) {
		if (args > 1) {
			char op[32];
			GetCmdArg(2, op, sizeof(op));
			Option_Turn(op, true);
		}
	} else if (StrEqual(action, "disable")) {
		if (args > 1) {
			char op[32];
			GetCmdArg(2, op, sizeof(op));
			Option_Turn(op, false);
		}
	} else {
		ReplyToCommand(client, usage);
		return Plugin_Handled;
	}
	
	return Plugin_Handled;
}

/*                                   Helpers                                  */

int GetAnyClient()
{
	for (int i = 1; i <= MaxClients; i++)
		if (IsClientInGame(i) && !IsFakeClient(i))
			return i;
	return 0;
}

void ExecCommand(const char[] name, bool server)
{
	int flags = GetCommandFlags(name);
	SetCommandFlags(name, flags & ~FCVAR_CHEAT);
	if (server) {
		ServerCommand(name);
	} else {
		int client = GetAnyClient();
		if (client)
			FakeClientCommand(client, name);
	}
	SetCommandFlags(name, flags);
}

bool IsParamEqual(const char[] name, const char[] value)
{
	ConVar h = FindConVar(name);
	if (h != INVALID_HANDLE) {
		char v[64];
		h.GetString(v, sizeof(v));
		return StrEqual(value, v);
	}
	return false;
}

void SetParam(const char[] name, const char[] value)
{
#if DEBUG
	LogMessage("Setting '%s' to '%s'", name, value);
#endif
	ConVar cvar = FindConVar(name);
	if (cvar == INVALID_HANDLE) {
		LogError("couldn't find cvar '%s'", name);
	} else {
		if (StrEqual(value, "!reset"))
			cvar.RestoreDefault();
		else
			cvar.SetString(value);
	}
}

/*                                Configuration                               */

bool Option_Locate(const char[] name, bool root = false)
{
	char op[32], subop[32];
	int pos;

	pos = SplitString(name, "/", op, sizeof(op));
	if (pos == -1)
		strcopy(op, sizeof(op), name);
	else
		strcopy(subop, sizeof(subop), name[pos]);

	kv.Rewind();
	if (!StrEqual(op, ""))
		if (!kv.JumpToKey(op))
			return false;
	
	if (root)
		return true;
	
	if (!StrEqual(subop, ""))
		if (!kv.JumpToKey(subop))
			return false;
	
	return true;
}

void Option_Perform(const char[] sub, bool enable)
{
	char act[32], varName[64], val[32];
	
	if (kv.GotoFirstSubKey()) {
		do {
			kv.GetSectionName(act, sizeof(act));
			if (StrEqual(act, "param")) {
				kv.GetString("name", varName, sizeof(varName));
				if (enable) {
					kv.GetString(sub, val, sizeof(val));
					if (StrEqual(val, ""))
						kv.GetString("enable", val, sizeof(val));
				} else {
					kv.GetString("disable", val, sizeof(val));
				}

				if (!StrEqual(val, ""))
					SetParam(varName, val);
			} else if (StrEqual(act, "command")) {
				if(enable) {
					kv.GetString(sub, val, sizeof(val));
					if (StrEqual(val, ""))
						kv.GetString("enable", val, sizeof(val));
				} else {
					kv.GetString("disable", val, sizeof(val));
				}
				
				if (!StrEqual(val, ""))
					ExecCommand(val, kv.GetNum("server", 1) == 1);
			}
		} while (kv.GotoNextKey());
		kv.GoBack();
	}
}

void Option_Turn(const char[] option, bool on, const char[] info = "")
{
	char base[MAX_OPLEN], sub[MAX_OPLEN];
	bool disable, issub;
	
	int pos = Option_Split(option, base, sizeof(base), disable, issub);

	if (pos != -1)
		strcopy(sub, sizeof(sub), option[pos]);
	else
		strcopy(sub, sizeof(sub), "");
	
	kv.Rewind();
	if (!kv.JumpToKey(base))
		return;
	Option_Perform(sub, on);
	
	if (on) {
		if (issub)
			PrintToChatAll("%t%s", "enabled-sub", base, option, info);
		else
			PrintToChatAll("%t%s", "enabled", base, info);
	} else {
		PrintToChatAll("%t%s", "disabled", base, info);
	}
}

void Option_ResetAll()
{
	char def[MAX_OPLEN];
	
	kv.Rewind();
	if (kv.GotoFirstSubKey()) {
		do {
			if (Option_IsGameModeAllowed()) {
				if (Option_HasSub()) {
					kv.GetString("default", def, sizeof(def));
					Option_Perform(def, StrEqual(def, "") ? false : true);
				} else {
					Option_Perform("", kv.GetNum("default") == 1);
				}
			} else {
				Option_Perform("", false);
			}
		} while (kv.GotoNextKey());
	}
	
	PrintToChatAll("%t", "all the options are reset");
}

bool Option_IsGameModeAllowed()
{
	char value[64];
	kv.GetString("gamemode", value, sizeof(value));

	char allowed_modes[8][32];
	int count = ExplodeString(value, ",", allowed_modes, 8, 32);
	if (StrEqual(allowed_modes[0], ""))	/* empty value means all game modes */
		return true;

	char current_mode[16];
	vGameMode.GetString(current_mode, sizeof(current_mode));
	
	for (int i = 0; i < count; i++)
		if (StrEqual(allowed_modes[i], current_mode))
			return true;
	return false;
}

bool Option_IsAllowed(int client)
{
	bool hidden = kv.GetNum("hidden") > 0;
	bool admin = GetAdminFlag(GetUserAdmin(client), Admin_Vote);
	return Option_IsGameModeAllowed() && (admin || !hidden);
}

VotingMode Option_GetVotingMode()
{
	char mode[32];
	kv.GetString("votingmode", mode, sizeof(mode));
	
	if (StrEqual(mode, "anyone"))
		return VotingMode_Anyone;
	else if (StrEqual(mode, "half"))
		return VotingMode_Half;
	else if (StrEqual(mode, "majority"))
		return VotingMode_Majority;
	else if (StrEqual(mode, "everyone"))
		return VotingMode_Everyone;
	else
		return VotingMode_Majority;
}

bool Option_HasSub()
{
	bool has = false;
	
	if (kv.JumpToKey("sub")) {
		has = true;
		kv.GoBack();
	}
	
	return has;
}

int Option_Split(const char[] op, char[] base, int maxbase, bool &disable, bool &sub)
{
	disable = false;
	sub = false;
	
	int ret = SplitString(op, "/", base, maxbase);

	if (ret > -1) {
		if (StrEqual(op[ret], "disable"))
			disable = true;
		else if (!StrEqual(op[ret], ""))
			sub = true;
	} else {
		strcopy(base, maxbase, op);
	}
	
	return ret;
}

/*                                  Option menu                               */

public int Submenu_Handler(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select) {
		int client = param1;
		int item = param2;
		
		char op[64];
		menu.GetItem(item, op, sizeof(op));
		
		bool admin = GetAdminFlag(GetUserAdmin(client), Admin_Vote);
		if (admin)
			StartAdminSubMenu(client, op);
		else
			StartVote(client, op);
	} else if (action == MenuAction_Cancel) {
		int client = param1;
		int reason = param2;
		if (reason == MenuCancel_ExitBack)
			ShowMenu(client);
	} else if (action == MenuAction_End) {
		CloseHandle(menu);
	}

	return 0;
}

void ShowSubmenu(int client, const char[] op)
{
	char id[128], title[128];
	char varName[64], val[64], key[4];
	char subop_list[MAX_SUBOP][MAX_OPLEN];
	int subop_num, enabled = -1;
	
	kv.Rewind();
	kv.JumpToKey(op);
	
	/* fill sub-option list */
	kv.JumpToKey("sub");
	for (subop_num = 0; subop_num < MAX_SUBOP; subop_num++) {
		IntToString(subop_num + 1, key, sizeof(key));
		kv.GetString(key, subop_list[subop_num], MAX_OPLEN);
		if (StrEqual(subop_list[subop_num], ""))
			break;
	}
	kv.GoBack();
	
	/* find what is enbaled */
	if (kv.JumpToKey("param")) {
		kv.GetString("name", varName, sizeof(varName));
		for (int i = 0; i < subop_num; i++) {
			kv.GetString(subop_list[i], val, sizeof(val));
			if (IsParamEqual(varName, val)) {
				enabled = i;
				break;
			}
		}
		kv.GoBack();
	}
	
	/* create sub-option menu */
	Menu menu = CreateMenu(Submenu_Handler);
	SetMenuExitBackButton(menu, true);
	
	for (int i = 0; i < subop_num; i++) {
		Format(id, sizeof(id), "%s/%s", op, subop_list[i]);
		Format(title, sizeof(title), "%T", id, client);
		AddMenuItem(menu, id, title, enabled == i ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	Format(id, sizeof(id), "%s/disable", op);
	AddMenuItem(menu, id, "Disable", enabled == -1 ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	
	DisplayMenu(menu, client, 20);
}

public int Menu_Handler(Menu menu, MenuAction action, int client, int item)
{
	if (action == MenuAction_Select) {
		char  op[64];
		menu.GetItem(item, op, sizeof(op));
		
		kv.Rewind();
		kv.JumpToKey(op);
		if (!StrEqual(op, "reset") && Option_HasSub()) {
			ShowSubmenu(client, op);
		} else {
			bool admin = GetAdminFlag(GetUserAdmin(client), Admin_Vote);
			if (admin)
				StartAdminSubMenu(client, op);
			else
				StartVote(client, op);
		}
	} else if (action == MenuAction_End) {
		CloseHandle(menu);
	}

	return 0;
}

void ShowMenu(int client)
{
	if (IsVoteInProgress()) {
		PrintToChat(client, "\x01[\x04SM\x01] %t", "Vote in Progress");
		return;
	}
	
	kv.Rewind();
	if (!kv.GotoFirstSubKey())
		return;
	
	char op[MAX_OPLEN], id[64];
	char title[128], subop_title[128];
	char varName[64], val[64], key[4];
	
	Menu menu = CreateMenu(Menu_Handler);
	
	do {
		kv.GetSectionName(op, sizeof(op));
		if (Option_IsAllowed(client)) {	
			bool enabled = false;
			char subop_list[MAX_SUBOP][MAX_OPLEN];
			int subop_num = 0, subop_enabled = -1;
			
			/* find out if there is sub-options and what option is enabled */
			if (kv.JumpToKey("sub")) {
				/* fill sub-option list */
				for (subop_num = 0; subop_num < MAX_SUBOP; subop_num++) {
					IntToString(subop_num + 1, key, sizeof(key));
					kv.GetString(key, subop_list[subop_num], MAX_OPLEN);
					if (StrEqual(subop_list[subop_num], ""))
						break;
				}
				kv.GoBack();
			}
			
			/* find what is enbaled */
			if (kv.JumpToKey("param")) {
				kv.GetString("name", varName, sizeof(varName));
				if (subop_num > 0) {
					/* check if sub-option is enabled */
					for (int i = 0; i < subop_num; i++) {
						kv.GetString(subop_list[i], val, sizeof(val));
						if (IsParamEqual(varName, val)) {
							subop_enabled = i;
							enabled = true;
							break;
						}
					}
				} else {
					/* check if option is enabled */
					kv.GetString("enable", val, sizeof(val));
					if (IsParamEqual(varName, val))
						enabled = true;
				}
				kv.GoBack();
			}
			
			/* make menu item title */
			if (enabled && subop_num > 0)
				Format(subop_title, sizeof(subop_title), " (%s)",
						subop_list[subop_enabled], client);
			else
				strcopy(subop_title, sizeof(subop_title), "");
			
			if (enabled && subop_num == 0)
				Format(id, sizeof(id), "%s/disable", op);
			else
				strcopy(id, sizeof(id), op);
			
			Format(title, sizeof(title), "[%s] %T%s%s",
					enabled ? OP_ENABLED : OP_DISABLED,
					op, client, subop_title, subop_num > 0 ? " >" : "");
			
			AddMenuItem(menu, id, title);
		}
	} while (kv.GotoNextKey());
	
	AddMenuItem(menu, "", "", ITEMDRAW_SPACER);
	
	Format(title, sizeof(title), "%T", "reset", client);
	AddMenuItem(menu, "reset", title);
	
	DisplayMenu(menu, client, 20);
}

/*                                Admin Submenu                               */

public int AdminSubMenu_Handler(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Display) {
		int client = param1;
		Panel handle = param2;
		char id[64];				// full option id
		char op[64];				// option id / base part
		char op_name[128];		// translated option name
		char subop_name[128];	// translated suboption name
		char title[255];			// final translated title
		bool disable, is_sub;
		
		menu.GetItem(0, id, sizeof(id));
		if (StrEqual(id, "reset")) {
			Format(title, sizeof(title), "%T", "ask to reset", client);
		} else {
			Option_Split(id, op, sizeof(op), disable, is_sub);
			if (is_sub)
				Format(subop_name, sizeof(subop_name), " (%T)", id, client);
			Format(op_name, sizeof(op_name), "%T%s", op, client, is_sub ? subop_name : "");
			Format(title, sizeof(title), "%T",
					disable ? "ask to disable" : "ask to enable", client, op_name);
		}
		handle.SetTitle(title);
	} else if (action == MenuAction_DisplayItem) {
		int client = param1, item = param2;
		char title[64];

		if (item == 0)
			Format(title, sizeof(title), "%T", "Yes", client);
		else if (item == 1)
			Format(title, sizeof(title), "%T", "No", client);
		else
			Format(title, sizeof(title), "%T", "start vote", client);
		return RedrawMenuItem(title);
	} else if (action == MenuAction_Select) {
		int client = param1, item = param2;
		char op[32];
		menu.GetItem(0, op, sizeof(op));
		
		if (item == 0) {
			char base[32];
			bool disable, issub;
			Option_Split(op, base, sizeof(base), disable, issub);
			if (StrEqual(op, "reset"))
				Option_ResetAll();
			else
				Option_Turn(disable ? base : op, !disable);
			ShowMenu(client);
		} else if (item == 1) {
			ShowMenu(client);
		} else if (item == 2) {
			StartVote(client, op);
		}
	} else if (action == MenuAction_End) {
		CloseHandle(menu);
	}
	
	return 0;
}

void StartAdminSubMenu(int client, const char[] op)
{
	Menu menu = CreateMenu(AdminSubMenu_Handler, MENU_ACTIONS_ALL);
	AddMenuItem(menu, op, "Yes");
	AddMenuItem(menu, "", "No");
	AddMenuItem(menu, "", "Start vote");
	DisplayMenu(menu, client, 10);
}

/*                                Vote menu                                   */

public int Vote_MenuHandler(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Display) {
		int client = param1;
		Panel handle = param2;
		char id[64];				// full option id
		char op[64];				// option id / base part
		char op_name[128];		// translated option name
		char subop_name[128];	// translated suboption name
		char title[255];			// final translated title
		bool disable, is_sub;
		
		menu.GetItem(0, id, sizeof(id));
		if (StrEqual(id, "reset")) {
			Format(title, sizeof(title), "%T", "reset", client);
		} else {
			Option_Split(id, op, sizeof(op), disable, is_sub);
			if (is_sub)
				Format(subop_name, sizeof(subop_name), " (%T)", id, client);
			Format(op_name, sizeof(op_name), "%T%s", op, client, is_sub ? subop_name : "");
			Format(title, sizeof(title), "%T",
					disable ? "ask to disable" : "ask to enable", client, op_name);
		}

		handle.SetTitle(title);
	} else if (action == MenuAction_DisplayItem) {
		int client = param1, item = param2;
		char title[64];
		if (item == 0)
			Format(title, sizeof(title), "%T", "Yes", client);
		else if (item == 1)
			Format(title, sizeof(title), "%T", "No", client);
		else
			Format(title, sizeof(title), "%T", "i dont care", client);
		return RedrawMenuItem(title);
	} else if (action == MenuAction_End) {
		CloseHandle(menu);
	}
	
	return 0;
}

public int Vote_Handler(Menu menu, int num_votes,
						int num_clients, const int[][] client_info,
						int num_items, const int[][] item_info)
{
	int yes = 0, no = 0;
	bool disable, issub;
	char op[32], base[32], result[64];
	menu.GetItem(0, op, sizeof(op));
	Option_Split(op, base, sizeof(base), disable, issub);
	
	for (int i = 0; i < num_items; i++) {
		if (item_info[i][VOTEINFO_ITEM_INDEX] == 0) {
			yes += item_info[i][VOTEINFO_ITEM_VOTES];
		} else if (item_info[i][VOTEINFO_ITEM_INDEX] == 1) {
			no += item_info[i][VOTEINFO_ITEM_VOTES];
		} else if (item_info[i][VOTEINFO_ITEM_INDEX] == 2) {
			int count = item_info[i][VOTEINFO_ITEM_VOTES];
			for (int n = 0; n < count; n++) {
				int r = GetRandomInt(0, 1);
				if (r == 0)
					yes++;
				else
					no++;
			}
		}
	}
	
	Option_Locate(op, true);
	VotingMode vm = Option_GetVotingMode();
	bool pass;
	if (disable)
		pass = (vm == VotingMode_Anyone && no > 0) ||
				(vm == VotingMode_Half && yes > no) ||
				(vm == VotingMode_Majority && yes >= no) ||
				(vm == VotingMode_Everyone && yes > 0);
	else
		pass = (vm == VotingMode_Anyone && yes > 0) ||
				(vm == VotingMode_Half && yes >= no) ||
				(vm == VotingMode_Majority && yes > no) ||
				(vm == VotingMode_Everyone && no == 0);
	
#if DEBUG
	LogMessage("Customizing menu vote, %s '%s': mode=%d, yes=%d, no=%d. %s",
			disable ? "disabling" : "enabling", op, vm, yes, no, pass ? "Passed" : "");
#endif	
	
	if (!pass)
		return 0;
	
	if (StrEqual(op, "reset"))
		Option_ResetAll();
	else {
		Format(result, sizeof(result), ". %d/%d", yes, num_votes);
		Option_Turn(disable ? base : op, !disable, result);
	}

	return 0;
}

void StartVote(int client, const char[] op)
{
	if (IsVoteInProgress()) {
		PrintToChat(client, "\x01[\x04SM\x01] %t", "Vote in Progress");
		return;
	}
	
#if USE_BUILTINVOTES
	new Handle:vote = CreateBuiltinVote(Vote_MenuHandler, BuiltinVoteType_Custom_YesNo,
			BuiltinVoteAction_Cancel | BuiltinVoteAction_End);
	SetBuiltinVoteArgument(vote, selected_title);
	SetBuiltinVoteResultCallback(vote, Vote_Handler);
	DisplayBuiltinVoteToAll(vote, 10);
#else
	Menu vote = CreateMenu(Vote_MenuHandler, MENU_ACTIONS_ALL);
	SetVoteResultCallback(vote, Vote_Handler);
	AddMenuItem(vote, op, "Yes");
	AddMenuItem(vote, "", "No");
	AddMenuItem(vote, "", "I don't care");
	VoteMenuToAll(vote, 10);
#endif
	
	if (GetConVarBool(cShowVoter))
		PrintToChatAll("%t", "started the vote", client);
}
