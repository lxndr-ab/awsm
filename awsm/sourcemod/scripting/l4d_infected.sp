#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <sdktools>
#include <left4dhooks>

#define DEBUG 1
#define TANK_ZOMBIE_CLASS 0

public Plugin myinfo = {
    name = "Infected control",
    author = "lxndr",
    description = "Adds multi-tanks, multi-witches, etc.",
    version = "1.1.1",
};

bool l4d2;
ConVar vExtraTank, vExtra;
int tank_chance_left = 0;
Handle extra_timer;

int l4d_infected_count = 3;
int l4d2_infected_count = 6;
char infected_list[][] = {"tank", "smoker", "boomer", "hunter", "spitter", "jockey", "charger"};

public void OnPluginStart()
{
    l4d2 = GetEngineVersion() == Engine_Left4Dead2;

    vExtraTank = CreateConVar("l4d_infected_extra_tank", "0",
            "Chance of extra tanks. Every 100 percents are an extra tank.",
            _, true, 0.0, true, 400.0);
    HookConVarChange(vExtraTank, ExtraTank_Changed);

    vExtra = CreateConVar("l4d_infected_extra", "0",
            "Spawn extra infected",
            _, true, 0.0, true, 1.0);

    HookConVarChange(vExtra, ExtraChanged);
}

public void OnMapStart()
{
    tank_chance_left = vExtraTank.IntValue;
}


/* Extra tanks  --------------------------------------------------------------*/

void ExtraTank_Changed(ConVar convar, const char[] oldValue, const char[] newValue)
{
    tank_chance_left = vExtraTank.IntValue;
}


public Action L4D_OnSpawnTank(const float vector[3], const float qangle[3])
{
#if DEBUG
    LogMessage("The director has spawned a tank. Extra tank chance %d", tank_chance_left);
#endif

    if (tank_chance_left > 0) {
        int chance = (tank_chance_left >= 100) ? 100 : tank_chance_left;

        if (GetRandomInt(1, 100) <= chance) {
            tank_chance_left -= chance;
            CreateTimer(15.0, CheckTankSpawn, _, TIMER_FLAG_NO_MAPCHANGE);
            CreateTimer(1.0, SpawnTank, _, TIMER_FLAG_NO_MAPCHANGE);
        }
    }

    return Plugin_Continue;
}

Action SpawnTank(Handle timer, any data)
{
    SpawnInfected(TANK_ZOMBIE_CLASS);
    return Plugin_Stop;
}

Action CheckTankSpawn(Handle timer, any data)
{
    tank_chance_left = vExtraTank.IntValue;
    return Plugin_Stop;
}

/* Extra infected  -----------------------------------------------------------*/

void ExtraChanged(ConVar convar, const char[] oldValue, const char[] newValue)
{
    if (convar.BoolValue) {
        extra_timer = CreateTimer(3.0, SpawnExtraInfected, _, TIMER_REPEAT);
    } else {
        KillTimer(extra_timer);
    }
}

Action SpawnExtraInfected(Handle timer, any data)
{
    SpawnRandomInfected();
    return Plugin_Continue;
}

void SpawnRandomInfected()
{
    int infected_count = l4d2 ? l4d2_infected_count : l4d_infected_count;
    int zombieClass = GetRandomInt(1, infected_count);
    SpawnInfected(zombieClass);
}

void SpawnInfected(int zombieClass)
{
    int client = GetAnyClient();

    float vecPos[3], vecAng[3];
    L4D_GetRandomPZSpawnPosition(client, zombieClass, 5, vecPos);

    if (zombieClass == TANK_ZOMBIE_CLASS) {
        L4D2_SpawnTank(vecPos, vecAng);
    } else {
        L4D2_SpawnSpecial(zombieClass, vecPos, vecAng);
    }

#if DEBUG
    LogMessage("'%s' has been spawned", infected_list[zombieClass]);
#endif
}

int GetAnyClient()
{
    for (int i = 1; i <= MaxClients; i++) {
        if (IsClientInGame(i) && !IsFakeClient(i)) {
            return i;
        }
    }

    return 0;
}
