#pragma semicolon 1
#pragma newdecls required

#define DEBUG 1

#include <sourcemod>

public Plugin myinfo = {
  name = "[AWSM] Empty server manager",
  author = "lxndr",
  version = "1.0",
};

public void OnPluginStart()
{
  HookEvent("player_disconnect", Event_PlayerDisconnect, EventHookMode_Pre);
}

public Action Event_PlayerDisconnect(Event event, const char[] name, bool dontBroadcast)
{
  int client = GetClientOfUserId(event.GetInt("userid"));

  if (client && !IsFakeClient(client) && !dontBroadcast) {
    #if DEBUG
    LogMessage("%d disconnected", client);
    #endif

    if (IsServerEmpty(client)) {
      #if DEBUG
      LogMessage("server is empty");
      #endif

      FindConVar("sv_hibernate_when_empty").SetInt(0);
      CreateTimer(0.5, Timer_RestartServer);
    }
  }

  return Plugin_Continue;
}

Action Timer_RestartServer(Handle timer)
{
  ShutdownServer();
  return Plugin_Stop;
}

bool IsServerEmpty(int ignoreClient)
{
  for (int client = 1; client <= MaxClients; client++) {
    if (IsClientConnected(client) && !IsFakeClient(client) && client != ignoreClient) {
      return false;
    }
  }

  return true;
}

void ShutdownServer()
{
  #if DEBUG
  LogMessage("shutting down");
  #endif

  ServerCommand("quit");
}
