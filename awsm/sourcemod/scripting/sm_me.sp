#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <awsm/util>

public Plugin myinfo = {
    name = "[AWSM] Me",
    author = "lxndr",
    version = "1.0.1",
    description = "Allows players to chat from 1st person"
}

public void OnPluginStart ()
{
    RegConsoleCmd("sm_me", Command_Me);
}

Action Command_Me(int client, int args)
{
    if (args == 0) {
        return Plugin_Continue;
    }

    char name[MAX_NAME_LENGTH], text[MAX_CHAT_LENGTH];

    if (client == 0) {
        name = "Console";
    } else {
        GetClientName(client, name, sizeof(name));
    }

    GetCmdArgString(text, sizeof(text));
    StripQuotes(text);

    PrintToChatAll("\x04%s\x01 %s", name, text);
    PrintToServer("%s %s", name, text);

    return Plugin_Handled;
}
