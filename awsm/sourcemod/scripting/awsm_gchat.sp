#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <awsm/db>
#include <awsm/servers>
#include <awsm/util>

Database g_DB = null;
Handle g_Timer = null;
char g_CurrentServer[4];
ArrayList g_AvailableServers = null;


public Plugin myinfo = {
    name = "[AWSM] Global Chat",
    author = "lxndr",
    description = "Allows players on different servers to chat to each other",
    version = "1.2.2",
}


public void OnPluginStart()
{
    LoadConfig();
    g_DB = OpenDatabase("awsm-gchat");
    g_Timer = CreateTimer(4.0, Timer_CheckMessages, _, TIMER_REPEAT);

    RegConsoleCmd("say", Command_Say);
    RegConsoleCmd("say_team", Command_Say);
}


public void OnPluginEnd()
{
    if (g_Timer) {
        CloseHandle(g_Timer);
    }

    if (g_DB) {
        delete g_DB;
    }

    if (g_AvailableServers) {
        delete g_AvailableServers;
    }
}


void LoadConfig()
{
    if (g_AvailableServers) {
        return;
    }

    KeyValues kv = OpenServersConfig();
    int hostport = FindConVar("hostport").IntValue;
    g_AvailableServers = new ArrayList(8);

    kv.GotoFirstSubKey();

    do {
        kv.GotoFirstSubKey();

        do {
            char shorthand[4], host[64];
            kv.GetString("shorthand", shorthand, sizeof(shorthand));
            kv.GetString("host", host, sizeof(host));
            int port = ExtractHostPort(host);

            g_AvailableServers.PushString(shorthand);

            if (port == hostport) {
                strcopy(g_CurrentServer, sizeof(g_CurrentServer), shorthand);
            }
        } while (kv.GotoNextKey());

        kv.GoBack();
    } while (kv.GotoNextKey());

    delete kv;
}

Action Command_Say(int client, int args)
{
    char text[256];
    GetCmdArgString(text, sizeof (text));
    StripQuotes(text);

    if (!(text[0] == '#' || text[0] == '!')) {
        return Plugin_Continue;
    }

    char to[4];
    int msgpos = BreakString(text[1], to, sizeof(to)) + 1;
    
    if (!(g_AvailableServers.FindString(to) > -1 || StrEqual(to, "0")) || strlen(text[msgpos]) == 0) {
        return Plugin_Continue;
    }

    if (StrEqual(to, g_CurrentServer)) {
        PrintToChat(client, "\x01[\x04SM\x01] You are trying to send a message to this server");
        return Plugin_Continue;
    }

    char from_name[64];
    GetClientName(client, from_name, sizeof(from_name));
    SendTo(from_name, to, text[msgpos]);

    return Plugin_Continue;
}


void SendTo(const char[] from_name, const char[] to, const char[] msg)
{
    Transaction tr = new Transaction();

    if (StrEqual(to, "0")) {
        int count = g_AvailableServers.Length;

        for (int i = 0; i < count; i++) {
            char dest[4];
            g_AvailableServers.GetString(i, dest, sizeof(dest));

            if (StrEqual(dest, g_CurrentServer)) {
                continue;
            }

            AddInsertQuery(tr, from_name, dest, msg);
        }
    } else {
        AddInsertQuery(tr, from_name, to, msg);
    }

    g_DB.Execute(tr, OnQuerySuccess, OnQueryFailure);
}

void AddInsertQuery(Transaction tr, const char[] from_name, const char[] to, const char[] msg)
{
    char safe_from_name[MAX_NAME_LENGTH * 2];
    g_DB.Escape(from_name, safe_from_name, sizeof(safe_from_name));

    char safe_msg[256];
    g_DB.Escape(msg, safe_msg, sizeof(safe_msg));

    char query[512];
    Format(query, sizeof(query),
        "INSERT INTO `global_chat` VALUES (DEFAULT, '%s', '%s', '%s', '%s')",
        g_CurrentServer, safe_from_name, to, safe_msg);

    tr.AddQuery(query);
}

Action Timer_CheckMessages(Handle t, any value)
{
    char query[512];
    Format(query, sizeof(query),
        "SELECT `from`,`from_name`,`msg` FROM `global_chat` WHERE `to`='%s'",
        g_CurrentServer);

    g_DB.Query(OnMessagesReceived, query);

    return Plugin_Continue;
}

void OnMessagesReceived(Database db, DBResultSet results, const char[] error, any data)
{
    char from[4], from_name[128], msg[256];

    if (strlen(error)) {
        LogError("Error executing db query: %s", error);
        return;
    }

    while (results.FetchRow()) {
        results.FetchString(0, from, sizeof(from));
        results.FetchString(1, from_name, sizeof(from_name));
        results.FetchString(2, msg, sizeof(msg));

        if (StrEqual(from, g_CurrentServer)) {
            continue;
        }

        PrintToChatAll("\x04%s\x03@\x05%s: \x01%s", from_name, from, msg);
    }

    DeleteReceivedMessages();
}

void DeleteReceivedMessages()
{
    char query[512];
    Format(query, sizeof(query),
        "DELETE FROM `global_chat` WHERE `to`='%s'",
        g_CurrentServer);

    g_DB.Query(OnMessagesDeleted, query);
}

void OnMessagesDeleted(Database db, DBResultSet results, const char[] error, any data)
{
    if (strlen(error)) {
        LogError("Error executing db query: %s", error);
        return;
    }
}

void OnQuerySuccess(Database db, any data, int numQueries, Handle[] results, any[] queryData)
{
}

void OnQueryFailure(Database db, any data, int numQueries, const char[] error, int failIndex, any[] queryData)
{
    LogError("Error executing db query: %s", error);
}
