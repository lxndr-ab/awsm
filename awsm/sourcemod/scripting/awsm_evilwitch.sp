#pragma semicolon 1
#pragma newdecls required

public Plugin myinfo = {
	name = "[AWSM] Evil Witch",
	author = "lxndr",
	description = "Witch that jst cannot stop",
	version = "1.0"
};

ConVar z_witch_kill_everyone;

public void OnPluginStart()
{
    // LoadTranslations ("awesome.phrases");

    z_witch_kill_everyone = CreateConVar("z_witch_kill_everyone", "0", "Witch can kill everyone", FCVAR_SPONLY);
    //HookConVarChange (hWitchKillEveryone, WitchKillEveryoneChanged);

    HookEvent("player_death", Event_PlayerDeath);
    HookEvent("player_incapacitated", Event_PlayerIncapacitated);

}

public void OnPluginEnd()
{
	//UnhookWitchRetreat();
    delete z_witch_kill_everyone;
}

Action Event_PlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
    int victim_id = event.GetInt("userid");

    char attacker_name[MAX_NAME_LENGTH];
    int attacker_id = event.GetInt("attacker");

    int attacker = GetClientOfUserId(attacker_id);
    GetClientName(attacker, attacker_name, sizeof(attacker_name));

    PrintToChatAll("Event_PlayerDeath: userid=%d; attacker=%d %s", victim_id, attacker_id, attacker_name);
    return Plugin_Continue;
}

Action Event_PlayerIncapacitated(Event event, const char[] name, bool dontBroadcast)
{
    int victim_id = event.GetInt("userid");

    char attacker_name[MAX_NAME_LENGTH];
    int attacker_id = event.GetInt("attacker");

    int attacker = GetClientOfUserId(attacker_id);
    GetClientName(attacker, attacker_name, sizeof(attacker_name));

    PrintToChatAll("Event_PlayerIncapacitated: userid=%d; attacker=%d %s", victim_id, attacker_id, attacker_name);
    return Plugin_Continue;
}

/*
public OnWitchRetreat(witch)
{
    new victim_list[MAXPLAYERS];
    new victim_count = 0;
    
    for (new i = 1; i <= MaxClients; i++) {
        if (IsClientInGame (i) && GetClientTeam (i) == 2 && IsPlayerAlive (i)) {
            victim_list[victim_count] = i;
            victim_count++;
        }
    }
    
    if (victim_count > 0)
        return victim_list[GetRandomInt (0, victim_count - 1)];
    
    return 0;
}*/
/*
public WitchKillEveryoneChanged (Handle:convar, const String:oldValue[], const String:newValue[])
{
    if (GetConVarBool (hWitchKillEveryone))
        HookWitchRetreat (OnWitchRetreat);
    else
        UnhookWitchRetreat ();
}
*/