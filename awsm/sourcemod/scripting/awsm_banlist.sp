#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <awsm/util>

#define DEBUG 0

public Plugin myinfo = {
    name = "[AWSM] Ban list",
    author = "lxndr",
    version = "1.2",
};

Database g_DB = null;


public void OnPluginStart()
{
    Database.Connect(OnDatabaseConnect, "awsm");
}

public void OnPluginEnd()
{
    if (g_DB) {
        delete g_DB;
    }
}

void OnDatabaseConnect(Database db, const char[] error, any data)
{
    if (!db) {
        LogError("Error connecting to db: %s", error);
        return;
    }

    if (!db.SetCharset("utf8")) {
        LogError("Failed to change db connection character set");
    }

    g_DB = db;
    ReloadBans();
}

void ReloadBans()
{
    char query[512];

    Format(
        query, sizeof(query),
        "SELECT `identity`, UNIX_TIMESTAMP(`end_time`) AS `end_time_unix`, `reason` FROM `bans` WHERE `active` = TRUE AND (`end_time` IS NULL OR `end_time` > FROM_UNIXTIME(%d))",
        GetTime()
    );

    g_DB.Query(OnBansFetched, query, _, DBPrio_High);
}

void OnBansFetched(Database db, DBResultSet results, const char[] error, any data)
{
    if (!results) {
        LogError("Error executing db query: %s", error);
        return;
    }

    char identity[32], reason[255];
    int now = GetTime();

    while (results.FetchRow()) {
        results.FetchString(0, identity, sizeof(identity));
        int end_time = results.IsFieldNull(1) ? 0 : results.FetchInt(1);
        results.FetchString(2, reason, sizeof(reason));

        int ban_time = (end_time == 0 ? 0 : end_time - now) / 60;
        int flags = StrContains(identity, "STEAM_") == 0 ? BANFLAG_AUTHID : BANFLAG_IP;

        #if DEBUG
        LogMessage("restore ban %s, time %d", identity, ban_time);
        #endif

        BanIdentity(identity, ban_time, flags, reason);

        int client = flags == BANFLAG_AUTHID
            ? FindClientBySteamID(identity)
            : FindClientByIP(identity);

        if (client > 0) {
            KickClient(client, reason);
        }
    }
}

public Action OnBanClient(int client, int time, int flags, const char[] reason, const char[] kick_message, const char[] command, any source)
{
    char identity[64];
    GetClientAuthId(client, AuthId_Engine, identity, sizeof(identity), true);

    OnBanIdentity(identity, time, flags, reason, command, source);
    return Plugin_Continue;
}

public Action OnBanIdentity(const char[] identity, int time, int flags, const char[] reason, const char[] command, any source)
{
    char player_name[MAX_NAME_LENGTH];
    int client = FindClientBySteamID(identity);

    if (client > 0) {
        GetClientName(client, player_name, sizeof(player_name));
    } else {
        strcopy(player_name, sizeof(player_name), "");
    }

    int admin_client = view_as<int>(source);
    char admin_steamid[64], admin_name[128];
    strcopy(admin_steamid, sizeof(admin_steamid), "");
    strcopy(admin_name, sizeof(admin_name), "");

    if (admin_client > 0) {
        if (GetClientAuthId(admin_client, AuthId_Engine, admin_steamid, sizeof(admin_steamid), true)) {
            AdminId admin = FindAdminByIdentity("steam", admin_steamid);

            if (admin != INVALID_ADMIN_ID) {
                admin.GetUsername(admin_name, sizeof(admin_name));
            }
        } else {
            strcopy(admin_steamid, sizeof(admin_steamid), "");
        }
    }

    StoreBan(identity, player_name, time, admin_steamid, admin_name, reason);
    return Plugin_Continue;
}

void StoreBan(const char[] identity, const char[] player_name, int time, const char[] admin_steamid, const char[] admin_name, const char[] reason)
{
    char safe_identity[64];
    g_DB.Escape(identity, safe_identity, sizeof(safe_identity));

    char safe_player_name[64];
    g_DB.Escape(player_name, safe_player_name, sizeof(safe_player_name));

    int start_time = GetTime();
    char end_time[64];

    if (time == 0) {
        strcopy(end_time, sizeof(end_time), "NULL");
    } else {
        Format(end_time, sizeof(end_time), "FROM_UNIXTIME(%d)", start_time + (time * 60));
    }

    char admin_steamid_query[64], admin_name_query[128];

    if (StrEqual(admin_steamid, "")) {
        strcopy(admin_steamid_query, sizeof(admin_steamid_query), "NULL");
        strcopy(admin_name_query, sizeof(admin_name_query), "NULL");
    } else {
        char safe_admin_steamid[64], safe_admin_name[128];
        g_DB.Escape(admin_steamid, safe_admin_steamid, sizeof(safe_admin_steamid));
        Format(admin_steamid_query, sizeof(admin_steamid_query), "'%s'", safe_admin_steamid);
        g_DB.Escape(admin_name, safe_admin_name, sizeof(safe_admin_name));
        Format(admin_name_query, sizeof(admin_name_query), "'%s'", safe_admin_name);
    }

    char safe_reason[255];
    g_DB.Escape(reason, safe_reason, sizeof(safe_reason));

    char query[512];
    Format(query, sizeof(query),
        "INSERT INTO `bans` (`admin_steamid`, `admin_name`, `identity`, `player_name`, `start_time`, `end_time`, `active`, `reason`) VALUES (%s, %s, '%s', '%s', FROM_UNIXTIME(%d), %s, TRUE, '%s')",
        admin_steamid_query, admin_name_query, safe_identity, safe_player_name, start_time, end_time, safe_reason);

    g_DB.Query(OnQueryFinished, query);
}

public Action OnRemoveBan(const char[] identity, int flags, const char[] command, any source)
{
    char safe_identity[64];
    g_DB.Escape(identity, safe_identity, sizeof(safe_identity));

    char query[512];
    Format(query, sizeof(query), "UPDATE `bans` SET `active` = FALSE WHERE `identity` = '%s'", safe_identity);

    g_DB.Query(OnQueryFinished, query);
    return Plugin_Continue;
}

void OnQueryFinished(Database db, DBResultSet results, const char[] error, any data)
{
    if (strlen(error)) {
        LogError("Error executing db query: %s", error);
        return;
    }
}
