CREATE TABLE IF NOT EXISTS `log`(
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `time` TIMESTAMP NOT NULL,
    `port` SMALLINT UNSIGNED NOT NULL,
    `player_id` INT NOT NULL,
    `player_name` VARCHAR(64) NOT NULL,
    `event` CHAR(32) NOT NULL,
    `data` VARCHAR(255) NOT NULL,
    KEY `event` (`event`),
    KEY `port` (`port`),
    KEY `time` (`time`)
);
