CREATE TABLE IF NOT EXISTS `reports` (
    `hero_id` VARCHAR(32) NOT NULL,
    `hero_name` VARCHAR(64) NOT NULL,
    `scum_id` VARCHAR(32) NOT NULL,
    `scum_name` VARCHAR(64) NOT NULL,
    `reason` VARCHAR(255) NOT NULL,
    `port` SMALLINT UNSIGNED NOT NULL,
    `time` TIMESTAMP NOT NULL
);
