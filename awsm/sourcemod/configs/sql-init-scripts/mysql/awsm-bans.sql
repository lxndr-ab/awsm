CREATE TABLE IF NOT EXISTS `bans` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `admin_id` INT UNSIGNED DEFAULT NULL,
    `admin_steamid` VARCHAR(32) DEFAULT NULL,
    `identity` VARCHAR(32) NOT NULL,
    `player_name` VARCHAR(64) NOT NULL,
    `start_time` TIMESTAMP NOT NULL,
    `end_time` TIMESTAMP DEFAULT NULL,
    `active` BOOL NOT NULL,
    `reason` VARCHAR(64) NOT NULL
);
