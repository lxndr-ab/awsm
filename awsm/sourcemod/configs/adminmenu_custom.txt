// Custom admin menu commands.
// For more information:
//
// http://wiki.alliedmods.net/Custom_Admin_Menu_%28SourceMod%29
//
// Note: This file must be in Valve KeyValues format (no multiline comments)
//

"Commands"
{
	"PlayerCommands"
	{
		"Switch player"
		{
			"cmd"			"sm_swapto @1 @2"
			"execute"		"player"
			"1"
			{
				"type"		"player"
				"method"	"userid"
			}
			"2"
			{
				"type"		"list"
				"title"		"Team"
				"1"			"1"
				"1."		"The Spectators"
				"2"			"2"
				"2."		"The Survivors"
				"3"			"3"
				"3."		"The Infected"			
			}
		}
		
		"Swap players"
		{
			"cmd"			"sm_swap @1 @2"
			"execute"		"player"
			"1"
			{
				"type"		"player"
				"method"	"userid"
			}
			"2"
			{
				"type"		"player"
				"method"	"userid"
			}
		}
	}
	
	
	"Spawning"
	{
		"The Infected"
		{
			"cmd"			"sm_spawn @1"
			"execute"		"player"
			"1"
			{
				"type"		"list"
				"title"		"Kind"
				"1"			"tank"
				"1."		"Tank"
				"2"			"witch"
				"2."		"Witch"
				"3"			"hunter"
				"3."		"Hunter"
				"4"			"boomer"
				"4."		"Boomer"
				"5"			"smoker"
				"5."		"Smoker"
				"6"			"spitter"
				"6."		"Spitter (L4D2)"
				"7"			"jockey"
				"7."		"Jockey (L4D2)"
				"8"	    	"charger"
				"8."		"Charger (L4D2)"
				"9"			"zombie"
				"9."		"Zombie"
				"10"		"horde"
				"10."		"Horde"
			}
		}

		"Items"
		{
			"cmd"			"sm_spawn @1"
			"execute"		"player"
			"1"
			{
				"type"		"list"
				"title"		"Kind"
				"1"			"first_aid_kit"
				"1."		"First Aid Kit"
				"2"			"defibrilator"
				"2."		"Defibrilator (L4D2)"
				"3"			"pipe_bomb"
				"3."		"Pipe-bomb"
				"4"			"molotov"
				"4."		"Molotov"
				"5"			"vomitjar"
				"5."		"Vomit Jar (L4D2)"
				"6"			"pain_pills"
				"6."		"Pain pills"
				"7"			"adrenaline"
				"7."		"Adrenaline (L4D2)"
				"8"			"gascan"
				"8."		"Gas can"
				"9"			"propanetank"
				"9."		"Propane tank"
				"10"		"oxygentank"
				"10."		"Oxygen tank"
			}
		}

		"Weapons"
		{
			"cmd"			"sm_spawn @1"
			"execute"		"player"
			"1"
			{
				"type"		"list"
				"title"		"Kind"
				"1"			"autoshotgun"
				"1."		"Auto-shotgun"
				"2"			"rifle"
				"2."		"M-16 Assault Rifle"
				"3"			"hunting_rifle"
				"3."		"Hunting rifle"
				"4"			"pistol"
				"4."		"M1911 Pistol"
				"5"			"smg"
				"5."		"Submachine Gun"
				"6"			"pumpshotgun"
				"6."		"Pump-shotgun"
			}
		}

		"Weapons (L4D2)"
		{
			"cmd"			"sm_spawn @1"
			"execute"		"player"
			"1"
			{
				"type"		"list"
				"title"		"Kind"
				"1"         "rifle_ak47"
				"1."        "AK-7"
				"2"         "sniper_awp"
				"2."        "AWP"
				"3"         "shotgun_chrome"
				"3."        "Chrome Shotgun"
				"4"         "rifle_desert"
				"4."        "Combat Rifle"
				"5"         "shotgun_spas"
				"5."        "Combat Shotgun"
				"6"         "weapon_grenade_launcher"
				"6."        "Grenade Launcher"
				"7"         "pistol_magnum"
				"7."        "Magnum Pistol"
				"8"         "rifle_m60"
				"8."        "M60"
				"9"         "smg_mp5"
				"9."        "MP5"
				"10"        "rifle_sg552"
				"10."       "SG 552"
				"11"        "sniper_scout"
				"11."       "Scout"
				"12"        "smg_silenced"
				"12."       "Silenced Submachine Gun"
				"13"        "sniper_military"
				"13."       "Sniper Rifle"
			}
		}

		"Melee (L4D2)"
		{
            "cmd"			"sm_spawn @1"
			"execute"		"player"
			"1"
			{
				"type"		"list"
				"title"		"Kind"
				"1"			"baseball_bat"
				"1."		"Baseball Bat"
				"2"			"chainsaw"
				"2."		"Chainsaw"
				"3"			"cricket_bat"
				"3."		"Cricket Bat"
				"4"			"crowbar"
				"4."		"Crowbar"
				"5"			"fireaxe"
				"5."		"Fireaxe"
				"6"			"frying_pan"
				"6."		"Frying Pan"
				"7"			"golfclub"
				"7."		"Golf Club"
				"8"			"electric_guitar"
				"8."		"Guitar"
				"9"			"katana"
				"9."		"Katana"
				"10"		"knife"
				"10."		"Knife"
				"11"		"machete"
				"11."		"Machete"
				"12"		"tonfa"
				"12."		"Nightstick"
				"13"		"pitchfork"
				"13."		"Pitchfork"
				"14"		"riotshield"
				"14."		"Riot Shield"
				"15"		"shovel"
				"15."		"Shovel"
			}
		}
		
		"Other Things"
		{
			"cmd"			"sm_spawn @1"
			"execute"		"player"
			"1"
			{
				"type"		"list"
				"title"		"Kind"
				"1"			"minigun"
				"1."		"Minigun"
				"2"			"50cal"
				"2."		"50cal"
				"3"			"ammostack"
				"3."		"Ammo Stack"
				"4"			"barrel"
				"4."		"Fuel Barrel"
			}
		}
		
		"Unspawn"
		{
			"cmd"			"sm_unspawn"
			"execute"		"player"
		}
	}
}
